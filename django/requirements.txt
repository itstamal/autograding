Django==1.9.1
South==1.0.2
click==6.2
django-rq==0.9.0
mysqlclient==1.3.7
redis==2.10.5
requests==2.9.1
rq==0.5.6
