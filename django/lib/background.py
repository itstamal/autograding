import json
import random
import requests
from main.models import Submission
import subprocess
from webapp.settings import BASE_DIR


def grade_request(submission_id):
    submission = Submission.objects.filter(id=submission_id).first()
    print(BASE_DIR + '/test.py')
    # TODO: pass course shortcode, homework short code, directory
    response = subprocess.check_output(['python', BASE_DIR + '/test.py'])
    parts = str(response.decode('utf-8')).strip('\n').split('|')
    submission.grade = parts[0]
    submission.feedback = parts[2]
    submission.efficiency = parts[1]
    submission.was_graded = True
    submission.save()
    print(submission.was_graded)
    print(submission.feedback)
