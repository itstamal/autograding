from django.http import JsonResponse


def response(text, object_id=None):
    return JsonResponse({'message': text, 'id': object_id})
