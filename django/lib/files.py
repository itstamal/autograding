import zipfile
from webapp.settings import UPLOAD_DIRECTORY


def unzip_file(zip_folder, username, assignment=None):
    # Open folder
    with zipfile.ZipFile(zip_folder, 'r') as z:
        output = UPLOAD_DIRECTORY + username + '/submissions/' + assignment
        print(output)
        z.extractall(output)
        return output
