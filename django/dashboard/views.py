from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from time import sleep
from lib.ajax import response
from lib.background import grade_request
from lib.files import unzip_file
from main.models import Subject, Submission, Assignment, Report
from webapp.settings import UPLOAD_DIRECTORY
from django.contrib import messages

title = "Dashboard"


def view_assignment(request, subject_id):
    subject = Subject.objects.filter(shortcode=str(subject_id)).first()
    small = subject.name
    latest = None
    if subject.latest_assignment is not None:
        latest = subject.latest_assignment
        print(latest.name)
    return render(request, 'dashboard/view_class.html', {'title': title, 'small': small, 'latest': latest,
                                                         'subject': subject,})


def submit(request):
    if not request.user.is_authenticated:
        return response('You must be logged in!')
    password = request.POST.get('submit-password', '')
    assignment = Assignment.objects.filter(id=int(request.POST.get('assignment', '0'))).first()
    username = request.POST.get('submit-name', '')
    user = User.objects.filter(email=request.POST.get('submit-username', '')).first()
    if not user:
        return response("No user is found with that username to associate the submission to!")
    print(user.username)
    subject_id = request.POST.get('subject_id', '')
    if not user.check_password(password):
        return response("Your password was incorrect!")
    file = request.FILES['docfile']
    print(str(file))
    if str(file).endswith('.zip') or str(file).endswith('tar'):
        output = unzip_file(file, request.user.username, subject_id + '/' + assignment.shortcode)
        submission = Submission.objects.create(assignment=assignment, anon_name=username,
                                               user=user, directory_location=output)
        submission.save()
        print("First: " + str(submission.id))
        # Get Queue and send task to it
        grade_request(submission.id)
        # django_rq.enqueue(grade_request, submission_id=submission.id)

        return response("Your name has been added to the queue, please wait...", object_id=submission.id)
    return response('The file uploaded was not .zip or .tar')


def check_response(request, submission_id):
    block = True
    sleep(1)
    while block:
        submission = Submission.objects.filter(id=submission_id).first()
        if submission.was_graded:
            block = False
            # Send back response details
        print('Ugh, waiting')
        sleep(1)

    # Set up message return
    message = "Grade: " + str(submission.grade) + " out of " + str(submission.assignment.max_grade) + "<br />"
    message += "Run time: " + str(submission.efficiency) + ' seconds.<br/><br/>'
    message += "Feedback: " + submission.feedback
    return response(message)


def report(request, assignment):
    assignment = Assignment.objects.filter(id=int(assignment)).first()
    report = Report.objects.create(assignment=assignment)
    report.save()
    return redirect('dashboard:index')
