from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<subject_id>[a-zA-Z0-9]{1,7})$', views.view_assignment, name='view'),
    url(r'^submit/$', views.submit, name='submit'),
    url(r'^check-response/(?P<submission_id>[0-9]{1,})/$', views.check_response, name='check_response'),
    url(r'^report/(?P<assignment>[0-9]{1,})$', views.report, name='report'),
]
