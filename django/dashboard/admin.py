from django.contrib import admin
from main.models import Report


class ReportAdmin(admin.ModelAdmin):
    list_display = ('name', 'subject')

admin.site.register(Report, ReportAdmin)
