"""webapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.models import User


def index(request):
    # return render(request, 'marketing/index.html')
    return redirect('dashboard:index')


def register_view(request):
    return render(request, 'marketing/signup.html')


def register_user(request):
    check_user = User.objects.filter(username=request.POST.get('username', ''))
    if check_user:
        messages.error(request, 'That user already exists!')
        return redirect('signup')

    if request.POST.get('password', None) and request.POST.get('username', None):
        username = request.POST.get('username', '')
        user = User.objects.create_user(username, username + '@buffalo.edu', request.POST.get('password', ''))
        user.save()
        user = authenticate(username=user.username, password=request.POST.get('password', ''))
        if user:
            login(request, user)
            return redirect('dashboard:settings')
    messages.error(request, 'Something went wrong!')
    return redirect('signup')


def login_view(request):
    if not request.user.username in ['', None]:
        return redirect('dashboard:index')
    return render(request, 'marketing/login.html')


def login_controller(request):
    user = authenticate(username=request.POST.get('username', ''), password=request.POST.get('password', ''))
    if user:
        login(request, user)
        return redirect('dashboard:index')
    return redirect('login')


def logout_view(request):
    logout(request)
    return redirect('index')


def contact(request):
    message = "Name: " + request.POST.get('name', '') + " " + request.POST.get('surname', '') + "\n"
    message += "Email: " + request.POST.get('email', '') + "\n"
    message += "\n" + request.POST.get('message', '')

    send_mail('UB CSE Contact', message, 'email', ['to_email'])
    return redirect('index')


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^django-rq/', include('django_rq.urls')),
    url(r'^login/$', login_view, name='login'),
    url(r'^signup/$', register_view, name='signup'),
    url(r'^signup/complete/$', register_user, name='register_user'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^login/user/$', login_controller, name='login_user'),
    url(r'^contact/$', contact, name='contact'),
    url(r'^admin/', admin.site.urls),
    url(r'^view/', include('dashboard.urls', namespace='dashboard')),
]
