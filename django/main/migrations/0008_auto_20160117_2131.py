# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-17 21:31
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20160116_0328'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='assignment',
            name='due_on',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='submission',
            name='uploaded_at',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
