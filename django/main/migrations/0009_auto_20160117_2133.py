# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-17 21:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20160117_2131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='due_on',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='submission',
            name='uploaded_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
