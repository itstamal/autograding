from django.contrib import admin
from main.models import Subject, Assignment, Submission


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'shortcode', 'teacher', 'size')

admin.site.register(Subject, SubjectAdmin)


class AssignmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'shortcode', 'subject', 'created_on', 'due_on')

admin.site.register(Assignment, AssignmentAdmin)


class SubmissionAdmin(admin.ModelAdmin):
    list_display = ('assignment', 'user', 'anon_name', 'grade', 'directory_location', 'efficiency', 'uploaded_at')


admin.site.register(Submission, SubmissionAdmin)
