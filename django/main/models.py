import datetime
from django.db.models import CharField, Model, OneToOneField, CASCADE, ForeignKey, ManyToManyField, IntegerField, \
    DateTimeField, DecimalField, TextField, BooleanField, FileField
from django.utils import timezone
from django.contrib.auth.models import User


class Profile(Model):
    user = OneToOneField(User, on_delete=CASCADE)


class Subject(Model):
    name = CharField(max_length=128, default='')
    shortcode = CharField(max_length=16, default='')
    teacher = ForeignKey(User, related_name='teaching_subjects', on_delete=CASCADE)
    students = ManyToManyField(User, related_name='subjects', blank=True)
    information = FileField(upload_to='class/', null=True, blank=True)

    @property
    def latest_assignment(self):
        latest = None
        assignments = self.assignments.all()
        if len(assignments) > 0:
            latest = assignments[0]
        for assignment in assignments:
            if assignment.due_on > latest.due_on:
                latest = assignment
        return latest

    @property
    def size(self):
        return len(self.students.all())

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'subjects'


class Assignment(Model):
    name = CharField(max_length=128, default='')
    shortcode = CharField(max_length=16, default='')
    subject = ForeignKey(Subject, related_name='assignments', on_delete=CASCADE)
    max_grade = IntegerField(default=100)
    created_on = DateTimeField(auto_now_add=True)
    due_on = DateTimeField(default=timezone.now)

    @property
    def best_submissions(self):
        return self.submissions.all()[:10]

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'assignments'


class Submission(Model):
    assignment = ForeignKey(Assignment, related_name='submissions', on_delete=CASCADE)
    anon_name = CharField(max_length=32, default='')
    user = ForeignKey(User, related_name='submissions', on_delete=CASCADE)
    grade = IntegerField(default=0)
    feedback = TextField(default='')
    directory_location = CharField(max_length=128, default='')
    uploaded_at = DateTimeField(auto_now_add=True)
    efficiency = DecimalField(max_digits=6, decimal_places=3, default=0.0)
    was_graded = BooleanField(default=False)

    def __str__(self):
        return self.assignment.name + " Submission by " + self.user.username

    class Meta:
        db_table = 'submissions'
        ordering = ['-grade', 'efficiency', '-uploaded_at']


class Report(Model):
    assignment = ForeignKey(Assignment, related_name='reports')

    @property
    def name(self):
        return self.assignment.name

    @property
    def subject(self):
        return self.assignment.subject.name

    class Meta:
        db_table = 'reports'
