// Submit post on submit
$('#post-form').on('submit', function(event){
    event.preventDefault();
    console.log("form submitted!")  // sanity check
    create_post();
});



// AJAX for posting
function create_post() {
    console.log("create post is working!"); // sanity check
    fd = new FormData($('#post-form')[0]);
    console.log($('#post-form')[0]);
    $('#responses').append("<div class='overlay quickremove'></div> <div class='loading-img quickremove'></div>");
    $.ajax({
        url : "submit/", // the endpoint
        type : "POST", // http method
        processData : false,
        contentType : false,
        data : fd,

        // handle a successful response
        success : function(json) {
            $('.quickremove').remove();
            $('.quickclear').val(''); // remove the value from the input
            console.log(json); // log the returned json to the console
            $('#response-text').text(json['message']);
            if (json['message'] == "Your name has been added to the queue, please wait...") {
                report_grade(json['id']);
            }
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('.quickremove').remove();
            $('#response-text').text("Oops! We have encountered an error!"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

function report_grade(id) {
    $.ajax({
        url : "check-response/" + id + "/",
        type : "POST",

        success : function(json) {
            var ele = document.getElementById('response-text');
            ele.innerHTML = json['message'];
        },

        error : function(xhr, errmsg, err) {
            $('#response-text').text('Your homework was submitted, but there was an error when trying to recieve an immediate feedback! Please contact your instructor with questions.');
        }
    })
}

$(function() {


    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});