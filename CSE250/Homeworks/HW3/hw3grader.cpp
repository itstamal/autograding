#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <unordered_set>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <fstream>

#include "StockTracker.h"
#include "TestSuite.h"

using namespace std;

TestSuite suite;
streambuf *old = cout.rdbuf();
stringstream ss;

const double EPSILON = 0.001;

void suppressCout() {
    cout.rdbuf(ss.rdbuf());
}

void restoreCout() {
    cout.rdbuf(old);
}

bool doubleCompare(double a, double b){
    return abs(a-b) < EPSILON;
}

char genRandom() {
    std::string alpha = "udlrab";
    return alpha.at(rand() % alpha.length());
}

string intToString(int a) {
    stringstream ss;
    ss << a;
    return ss.str();
}



double realDataUsage(){

    StockTracker stockTracker;

    string totalSharesFilename = "sharesOutstanding.csv";
    string pricesFilename = "2016May3Price.csv";

    ifstream shares;
    ifstream currentPrices;

    shares.open(totalSharesFilename);
    currentPrices.open(pricesFilename);

    // Always check to see if file opening succeeded
    if (!shares.is_open())
    {
        cout << "Could not open " + totalSharesFilename + "\n";
        return -2.0;
    }
    if (!currentPrices.is_open())
    {
        cout << "Could not open " + pricesFilename + "\n";
        return -2.0;
    }


    string lineFromFile;
    getline(shares, lineFromFile); // Strip reference

    // Assumes specific format is followed
    while(getline(shares, lineFromFile)){
        string ticker = lineFromFile.substr(0,lineFromFile.find(","));
        // Market cap in thousands of shares
        int totalShares = stoi(lineFromFile.substr(lineFromFile.find(",")+1, lineFromFile.length() - lineFromFile.find(",") - 4));
        stockTracker.registerTicker(ticker, totalShares);
    }


    getline(currentPrices, lineFromFile); // Strip reference

    vector<pair<string,double> > stockPrices;

    // Assumes specific format is followed
    while(getline(currentPrices, lineFromFile)){
        string ticker = lineFromFile.substr(0,lineFromFile.find(","));
        double stockPrice = stod(lineFromFile.substr(lineFromFile.find(",")+1));
        stockPrices.push_back(make_pair(ticker,stockPrice));
    }


    vector<pair<string,double> >::iterator itPair;

    clock_t startTime = clock();

    for(itPair = stockPrices.begin(); itPair != stockPrices.end(); itPair++){
        string ticker = (*itPair).first;
        double stockPrice = (*itPair).second;
        for(int i=0; i<10; i++) {
            stockTracker.updateCurrentPrice(ticker, 100.0 * ((double) rand() / RAND_MAX)); // Simulate a changing price
        }
        stockTracker.updateCurrentPrice(ticker, stockPrice); // Replace random value with actual price
    }


    vector <string> topStocks;
    int topK = 20;
    // Run multiple times for timing test
    for(int i=0; i<5; i++) {
        topStocks = stockTracker.topMarketCaps(topK);
    }

    double elapsedTime = (double(clock() - startTime) / CLOCKS_PER_SEC)*1000.0;

    int rank = 1;
    for(vector<string>::iterator it = topStocks.begin(); it != topStocks.end(); it ++){
        string currentTicker = *it;
        cout << rank << ": " << currentTicker <<
        " | market cap: " << stockTracker.getMarketCap(currentTicker) <<
        " | price: " << stockTracker.getCurrentPrice(currentTicker) <<
        " | share outstanding: " << stockTracker.getSharesOutstanding(currentTicker) << endl;
        rank++;
    }

    cout << "\nElapsed time: " << elapsedTime << "ms" << endl;

    vector <string> topStocksSolution;

    topStocksSolution.push_back("AAPL");
    topStocksSolution.push_back("GOOG");
    topStocksSolution.push_back("MSFT");
    topStocksSolution.push_back("XOM");
    topStocksSolution.push_back("FB");

    topStocksSolution.push_back("GE");
    topStocksSolution.push_back("JNJ");
    topStocksSolution.push_back("AMZN");
    topStocksSolution.push_back("WFC");
    topStocksSolution.push_back("T");

    topStocksSolution.push_back("PG");
    topStocksSolution.push_back("JPM");
    topStocksSolution.push_back("VZ");
    topStocksSolution.push_back("WMT");
    topStocksSolution.push_back("KO");

    topStocksSolution.push_back("PFE");
    topStocksSolution.push_back("V");
    topStocksSolution.push_back("CVX");
    topStocksSolution.push_back("DIS");
    topStocksSolution.push_back("ORCL");

    if(topStocksSolution.size() != topStocks.size()){
        return -1.0;
    }

    for(int i=0; i<topStocksSolution.size(); i++){
        if(topStocksSolution.at(i) != topStocks.at(i)){
            return -1.0;
        }
    }

    return elapsedTime;


//    1: AAPL | market cap: 5.62886e+08 | price: 101.52 | share outstanding: 5544583
//    2: GOOG | market cap: 4.89567e+08 | price: 711.25 | share outstanding: 688319
//    3: MSFT | market cap: 4.13914e+08 | price: 52.3325 | share outstanding: 7909302
//    4: XOM | market cap: 3.41896e+08 | price: 82.33 | share outstanding: 4152756
//    5: FB | market cap: 3.11411e+08 | price: 109.41 | share outstanding: 2846280
//    6: GE | market cap: 2.94703e+08 | price: 30.2415 | share outstanding: 9745000
//    7: JNJ | market cap: 2.94368e+08 | price: 106.68 | share outstanding: 2759359
//    8: AMZN | market cap: 2.71728e+08 | price: 577.11 | share outstanding: 470842
//    9: WFC | market cap: 2.52135e+08 | price: 49.665 | share outstanding: 5076712
//    10: T | market cap: 2.33684e+08 | price: 37.99 | share outstanding: 6151208
//    11: PG | market cap: 2.24181e+08 | price: 82.89 | share outstanding: 2704565
//    12: JPM | market cap: 2.18993e+08 | price: 59.785 | share outstanding: 3663001
//    13: VZ | market cap: 2.11453e+08 | price: 51.905 | share outstanding: 4073841
//    14: WMT | market cap: 2.10531e+08 | price: 66.08 | share outstanding: 3186000
//    15: KO | market cap: 1.90065e+08 | price: 43.9 | share outstanding: 4329497
//    16: PFE | market cap: 1.8448e+08 | price: 29.885 | share outstanding: 6173001
//    17: V | market cap: 1.76964e+08 | price: 73.715 | share outstanding: 2400645
//    18: CVX | market cap: 1.64748e+08 | price: 87.485 | share outstanding: 1883156
//    19: DIS | market cap: 1.6106e+08 | price: 98.715 | share outstanding: 1631570
//    20: ORCL | market cap: 1.59016e+08 | price: 37.85 | share outstanding: 4201220
//
//    Elapsed time: 30ms

}


void testQ1(void) {
    Test *test1 = new Test("Test1");
    test1->setWorth(1);
    suite.addTest(test1);

    clock_t startTime = clock();


    bool passedTest = true;

    StockTracker tracker;

    tracker.registerTicker("StockA", 100);
    tracker.registerTicker("StockB", 200);
    tracker.registerTicker("StockC", 300);
    tracker.registerTicker("StockD", 200);

    if(passedTest && !tracker.isTicker("StockA")){
        passedTest = false;
        test1->failedTest("Returned false on tracker.isTicker(\"StockA\") from sampleUsage()");
    }
    if(passedTest && !tracker.isTicker("StockB")){
        passedTest = false;
        test1->failedTest("Returned false on tracker.isTicker(\"StockB\") from sampleUsage()");
    }
    if(passedTest && !tracker.isTicker("StockC")){
        passedTest = false;
        test1->failedTest("Returned false on tracker.isTicker(\"StockC\") from sampleUsage()");
    }

    if(passedTest && tracker.isTicker("StockE")){
        passedTest = false;
        test1->failedTest("Returned true on tracker.isTicker(\"StockE\") from sampleUsage()");
    }
    if(passedTest && tracker.isTicker("stockC")){
        passedTest = false;
        test1->failedTest("Returned true on tracker.isTicker(\"stockC\") from sampleUsage()");
    }
    if(passedTest && tracker.isTicker("Stocka")){
        passedTest = false;
        test1->failedTest("Returned true on tracker.isTicker(\"Stocka\") from sampleUsage()");
    }
    if(passedTest && tracker.isTicker("GOOG")){
        passedTest = false;
        test1->failedTest("Returned true on tracker.isTicker(\"GOOG\") from sampleUsage()");
    }

    if(passedTest && tracker.getSharesOutstanding("StockA") != 100){
        passedTest = false;
        test1->failedTest("Returned " + intToString(tracker.getSharesOutstanding("StockA")) + " shares for "
                                 "tracker.getSharesOutstanding(\"StockA\") in sampleUsage() instead of 100");
    }
    if(passedTest && tracker.getSharesOutstanding("StockB") != 200){
        passedTest = false;
        test1->failedTest("Returned " + intToString(tracker.getSharesOutstanding("StockB")) + " shares for "
                                 "tracker.getSharesOutstanding(\"StockB\") in sampleUsage() instead of 200");
    }
    if(passedTest && tracker.getSharesOutstanding("StockC") != 300){
        passedTest = false;
        test1->failedTest("Returned " + intToString(tracker.getSharesOutstanding("StockC")) + " shares for "
                                 "tracker.getSharesOutstanding(\"StockC\") in sampleUsage() instead of 300");
    }
    if(passedTest && tracker.getSharesOutstanding("StockD") != 200){
        passedTest = false;
        test1->failedTest("Returned " + intToString(tracker.getSharesOutstanding("StockD")) + " shares for "
                                 "tracker.getSharesOutstanding(\"StockD\") in sampleUsage() instead of 200");
    }
    if(passedTest && tracker.getSharesOutstanding("StockE") != 0){
        passedTest = false;
        test1->failedTest("Returned " + intToString(tracker.getSharesOutstanding("StockE")) + " shares for "
                                 "tracker.getSharesOutstanding(\"StockE\") in sampleUsage() instead of 0");
    }


    clock_t endTime = clock();

    test1->setRuntime(calculateRuntime(startTime, endTime));
    if (passedTest) {
        test1->passedTest();
    }
}

void testQ2(void) {
    Test *test2 = new Test("Test2");
    test2->setWorth(1);
    suite.addTest(test2);

    clock_t startTime = clock();


    bool passedTest = true;

    StockTracker tracker;

    tracker.registerTicker("StockA", 100);
    tracker.registerTicker("StockB", 200);
    tracker.registerTicker("StockC", 300);
    tracker.registerTicker("StockD", 200);


    tracker.updateCurrentPrice("StockA", 10.0);
    tracker.updateCurrentPrice("StockA", 10.2);
    tracker.updateCurrentPrice("StockA", 10.3);

    if(passedTest && !doubleCompare(tracker.getCurrentPrice("StockA"), 10.3)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getCurrentPrice("StockA")) + " for "
                "tracker.getCurrentPrice(\"StockA\") in sampleUsage() instead of 10.3");
    }

    tracker.updateCurrentPrice("StockA", 11.6);
    if(passedTest && !doubleCompare(tracker.getCurrentPrice("StockA"), 11.6)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getCurrentPrice("StockA")) + " for "
                "tracker.getCurrentPrice(\"StockA\") in sampleUsage() instead of 11.6");
    }


    if(passedTest && !doubleCompare(tracker.getMarketCap("StockA"), 1160.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getMarketCap("StockA")) + " for "
                "tracker.getMarketCap(\"StockA\") in sampleUsage() instead of 1160.0");
    }

    tracker.updateCurrentPrice("StockA", 1.4);
    if(passedTest && !doubleCompare(tracker.getCurrentPrice("StockA"), 1.4)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getCurrentPrice("StockA")) + " for "
                "tracker.getCurrentPrice(\"StockA\") after updating price to 1.4");
    }
    tracker.updateCurrentPrice("StockA", 5.6);
    if(passedTest && !doubleCompare(tracker.getCurrentPrice("StockA"), 5.6)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getCurrentPrice("StockA")) + " for "
                "tracker.getCurrentPrice(\"StockA\") after updating price to 5.6");
    }
    tracker.updateCurrentPrice("StockA", 1.55);
    if(passedTest && !doubleCompare(tracker.getCurrentPrice("StockA"), 1.55)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getCurrentPrice("StockA")) + " for "
                "tracker.getCurrentPrice(\"StockA\") after updating price to 1.55");
    }

    tracker.updateCurrentPrice("StockB", 5.25);

    if(passedTest && !doubleCompare(tracker.getCurrentPrice("StockB"), 5.25)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getCurrentPrice("StockB")) + " for "
                "tracker.getCurrentPrice(\"StockB\") in sampleUsage() instead of 5.25");
    }

    if(passedTest && !doubleCompare(tracker.getCurrentPrice("StockC"), 0.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getCurrentPrice("StockC")) + " for "
                "tracker.getCurrentPrice(\"StockC\") in sampleUsage() instead of 0.0");
    }


    if(passedTest && !doubleCompare(tracker.getMarketCap("StockA"), 155.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getMarketCap("StockA")) + " for "
                "tracker.getMarketCap(\"StockA\") after updating price to 1.55 in sampleUsage() instead of 155.0");
    }
    if(passedTest && !doubleCompare(tracker.getMarketCap("StockB"), 1050.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getMarketCap("StockB")) + " for "
                "tracker.getMarketCap(\"StockB\") in sampleUsage() instead of 1050.0");
    }
    if(passedTest && !doubleCompare(tracker.getMarketCap("StockC"), 0.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getMarketCap("StockC")) + " for "
                "tracker.getMarketCap(\"StockC\") in sampleUsage() instead of 0.0");
    }
    if(passedTest && !doubleCompare(tracker.getMarketCap("StockD"), 0.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getMarketCap("StockD")) + " for "
                "tracker.getMarketCap(\"StockD\") in sampleUsage() instead of 0.0");
    }
    if(passedTest && !doubleCompare(tracker.getMarketCap("StockE"), 0.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getMarketCap("StockE")) + " for "
                "tracker.getMarketCap(\"StockE\") in sampleUsage() instead of 0.0");
    }


    tracker.registerTicker("StockE", 1000);
    tracker.updateCurrentPrice("StockE", 200.0);


    if(passedTest && !doubleCompare(tracker.getMarketCap("StockE"), 200000.0)){
        passedTest = false;
        test2->failedTest("Returned " + intToString(tracker.getMarketCap("StockE")) + " for "
                "tracker.getMarketCap(\"StockE\") after registering with 1000 shares and a price of 100.0");
    }


    clock_t endTime = clock();

    test2->setRuntime(calculateRuntime(startTime, endTime));
    if (passedTest) {
        test2->passedTest();
    }
}


void testQ3(void) {
    Test *test3 = new Test("Test3");
    test3->setWorth(1);
    suite.addTest(test3);

    bool passedTest = true;
    clock_t startTime = clock();


    if(realDataUsage() < -0.5){
        passedTest = false;
        test3->failedTest("Output of topMarketCaps did not match the sample in realDataUsage()");
    }

    if(passedTest) {

        StockTracker tracker;

        tracker.registerTicker("StockA", 100);
        tracker.registerTicker("StockB", 200);
        tracker.registerTicker("StockC", 300);
        tracker.registerTicker("StockD", 200);
        tracker.registerTicker("StockE", 500);
        tracker.registerTicker("StockF", 900);
        tracker.registerTicker("StockG", 100);
        tracker.registerTicker("StockH", 20);
        tracker.registerTicker("StockI", 550);
        tracker.registerTicker("StockJ", 1000);

        tracker.updateCurrentPrice("StockA", 10.0);
        tracker.updateCurrentPrice("StockA", 10.2);
        tracker.updateCurrentPrice("StockA", 10.3);
        tracker.updateCurrentPrice("StockA", 11.6);
        tracker.updateCurrentPrice("StockA", 11.0);

        tracker.updateCurrentPrice("StockB", 5.5);
        tracker.updateCurrentPrice("StockB", 5.6);

        tracker.updateCurrentPrice("StockC", 1.0);
        tracker.updateCurrentPrice("StockC", 5.5);

        tracker.updateCurrentPrice("StockD", 11.0);
        tracker.updateCurrentPrice("StockD", 7.7);

        tracker.updateCurrentPrice("StockE", 17.0);
        tracker.updateCurrentPrice("StockE", 10.2);

        tracker.updateCurrentPrice("StockF", 14.9);
        tracker.updateCurrentPrice("StockF", 8.8);

        tracker.updateCurrentPrice("StockG", 110.0);
        tracker.updateCurrentPrice("StockG", 2.2);

        tracker.updateCurrentPrice("StockH", 1.0);
        tracker.updateCurrentPrice("StockH", 2.0);

        tracker.updateCurrentPrice("StockI", 11.0);
        tracker.updateCurrentPrice("StockI", 1.5);

        tracker.updateCurrentPrice("StockJ", 7.0);
        tracker.updateCurrentPrice("StockJ", 15.5);


        vector <string> studentTop3 = tracker.topMarketCaps(3);


        vector <string> studentTop6 = tracker.topMarketCaps(6);

        vector<string> topStocks;
        topStocks.push_back("StockJ");
        topStocks.push_back("StockF");
        topStocks.push_back("StockE");
        topStocks.push_back("StockC");
        topStocks.push_back("StockD");
        topStocks.push_back("StockB");
        topStocks.push_back("StockA");
        topStocks.push_back("StockI");
        topStocks.push_back("StockG");
        topStocks.push_back("StockH");

        if(passedTest && studentTop3.size() != 3){
            passedTest = false;
            test3->failedTest("Returned " + intToString(studentTop3.size()) + " elements when calling tracker.topMarketCaps(3)");
        }

        if(passedTest && studentTop6.size() != 6){
            passedTest = false;
            test3->failedTest("Returned " + intToString(studentTop6.size()) + " elements when calling tracker.topMarketCaps(6)");
        }

        if(passedTest) {
            for (int i = 0; i < 3; i++) {
                if (studentTop3.at(i) != topStocks.at(i)) {
                    passedTest = false;
                    break;
                }
            }
            for (int i = 0; i < 6; i++) {
                if (studentTop6.at(i) != topStocks.at(i)) {
                    passedTest = false;
                    break;
                }
            }
        }

        if(!passedTest){
            passedTest = false;
            test3->failedTest("Output of topMarketCaps matched order in realDataUsage(), but did not match while "
                                      "testing with duplicates in market cap, shares outstanding, and price. Try "
                                      "expanding and testing on the sample given in run.cpp:\n"
                                      " // *Similar to, but not exactly the test inputs used\n"
                                      " tracker.registerTicker(\"StockA\", 100);\n"
                                      " tracker.registerTicker(\"StockB\", 200);\n"
                                      " tracker.registerTicker(\"StockC\", 300);\n"
                                      " tracker.registerTicker(\"StockD\", 200);\n"
                                      " tracker.updateCurrentPrice(\"StockA\", 10.0);\n"
                                      " tracker.updateCurrentPrice(\"StockA\", 10.2);\n"
                                      " tracker.updateCurrentPrice(\"StockA\", 10.3);\n"
                                      " tracker.updateCurrentPrice(\"StockA\", 11.6);\n"
                                      " tracker.updateCurrentPrice(\"StockA\", 11.0);\n"
                                      " tracker.updateCurrentPrice(\"StockB\", 5.5);\n"
                                      " tracker.updateCurrentPrice(\"StockB\", 5.6);\n"
                                      " tracker.updateCurrentPrice(\"StockC\", 1.0);\n"
                                      " tracker.updateCurrentPrice(\"StockD\", 1.0);\n");
        }


    }


    clock_t endTime = clock();

    test3->setRuntime(calculateRuntime(startTime, endTime));
    if (passedTest) {
        test3->passedTest();
    }
}


void testQ4(void) {
    Test *test4 = new Test("Test4");
    test4->setWorth(1);
    suite.addTest(test4);


    double runtime = realDataUsage();


    test4->setRuntime(runtime);
    if (runtime > -0.5 && runtime<65) {
        test4->passedTest();
    } else {
        test4->failedTest("Check for O(log(n)) runtime for updateCurrentPrice and topMarketCaps");
    }

}

int main(int argc, char *argv[]) {
    //main testing logic
    double runtime;
    for (int i = 1; i < argc; i++) {
        switch (atoi(argv[i])) {
            case 1:
                cout << "Running testQ1" << endl;
                suppressCout();
                testQ1();
                restoreCout();
                break;
            case 2:
                cout << "Running testQ2" << endl;
                suppressCout();
                testQ2();
                restoreCout();
                break;
            case 3:
                cout << "Running testQ3" << endl;
                suppressCout();
                testQ3();
                restoreCout();
                break;
            case 4:
                cout << "Running testQ4" << endl;
                suppressCout();
                testQ4();
                restoreCout();
                break;
            default:
                cout << "No test found matching input" << endl;
        }
    }
    //Couts for feedback results. Should be standard for each assignment.
    cout << suite.results();
    cout << "Success Rate: " << suite.percentage() << "/4" << endl;
    cout << "Total Runtime: " << suite.totalRuntime() << "ms" << endl;
    cout << endl;
    return 0;

}
