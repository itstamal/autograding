// Make sure filename is "Grade(AssignmentName)" with proper capitalization. 
#include "TestSuite.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
//Set up three suites at the top corresponding to each "Part"
TestSuite suite;
streambuf *old = cout.rdbuf();
stringstream ss;

extern int countCharOnEven(std::string phrase, char letter); //Temp till we use header files
extern int computeFibonacci(int number); //Temp till we use header files

void suppressCout(){
    cout.rdbuf (ss.rdbuf());
}

void restoreCout(){
    cout.rdbuf (old);
}

char genRandom(){
    std::string alpha ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    return alpha.at(rand() % alpha.length());
}

string intToString(int a){
    stringstream ss;
    ss << a;
    return ss.str();
}

//helper functions to check correctness (usually renamed solutions dumped in to check against students code)
int countCharsGrade(std::string words, char character){
    int count=0;
    for(int i=0; i<words.length(); i=i+2){
        if(words[i] == character){
            count++;
        }
    }
    return count;
}

int computeFibonacciGrade(int n){
    int a = 0, b = 1, c, i;
     if( n == 0){return a;}
    for (i = 2; i <= n; i++){
        c = a + b;
        a = b;
        b = c;
    }
    return b;
}

void testQ1(void){
    Test *test1 = new Test("Q1");
    test1->setWorth(25);
    suite.addTest(test1);
    clock_t startTime = clock();
    bool passed = true;
    std::string Str;
    for (int j =0; j< 10; j++){
        Str = "";
        char c = genRandom();
        for(unsigned int i = 0; i < 99; ++i){
            Str += genRandom();
        }
        if(countCharOnEven(Str, c) != countCharsGrade(Str, c)){
            passed=false;
            break;
        }
    }
    if(passed){
        test1->passedTest();
    }else{
        test1->failedTest("Strings didnt match on input string " + Str + ". Please check your code"); 
    }
    clock_t endTime = clock();
    test1->setRuntime(calculateRuntime(startTime,endTime));
}
void testQ2(void){
    Test *test2 = new Test("Q2");
    test2->setWorth(25);
    suite.addTest(test2);
    clock_t startTime = clock();     
    bool passed = true;
    int n = 1;
    for(n; n<6; n++){  
        if(computeFibonacciGrade(n) != computeFibonacci(n)){
            passed = false;
            break;
        }
    }
    if(passed){
        test2->passedTest();
    }else{
        test2->failedTest("Fibonacci number incorrect on input " + intToString(n) + ". Please check your code");         
        
    }
    clock_t endTime = clock();
    test2->setRuntime(calculateRuntime(startTime,endTime)); 
}
void testQ3(void){
    Test *test3 = new Test("Q3");
    test3->setWorth(25);
    suite.addTest(test3);
    clock_t startTime = clock();     
    bool passed = true;
    int n = 0;
    for(n; n<30; n++){  
        if(computeFibonacciGrade(n) != computeFibonacci(n)){
            passed = false;
            break;
        }
    }
    if(passed){
        test3->passedTest();
    }else{
        test3->failedTest("Fibonacci number incorrect on input " + intToString(n) + ". Please check your code");         
        
    }
    clock_t endTime = clock();
    test3->setRuntime(calculateRuntime(startTime,endTime));    
}

void testQ4(void){
    Test *test4 = new Test("Q4");
    test4->setWorth(25);
    suite.addTest(test4);
    clock_t startTime = clock();     
    bool passed = true;
    int n = 0;
    for(n; n<42; n++){  
        if(computeFibonacciGrade(n) != computeFibonacci(n)){
            passed = false;
            break;
        }
    }
    clock_t endTime = clock();
    test4->setRuntime(calculateRuntime(startTime,endTime));
    if(passed){
        if(calculateRuntime(startTime,endTime) < 500){
            test4->passedTest();
        }else{
            test4->failedTest("Your function is not efficient enough. You may want to rethink your approach and optimize your function.");
        }
    }else{
        test4->failedTest("Fibonacci number incorrect on input " + intToString(n) + ". Please check your code");         
        
    }     
}

int main(int argc, char *argv[]){
    //main testing logic
    double runtime;
    for (int i = 1; i < argc; i++) { 
        switch (atoi(argv[i])) {
        case 1:
            cout << "Running testQ1" << endl;
            suppressCout();
            testQ1();
            restoreCout();
            break;
        case 2:
            cout << "Running testQ2" << endl;  
            suppressCout();
            testQ2();
            restoreCout();
            break;
        case 3:
            cout << "Running testQ3" << endl; 
            suppressCout();            
            testQ3();
            break;
        case 4:
            cout << "Running testQ4" << endl; 
            suppressCout();            
            testQ4();
            restoreCout();
            break;          
        default:
            cout << "No test found matching input" << endl;
        }
    }
    //Couts for feedback results. Should be standard for each assignment.
    cout << suite.results(); 
    cout << "Success Rate: " << suite.percentage() << "%" << endl;
    cout << "Total Runtime: " << suite.totalRuntime() << "ms" << endl;
    cout << endl;
    return 0;
}
