import sys


def phraseToSounds(phrase, dictionary):
    words = phrase.split(" ")
    words.reverse()
    sounds = []
    for word in words:
        pronounce = dictionary.get(word, [])
        if len(pronounce) == 0:
            break
        sounds = pronounce + sounds
    return sounds


def isVowel(sound):
    return len(sound) == 3


def rhymeLength(sounds1, sounds2):
    pointer1 = len(sounds1) - 1
    pointer2 = len(sounds2) - 1
    rhymingSyllables = 0
    while pointer1 >= 0 and pointer2 >= 0:
        if sounds1[pointer1] != sounds2[pointer2]:
            break
        if isVowel(sounds1[pointer1]):
            rhymingSyllables += 1
        pointer1 -= 1
        pointer2 -= 1
    return rhymingSyllables


def parseDictionaryFile(originalFilename):
    dictionary = {}
    with open(originalFilename) as pronounceFile:
        for line in pronounceFile:
            line = line.strip()
            if len(line) == 0 or not line[0].isalpha() or "(" in line:
                continue
            else:
                afterSplit = line.split("  ")
                if len(afterSplit) != 2:
                    # print(line)
                    # print("Didn't get 2")
                    continue
                dictionary[afterSplit[0].lower()] = \
                    afterSplit[1].lower().replace("0", "0").replace("1", "0").replace("2", "0").split(" ")
    return dictionary


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("improper usage")
        exit()
    solutionRhymesFilename = sys.argv[1]
    studentRhymesFilename = sys.argv[2]

    dictionary = parseDictionaryFile("cmudict-0.7b")

    with open(solutionRhymesFilename) as solutionFile:
        with open(studentRhymesFilename) as studentFile:
            toRhyme = solutionFile.readline().strip().replace("_", " ").lower()
            toRhymeSounds = phraseToSounds(toRhyme, dictionary)
            for line in solutionFile:
                solutionSounds = phraseToSounds(line.strip().lower(), dictionary)
                studentSounds = phraseToSounds(studentFile.readline().strip().lower(), dictionary)
                solutionRhymes = rhymeLength(solutionSounds, toRhymeSounds)
                studentRhymes = rhymeLength(studentSounds, toRhymeSounds)
                if solutionRhymes != studentRhymes:
                    print("Try: " + toRhyme)
                    exit()
    print("pass")
