#ifndef UNTITLED_GAMELOGICSOLUTION_H
#define UNTITLED_GAMELOGICSOLUTION_H

#include <iostream>
using namespace std;

class GameLogicSolution {

public:
    bool containsKonamiCode(std::string inputString);
    virtual int computeScore(std::string inputString) =0;
};


#endif //UNTITLED_GAMELOGICSOLUTION_H
