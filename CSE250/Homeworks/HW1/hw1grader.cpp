#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <fstream>

#include "TestSuite.h"
#include "GameBoard.h"
#include "TreasureHunter.h"
#include "TreasureHunterSolution.h"


using namespace std;

/*Documentation for TDD to create this at the bottom*/

TestSuite suite;
streambuf *old = cout.rdbuf();
stringstream ss;

void suppressCout() {
    cout.rdbuf(ss.rdbuf());
}

void restoreCout() {
    cout.rdbuf(old);
}

//char genRandom() {
//    std::string alpha = "udlrab";
//    return alpha.at(rand() % alpha.length());
//}

string intToString(int a) {
    stringstream ss;
    ss << a;
    return ss.str();
}

GameBoard getHugeBoard(int howMuchTreasure, int howManyEnemies) {
    GameBoard board(Location(5, 7));
    for (int i = 0; i < howMuchTreasure; i++) {
        board.addTreasure(Location(i, 1));
    }
    for (int i = 0; i < howManyEnemies; i++) {
        board.addEnemy(Location(i, 2));
    }
    return board;
}

GameBoard getBoard1() {
    GameBoard board(Location(5, 7));
    return board;
}
GameBoard getBoard1Point1() {
    GameBoard board(Location(4, 4));
    return board;
}

GameBoard getBoard2() {
    GameBoard board(Location(100, 100));
    board.addTreasure(Location(4, 0));
    board.addEnemy(Location(-4, 0));
    return board;
}

GameBoard getBoard3() {
    GameBoard board(Location(0, 7)); // dlruuuuyllaruauuuluuulll
    board.addTreasure(Location(1, 6));
    board.addTreasure(Location(1, 5));
    board.addTreasure(Location(0, 2));
    board.addEnemy(Location(0, 3));
    board.addEnemy(Location(1, 4));
    board.addEnemy(Location(2, 0));
    return board;
}

void testQ1(void) {
    Test *test1 = new Test("KonamiTest");
    test1->setWorth(1);
    suite.addTest(test1);

    Location goalLocation(Location(5, 7));
    GameBoard board(goalLocation);

    TreasureHunter treasureHunter(0, 0, board);
    TreasureHunterSolution treasureHunterSolution(0, 0, board);
    string lineTest;

    ifstream fs;
    fs.open("/projects/CSE250/Spring2016/AutoGrader/SupportFiles/CSE250/HW1/stringtests.txt");
    bool passedTest = true;
    while (!fs.eof()) {
        getline(fs, lineTest);
        bool studentKonami = treasureHunter.containsKonamiCode(lineTest);
        bool realKonami = treasureHunterSolution.containsKonamiCode(lineTest);
        if (studentKonami != realKonami) {
            passedTest = false;
            break;
        }
    }

    if (passedTest == true) {
        test1->passedTest();
    }
    else {
        test1->failedTest("Did not properly detect the Konami code");
    }
}

void testQ2(void) {
    Test *test2 = new Test("FuntionalityTest");
    test2->setWorth(1);
    suite.addTest(test2);

    string message = "";
    GameBoard theBoard = getBoard1();

// 5,7
    string userInputs = "rrrrrrrrrllllllllllluuuuuuuuuuuuuuuuuuudddddddddddddddd"; //
    // Test basic movement with +1/-2
    theBoard = getBoard1();
    TreasureHunter treasureHunter(0, 0, theBoard);
    int studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard1();
    TreasureHunterSolution treasureHunterSolution(0, 0, theBoard);
    int solutionResult = treasureHunterSolution.computeScore(userInputs); // 24

    if (studentResult != solutionResult) {
        message += "Did not properly update score while moving towards/away from goal.\n";
    }


    userInputs = "rrrrruuuuuuu";
    // Found goal
    theBoard = getBoard1();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard1();
    TreasureHunterSolution treasureHunterSolution2 = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution2.computeScore(userInputs); //162

    if (studentResult != solutionResult) {
        message += "Did not compute score when goal was found.\n";
    }


// 100, 100
    userInputs = "rrrrrr";
    // find treasure
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    TreasureHunterSolution treasureHunterSolution3 = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution3.computeScore(userInputs); // 66

    if (studentResult != solutionResult) {
        message += "Did not properly find treasure.\n";
    }



    userInputs = "lllllll";
    // fight enemy - stop movement
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution.computeScore(userInputs); // 42

    if (studentResult != solutionResult) {
        message += "Did not prevent movement when enemy is encountered.\n";
    }


    userInputs = "wasdkl";
    // improper inputs
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    TreasureHunterSolution treasureHunterSolution5 = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution5.computeScore(userInputs); // 43

    if (studentResult != solutionResult) {
        message += "Did not handle improper inputs. Should subtract 1 point for each improper input\n";
    }



    userInputs = "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
    // lose game
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    TreasureHunterSolution treasureHunterSolution6 = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution6.computeScore(userInputs); // 0

    if (studentResult != solutionResult) {
        message += "Did not end game at score of 0.\n";
    }


    if (message == "") {
        test2->passedTest();
    } else {
        test2->failedTest(message);
    }
}


void testQ3(void) {
    Test *test3 = new Test("CompleteFuntionalityTest");
    test3->setWorth(1);
    suite.addTest(test3);
    string message = "";
    GameBoard theBoard = getBoard1();


    string userInputs = "rrrrruuuuuuuawdlkjaregljkve";
    // ignore inputs after goal
    theBoard = getBoard1();
    TreasureHunter treasureHunter = TreasureHunter(0, 0, theBoard);
    int studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard1();
    TreasureHunterSolution treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    int solutionResult = treasureHunterSolution.computeScore(userInputs); // 162

    if (studentResult != solutionResult) {
        message += "Did not ignore inputs after goal was found.";
    }

    userInputs = "rrrrrrlll";
    // find same treasure twice - edge case
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution.computeScore(userInputs); // 60

    if (studentResult != solutionResult) {
        message += "Did not remove treasure from board after collecting.\n";
    }

// -4
    userInputs = "lllllllarruuu";
    // fight enemy - attack
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    TreasureHunterSolution treasureHunterSolution4 = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution4.computeScore(userInputs); // 47

    if (studentResult != solutionResult) {
        message += "Did not handle combat properly.\n";
    }

    userInputs = "lllllllarluuuuuuuuu";
    // fight enemy - remove
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution.computeScore(userInputs); // 50

    if (studentResult != solutionResult) {
        message += "Did not remove enemy after KO.\n";
    }

    userInputs = "uuddlrlrballlllllllllll";
    // Konami
    theBoard = getBoard2();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard2();
    treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution.computeScore(userInputs); // 20

    if (studentResult != solutionResult) {
        message += "Did not activate incibility after Konami code is entered.\n";
    }

    userInputs = "dlruuuuyllaruauuuluuulll";
    // all of it
    theBoard = getBoard3();
    treasureHunter = TreasureHunter(0, 0, theBoard);
    studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard3();
    treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution.computeScore(userInputs); // 183

    if (studentResult != solutionResult) {
        message += "Game features not working when all situtaions are combined in a single simulation.\n";
    }

    if (message == "") {
        test3->passedTest();
    } else {
        test3->failedTest(message);
    }
}


void testQ4(void) {
    Test *test4 = new Test("EfficiencyTest");
    test4->setWorth(1);
    suite.addTest(test4);


    string userInputs = "rrrrruuuuuuu";
    // Found goal
    GameBoard theBoard = getBoard1();
    TreasureHunter treasureHunter = TreasureHunter(0, 0, theBoard);
    int studentResult = treasureHunter.computeScore(userInputs);

    theBoard = getBoard1();
    TreasureHunterSolution treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    int solutionResult = treasureHunterSolution.computeScore(userInputs); // 162

    if (studentResult != solutionResult) {
        test4->failedTest("Game simulation not funtioning.");
        return;
    }

    GameBoard hugeBoard = getHugeBoard(10000, 10000);

    TreasureHunter treasureHunter2(0, 0, hugeBoard);
    int swaps = 3;

    clock_t startTime = clock();

    for (int i = 0; i < swaps; i++) {
        treasureHunter2.changeBoard(hugeBoard);
        treasureHunter2.changeBoard(&hugeBoard);
    }

    clock_t endTime = clock();


    userInputs = "rrrruuuu";
    // Found goal
    GameBoard newBoard1 = getBoard1Point1();
    treasureHunter2.changeBoard(&newBoard1);
    studentResult = treasureHunter2.computeScore(userInputs);

    theBoard = getBoard1Point1();
    treasureHunterSolution = TreasureHunterSolution(0, 0, theBoard);
    solutionResult = treasureHunterSolution.computeScore(userInputs); // 162

    if (studentResult != solutionResult) {
        test4->failedTest("Game simulation not funtioning after swapping boards.");
        return;
    }

    test4->setRuntime(calculateRuntime(startTime, endTime));
    if (calculateRuntime(startTime, endTime) < 10) {
        test4->passedTest();
    } else {
        test4->failedTest(
                "TreasureHunter took too long to swap large boards. Be sure to use pointers and references to avoid copying large boards.");
    }

}

int main(int argc, char *argv[]) {
    //main testing logic
    double runtime;
    for (int i = 1; i < argc; i++) {
        switch (atoi(argv[i])) {
            case 1:
                cout << "Running testQ1" << endl;
                suppressCout();
                testQ1();
                restoreCout();
                break;
            case 2:
                cout << "Running testQ2" << endl;
                suppressCout();
                testQ2();
                restoreCout();
                break;
            case 3:
                cout << "Running testQ3" << endl;
                suppressCout();
                testQ3();
                restoreCout();
                break;
            case 4:
                cout << "Running testQ4" << endl;
                suppressCout();
                testQ4();
                restoreCout();
                break;
            default:
                cout << "No test found matching input" << endl;
        }
    }
    //Couts for feedback results. Should be standard for each assignment.
    cout << suite.results();
    cout << "Success Rate: " << suite.percentage() << "/4" << endl;
    cout << "Total Runtime: " << suite.totalRuntime() << "ms" << endl;
    cout << endl;
    return 0;

}
