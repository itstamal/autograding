#include <string>
#include "GameLogicSolution.h"

/*
 *Returns whether or not the Konami Code was contained in the input
 */
bool GameLogicSolution::containsKonamiCode(std::string inputs) {
    return inputs.find("uuddlrlrba") != string::npos;
}
