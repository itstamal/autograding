import sys


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("improper usage")
        exit()
    referenceFilename = sys.argv[1]
    studentFilename = sys.argv[2]

    solutionList = []

    with open(referenceFilename, "r") as referenceFile:
        with open(studentFilename, "r") as studentFile:
            for line in referenceFile:
                solutionList.append(line.strip())
            for line in studentFile:
                for link in solutionList:
                    if link in line:
                        # this might be the worst test ever. Shouldn't be any false negatives though
                        print("pass")
                        exit()

    print("Did not match " + referenceFilename + " and " + studentFilename)
