#include "gameUtil.h"
#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>

int GameUtil::compute(Square* square, Player* player){
  double chi = player->getChi();
  double weight = player->getWeight();
  double teleporterEnergy = square->getTeleporterEnergy();
  double cannonPowder = square->getCannonPowder();

  double teleporterCompute = 0;
  for(int i = 0; i <= (int)chi; i = i + 1){
    teleporterCompute += sqrt(chi * i * teleporterEnergy);
  }
  teleporterCompute *= 1/(1 + chi);

  double cannonCompute = pow((pow(cannonPowder, 1.7)/pow(weight,1.5)),2)/9.8;
  return std::max(cannonCompute > teleporterCompute ? (int)cannonCompute : (int)teleporterCompute, 1);
}

bool GameUtil::isValidPath(std::vector<int> path, Player* player, Game* game){
  std::vector<Square*> board = game->getBoard();
  if(path.size() == 0 || path.back() != board.size() - 1 || path.at(0) != 0) return false;

  int* computeArray = new int[board.size()];
  for(size_t i = 0; i < board.size(); ++i){
    computeArray[i] = compute(board[i], player);
  }

  for(size_t i = 1; i < path.size(); ++i){
    if(path[i] <= path[i-1] || path[i] >= board.size() || path[i] - path[i-1] > computeArray[i-1]) return false;
  }

  delete[] computeArray;
  return true;
}

int GameUtil::shortestPathDistance(Game* game, Player* player){
  std::vector<Square*> board = game->getBoard();
  if(board.size() == 1){
    return 0;
  }

  int* computeArray = new int[board.size()];
  for(size_t i = 0; i < board.size(); ++i){
    computeArray[i] = compute(board[i], player);
  }

  std::vector<int> distances(board.size(), board.size());
  distances.back() = 0;
  for(int i = distances.size() - 2; i >= 0; --i){
    for(int j = i + 1; j < distances.size() && j-i <= computeArray[i]; ++j){
      if(distances[i] > distances[j] + 1 < ){
        distances[i] = distances[j] + 1;
      }
    }
  }
  delete[] computeArray;
  return distances[0];
}

/*]int shortestPathDistanceSlowHelper(int* computeArray, int index, int size){
  if(index >= size - 1){
    return 0;
  }
  int min = size;
  int remainingDist;
  for(int i = index + 1; i <= index + computeArray[index]; ++i){
    remainingDist = shortestPathDistanceSlowHelper(computeArray, i, size);
    if(remainingDist < min){
      min = remainingDist;
    }
  }
  return 1 + min;
}

int GameUtil::shortestPathDistance(Game* game, Player* player){
  std::vector<Square*> board = game->getBoard();
  if(board.size() == 1){
    return 0;
  }
  int* computeArray = new int[board.size()];
  for(size_t i = 0; i < board.size(); ++i){
    computeArray[i] = compute(board[i], player);
  }
  return shortestPathDistanceSlowHelper(computeArray, 0, board.size());
}
*/
