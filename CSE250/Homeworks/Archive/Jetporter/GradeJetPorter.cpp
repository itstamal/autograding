#include "square.h"
#include "game.h"
#include "player.h"
#include "gameUtil.h"
#include "TestSuite.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <math.h>
#include <algorithm>

TestSuite suite;
TestSuite suite2;
TestSuite suite3;

int computeGrade(Square* square, Player* player){
  double chi = player->getChi();
  double weight = player->getWeight();
  double teleporterEnergy = square->getTeleporterEnergy();
  double cannonPowder = square->getCannonPowder();

  double teleporterCompute = 0;
  for(int i = 0; i <= (int)chi; i = i + 1){
    teleporterCompute += sqrt(chi * i * teleporterEnergy);
  }
  teleporterCompute *= 1/(1 + chi);

  double cannonCompute = pow((pow(cannonPowder, 1.7)/pow(weight,1.5)),2)/9.8;
  return std::max(cannonCompute > teleporterCompute ? (int)cannonCompute : (int)teleporterCompute, 1);
}

bool isValidPathGrade(std::vector<int> path, Player* player, Game* game){
  std::vector<Square*> board = game->getBoard();
  if(path.size() == 0 || path.back() != board.size() - 1 || path.at(0) != 0) return false;

  int* computeArray = new int[board.size()];
  for(size_t i = 0; i < board.size(); ++i){
    computeArray[i] = computeGrade(board[i], player);
  }

  for(size_t i = 1; i < path.size(); ++i){
    if(path[i] <= path[i-1] || path[i] >= board.size() || path[i] - path[i-1] > computeArray[i-1]) return false;
  }

  delete[] computeArray;
  return true;
}

int shortestPathDistanceGrade(Game* game, Player* player){
  std::vector<Square*> board = game->getBoard();
  if(board.size() == 1){
    return 0;
  }

  int* computeArray = new int[board.size()];
  for(size_t i = 0; i < board.size(); ++i){
    computeArray[i] = computeGrade(board[i], player);
  }

  std::vector<int> distances(board.size(), board.size());
  distances.back() = 0;
  for(int i = distances.size() - 2; i >= 0; --i){
    for(int j = i + 1; j < distances.size() && j-i <= computeArray[i]; ++j){
      if(distances[j] + 1 < distances[i]){
        distances[i] = distances[j] + 1;
      }
    }
  }
  delete[] computeArray;
  return distances[0];
}

void testQ1a(void){
  Test *testa = new Test("Q1-a");
  testa->setWorth(33);
  suite.addTest(testa);

  GameUtil *util = new GameUtil();
  Player *player1 = new Player(9.0,2.0);
  Square *square1 = new Square(21.0,11.0);

  if(util->compute(square1, player1) == computeGrade(square1, player1)){
    testa->passedTest();
  } else {
    testa->failedTest("Wrong number");
  }
}

void testQ1b(void){
  Test *testb = new Test("Q1-b");
  testb->setWorth(33);
  suite.addTest(testb);

  GameUtil *util = new GameUtil();
  Player *player1 = new Player(9.0,2.0);
  Square *square1 = new Square(14.0,21.0);

  if(util->compute(square1, player1) == computeGrade(square1, player1)){
    testb->passedTest();
  } else {
    testb->failedTest("Wrong number");
  }

}

void testQ1c(void){
  Test *testc = new Test("Q1-c");
  testc->setWorth(34);
  suite.addTest(testc);

  GameUtil *util = new GameUtil();
  Player *player1 = new Player(9.0,2.0);
  Square *square1 = new Square(0.0,0.0);

  if(util->compute(square1, player1) == computeGrade(square1, player1)){
    testc->passedTest();
  } else {
    testc->failedTest("Wrong number");
  }
}

void testQ2a(void){
  Test *testa = new Test("Q2-a");
  testa->setWorth(20);
  suite2.addTest(testa);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));

  Game* game1 = new Game(game_board);
  Player* player1 = new Player(9, 2);

  std::vector<Square*> board = game1->getBoard();

  GameUtil* util = new GameUtil();

  std::vector<int> path;

  path.push_back(0);
  path.push_back(1);
  path.push_back(2);
  path.push_back(3);
  path.push_back(4);
  path.push_back(5);
  path.push_back(6);
  path.push_back(7);
  path.push_back(8);
  path.push_back(9);
  path.push_back(10);
  path.push_back(11);
  path.push_back(12);
  path.push_back(13);
  path.push_back(14);
  path.push_back(15);
  path.push_back(16);
  path.push_back(17);
  path.push_back(18);
  path.push_back(19);
  path.push_back(20);
  path.push_back(21);
  path.push_back(22);
  path.push_back(23);
  path.push_back(24);
  path.push_back(25);
  path.push_back(26);
  path.push_back(27);
  path.push_back(28);
  path.push_back(29);
  path.push_back(30);
  path.push_back(31);
  path.push_back(32);
  path.push_back(33);
  path.push_back(34);
  path.push_back(35);

  if(util->isValidPath(path, player1, game1) == isValidPathGrade(path, player1, game1)){
    testa->passedTest();
  } else {
    testa->failedTest("Function returned false when path is valid");
  }
}

void testQ2b(void){
  Test *testb = new Test("Q2-b");
  testb->setWorth(20);
  suite2.addTest(testb);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));

  Game* game1 = new Game(game_board);
  Player* player1 = new Player(9, 2);

  std::vector<Square*> board = game1->getBoard();

  GameUtil* util = new GameUtil();

  std::vector<int> path;

  path.push_back(0);
  path.push_back(1);
  path.push_back(2);
  path.push_back(3);
  path.push_back(4);
  path.push_back(5);
  path.push_back(6);
  path.push_back(7);
  path.push_back(8);
  path.push_back(9);
  path.push_back(10);
  path.push_back(11);
  path.push_back(12);
  path.push_back(13);
  path.push_back(14);
  path.push_back(15);
  path.push_back(16);
  path.push_back(17);
  path.push_back(18);
  path.push_back(19);
  path.push_back(20);
  path.push_back(21);
  path.push_back(22);
  path.push_back(23);
  path.push_back(24);
  path.push_back(25);
  path.push_back(26);
  path.push_back(27);
  path.push_back(28);
  path.push_back(29);
  path.push_back(30);
  path.push_back(31);
  path.push_back(32);
  path.push_back(33);
  path.push_back(36);
  path.push_back(35);

  if(util->isValidPath(path, player1, game1) == isValidPathGrade(path, player1, game1)){
    testb->passedTest();
  } else {
    testb->failedTest("Function returned true when path is invalid");
  }
}
void testQ2c(void){
  Test *testc = new Test("Q2-c");
  testc->setWorth(20);
  suite2.addTest(testc);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));

  Game* game1 = new Game(game_board);
  Player* player1 = new Player(9, 2);

  std::vector<Square*> board = game1->getBoard();

  GameUtil* util = new GameUtil();

  std::vector<int> path;

  path.push_back(0);
  path.push_back(4);
  path.push_back(5);
  path.push_back(6);
  path.push_back(7);
  path.push_back(8);
  path.push_back(9);
  path.push_back(10);
  path.push_back(11);
  path.push_back(12);
  path.push_back(13);
  path.push_back(14);
  path.push_back(15);
  path.push_back(16);
  path.push_back(17);
  path.push_back(18);
  path.push_back(19);
  path.push_back(20);
  path.push_back(21);
  path.push_back(22);
  path.push_back(23);
  path.push_back(24);
  path.push_back(25);
  path.push_back(26);
  path.push_back(27);
  path.push_back(28);
  path.push_back(29);
  path.push_back(30);
  path.push_back(31);
  path.push_back(32);
  path.push_back(33);
  path.push_back(34);
  path.push_back(35);

  if(util->isValidPath(path, player1, game1) == isValidPathGrade(path, player1, game1)){
    testc->passedTest();
  } else {
    testc->failedTest("Function returned true when path is invalid");
  }
}

void testQ2d(void){
  Test *testd = new Test("Q2-d");
  testd->setWorth(20);
  suite2.addTest(testd);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));

  Game* game1 = new Game(game_board);
  Player* player1 = new Player(9, 2);

  std::vector<Square*> board = game1->getBoard();

  GameUtil* util = new GameUtil();

  std::vector<int> path;

  path.push_back(0);
  path.push_back(1);
  path.push_back(2);
  path.push_back(3);
  path.push_back(4);
  path.push_back(5);
  path.push_back(6);
  path.push_back(7);
  path.push_back(8);
  path.push_back(9);
  path.push_back(10);
  path.push_back(11);
  path.push_back(12);
  path.push_back(13);
  path.push_back(14);
  path.push_back(15);
  path.push_back(16);
  path.push_back(17);
  path.push_back(18);
  path.push_back(19);
  path.push_back(20);
  path.push_back(21);
  path.push_back(22);
  path.push_back(23);
  path.push_back(24);
  path.push_back(25);
  path.push_back(26);
  path.push_back(27);
  path.push_back(28);
  path.push_back(29);
  path.push_back(30);
  path.push_back(31);
  path.push_back(32);
  path.push_back(33);
  path.push_back(34);

  if(util->isValidPath(path, player1, game1) == isValidPathGrade(path, player1, game1)){
    testd->passedTest();
  } else {
    testd->failedTest("Function returned true when path is invalid");
  }
}

void testQ2e(void){
  Test *teste = new Test("Q2-e");
  teste->setWorth(20);
  suite2.addTest(teste);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));

  Game* game1 = new Game(game_board);
  Player* player1 = new Player(9, 2);

  std::vector<Square*> board = game1->getBoard();

  GameUtil* util = new GameUtil();

  std::vector<int> path;

  path.push_back(1);
  path.push_back(2);
  path.push_back(3);
  path.push_back(4);
  path.push_back(5);
  path.push_back(6);
  path.push_back(7);
  path.push_back(8);
  path.push_back(9);
  path.push_back(10);
  path.push_back(11);
  path.push_back(12);
  path.push_back(13);
  path.push_back(14);
  path.push_back(15);
  path.push_back(16);
  path.push_back(17);
  path.push_back(18);
  path.push_back(19);
  path.push_back(20);
  path.push_back(21);
  path.push_back(22);
  path.push_back(23);
  path.push_back(24);
  path.push_back(25);
  path.push_back(26);
  path.push_back(27);
  path.push_back(28);
  path.push_back(29);
  path.push_back(30);
  path.push_back(31);
  path.push_back(32);
  path.push_back(33);
  path.push_back(34);
  path.push_back(35);

  if(util->isValidPath(path, player1, game1) == isValidPathGrade(path, player1, game1)){
    teste->passedTest();
  } else {
    teste->failedTest("Function returned true when path is invalid");
  }
}

void testQ3a(void){
  Test *testa = new Test("Q3-a");
  testa->setWorth(33);
  suite3.addTest(testa);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  /*
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
*/
  Game* game1 = new Game(game_board);
  Player* player1 = new Player(8, 1.5);

  GameUtil* util = new GameUtil();
  clock_t clock1 = clock();
  bool passed = (shortestPathDistanceGrade(game1, player1) == util->shortestPathDistance(game1, player1));
  clock_t clock2 = clock(); 
  if(passed && calculateRuntime(clock1, clock2) < 250){
    testa->passedTest();
  } else {
    testa->failedTest("Wrong distance or too slow.");
  }
}

void testQ3b(void){
  Test *testb = new Test("Q3-b");
  testb->setWorth(33);
  suite3.addTest(testb);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  /*
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
*/
  Game* game1 = new Game(game_board);
  Player* player1 = new Player(9, 2);

  GameUtil* util = new GameUtil();
  clock_t clock1 = clock();
  bool passed = (shortestPathDistanceGrade(game1, player1) == util->shortestPathDistance(game1, player1));
  clock_t clock2 = clock(); 
  if(passed && calculateRuntime(clock1, clock2) < 250){
    testb->passedTest();
  } else {
    testb->failedTest("Wrong distance or too slow.");
  }
}

void testQ3c(void){
  Test *testc = new Test("Q3-c");
  testc->setWorth(34);
  suite3.addTest(testc);

  std::vector<Square*> game_board;

  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(14,11));
  game_board.push_back(new Square(17,9));
  game_board.push_back(new Square(21,11));
  game_board.push_back(new Square(14,21));
  game_board.push_back(new Square(12,4));
  game_board.push_back(new Square(11,12));
  game_board.push_back(new Square(20,4));
  game_board.push_back(new Square(20,6));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  /*
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
  game_board.push_back(new Square(10,10));
 */ 

  Game* game1 = new Game(game_board);
  Player* player1 = new Player(7, 4);
  GameUtil* util = new GameUtil();
  clock_t clock1 = clock();
  bool passed = (shortestPathDistanceGrade(game1, player1) == util->shortestPathDistance(game1, player1));
  clock_t clock2 = clock(); 
  if(passed && calculateRuntime(clock1, clock2) < 250){
    testc->passedTest();
  } else {
    testc->failedTest("Wrong distance or too slow.");
  }
}

int main(){
  double runtime;
  suite.setWorth(33);
  suite2.setWorth(33);
  suite3.setWorth(34);
  
  streambuf *old = cout.rdbuf();
  stringstream ss;
  cout.rdbuf (ss.rdbuf()); 
  
  testQ1a();
  testQ1b();
  testQ1c();
  cout.rdbuf (old);
  cout << suite.results();
  cout << "Results for part 1: " << suite.percentage() << "%" << endl;

  cout.rdbuf (ss.rdbuf()); 
  testQ2a();
  testQ2b();
  testQ2c();
  testQ2d();
  testQ2e();
  cout.rdbuf (old);
  cout << suite2.results();
  cout << "Results for part 2: " << suite2.percentage() << "%" << endl;
  
  cout.rdbuf (ss.rdbuf()); 
  clock_t clock1 = clock();
  testQ3a();
  testQ3b();
  testQ3c();
  cout.rdbuf (old);
  clock_t clock2 = clock();
  cout << suite3.results();
  cout << "Results for part 3: " << suite3.percentage() << "%" << endl;
  
  cout << "Success Rate: " << averageSuites(suite, suite2, suite3) << "%" << " Runtime: " << calculateRuntime(clock1, clock2) << " milliseconds.";

  return 0;
}
