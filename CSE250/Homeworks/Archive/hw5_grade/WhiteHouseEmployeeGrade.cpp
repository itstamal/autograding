#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include "Employee.h"
#include "FilterGrade.h"
#include "Filter.h"
#include "TestSuite.h"
using namespace std;

TestSuite suite1;
TestSuite suite2;
TestSuite suite3;

bool compareFiles(string firstFile, string secondFile);
void getUserInput(vector <vector<Employee *> *>  &sortedByDataFieldVector);
template <typename FileStream>
void openFile(FileStream& file, string filename);
void testQ1a(void);
void testQ1b(void);

void testQ2a(void);
void testQ2b(void);
void testQ2c(void);


void testQ3a(void);
void testQ3b(void);
void testQ3c(void);
void testQ3d(void);
void testQ3e(void);
void testQ3f(void);





int main(){
	double runtime;
	suite1.setWorth(20);
	suite2.setWorth(30);
	suite3.setWorth(50);

	streambuf *old = cout.rdbuf();
	stringstream ss;
	cout.rdbuf(ss.rdbuf());

	testQ1a();
	testQ1b();

	cout.rdbuf(old);
	cout << suite1.results();
	cout << "Results for part 1: " << suite1.percentage() << "%" << endl;

	cout.rdbuf(ss.rdbuf());
	testQ2a();
	testQ2b();
	testQ2c();
	
	cout.rdbuf(old);
	cout << suite2.results();
	cout << "Results for part 2: " << suite2.percentage() << "%" << endl;


	cout.rdbuf(ss.rdbuf());
	testQ3a();
	testQ3b();
	testQ3c();
	testQ3d();
	testQ3e();
	testQ3f();

	cout.rdbuf(old);
	cout << suite3.results();
	cout << "Results for part 3: " << suite3.percentage() << "%" << endl;

	/*cout.rdbuf(ss.rdbuf());
	clock_t clock1 = clock();
	testQ3a();
	testQ3b();
	testQ3c();
	cout.rdbuf(old);
	clock_t clock2 = clock();
	cout << suite3.results();
	cout << "Results for part 3: " << suite3.percentage() << "%" << endl;

	cout << "Success Rate: " << averageSuites(suite, suite2, suite3) << "%" << " Runtime: " << calculateRuntime(clock1, clock2) << " milliseconds.";*/


	return 0;
}


int second()
{
	ifstream inputFile;
   vector<string> filenames;
   string filenamearray[6] =
   {
      "2010.csv",
      "2011.csv",
      "2012.csv",
      "2013.csv",
      "2014.csv",
   };

   ofstream outputFile;
   ofstream outputFileBeforeSort;
   ofstream outputFileBeforeSortGrading;
   ofstream outputFileAfterSort;
   ofstream outputFileAfterSortGrading;

   inputFile.open(filenamearray[0]);
   // We assume argv[2] is a filename to open for output
   outputFile.open("TestGrade.rpt");
   // argv[3] is sorted by
   DATA_FIELDS sortedBy = DATA_FIELDS(0);

   bool isAscending = true;

   // Always check to see if file opening succeeded
   if (!inputFile.is_open())
   {
      cout << "Could not open the input file \n";
      exit(0);
   }
   // Always check to see if file opening succeeded
   if (!outputFile.is_open())
   {
      cout << "Could not open the output file \n";
      exit(0);
   }

   string lineFromFile;
   getline(inputFile, lineFromFile);

   vector<Employee *> employeeVector;

   while (getline(inputFile, lineFromFile))
   {

	  Employee *pEmployee = new Employee(lineFromFile);
	  employeeVector.push_back(pEmployee);

   }

   // TODO: Perform the sorting based on the criteria passed
   // Add functionality to Employee.h or Filter.h file (outside the class declaration).

   CompareClass compareClass(sortedBy, isAscending);
   std::sort(employeeVector.begin(), employeeVector.end(), compareClass);

  
   outputFile << getEmployeeFileHeader();
   for (size_t i = 0; i < employeeVector.size(); i++)
   {
      outputFile << *(employeeVector[i]);
   }
   outputFile.close();

   vector <vector<Employee *> *>  sortedByDataFieldVector;

   for (int i = NAME; i <= POSITION; i++)
   {
      vector <Employee *> *pEmployeeVector = new vector <Employee *>(employeeVector.begin(), employeeVector.end());
      CompareClass compareClass(static_cast<DATA_FIELDS>(i));
      std::sort(pEmployeeVector->begin(), pEmployeeVector->end(), compareClass);
      sortedByDataFieldVector.push_back(pEmployeeVector);
   }

   while (true)
   {

      getUserInput(sortedByDataFieldVector);

   }

   int x = 8;

}


unsigned get_uint_as_Input()
{
   unsigned int userInput;
   //executes loop if the input fails (e.g., no characters were read)
   while (!(cin >> userInput)) {
      std::cin.clear(); //clear bad input flag
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //discard input
      std::cout << "Invalid input; please re-enter.\n";
   }
   return userInput;
}

void getUserInput(vector <vector<Employee *> *>  &sortedByDataFieldVector)
{
   cout << "Enter your choice" << endl;
   cout << "1. New query" << endl;
   cout << "2. Quit" << endl;

   

   unsigned int userInput = get_uint_as_Input();
   if (userInput != 1)
   {
      exit(0);
   }

   unsigned int filterType = FIRST;

   Filter *pNewFilter = NULL;

   while (filterType != END)
   {

      cout << " Filter on " << endl;
      cout << " 0. NAME" << endl;
      cout << " 1. STATUS" << endl;
      cout << " 2. SALARY" << endl;
      cout << " 3. PAY_BASIS" << endl;
      cout << " 4. POSITION" << endl;
      cout << " 5. RETURN" << endl;

      userInput = get_uint_as_Input();

      if (userInput > 4)
      {
         return;
      }


      cout << "Select Condition" << endl;

      cout << "0. IS_EQUAL" << endl;
      cout << "1. BETWEEN" << endl;
      cout << "2. GREATER" << endl;
      cout << "3. LESS" << endl;
      cout << "4. RETURN" << endl;

   
      int userCondition = get_uint_as_Input();

      if (userCondition >  3)
      {
         return;
      }

      string rangeStart = "";
      string rangeEnd = "";

      if (userCondition == 1)
      {
         cout << "Insert start and end of range: ";
         cin >> rangeStart >> rangeEnd;
         if (rangeStart > rangeEnd)
         {
            cout << "Start range cannot be greater than End range" << endl;
            return;
         }
      }
      else
      {
         cout << "Insert value: ";
         cin >> rangeStart;
      }

      if (filterType == FIRST)
      {
         if (pNewFilter != NULL)
         {
            delete pNewFilter;
         }
         pNewFilter  = new Filter(sortedByDataFieldVector[userInput], static_cast<DATA_FIELDS>(userInput), static_cast<SELECTION_CRITERIA>(userCondition), make_pair(rangeStart, rangeEnd));
      }
      else
      {
         pNewFilter->addFilter(sortedByDataFieldVector[userInput], static_cast<DATA_FIELDS>(userInput), static_cast<SELECTION_CRITERIA>(userCondition), make_pair(rangeStart, rangeEnd), static_cast<FILTER_TYPE>(filterType));
      }
      
      pNewFilter->printFilter();

      cout << "Add more filters to the existing one? " << endl;
      cout << "1. OR filter " << endl;
      cout << "2. AND filter " << endl;
      cout << "3. End the current filter and display " << endl;

      filterType = get_uint_as_Input();

      if (filterType > 2)
      {
         filterType = END;
         cout << "====================================================================================================================================\n";
         cout << pNewFilter->getPrintFilter();
         cout << "====================================================================================================================================\n";
      }

   }


}
/* Function: compareFiles
* --------------------------------------------------------
* Compares two files byte-by-byte to determine whether or
* not they are equivalent to one another.
*/
bool compareFiles(string firstFile, string secondFile) {
   /* Get the two files to compare. */
   ifstream one, two;
   openFile(one, firstFile);
   openFile(two, secondFile);

   /* Read the contents of the files into a stringstream for
   * comparison.
   */
   ostringstream oneContents, twoContents;
   oneContents << one.rdbuf();
   twoContents << two.rdbuf();

   one.close();
   two.close();

   const string oneString = oneContents.str(), twoString = twoContents.str();

   /* Check lengths are the same. */
   if (oneString.length() != twoString.length()) {
      cout << "Files differ!" << endl;
      cout << "File one has length " << oneString.length() << "." << endl;
      cout << "File two has length " << twoString.length() << "." << endl;
	  return false;
   }
   else {
      /* Compare the two sequences to find a mismatch. */
      pair<string::const_iterator, string::const_iterator> diff =
         mismatch(oneString.begin(), oneString.end(), twoString.begin());
      if (diff.first != oneString.end()) {
         cout << "Files differ!" << endl;
         ptrdiff_t offset = diff.first - oneString.begin();
         cout << "Bytes differ at offset " << offset << "." << endl;
         cout << "File one has value " << *diff.first << endl;
         cout << "File two has value " << *diff.second << endl;
		 return false;
      }
      else {
         /* Files match! */
         cout << "Files match!" << endl;
		 return true;
      }
   }
}

template <typename FileStream>
void openFile(FileStream& file, string filename) {
	//while (true) {

	file.open(filename.c_str());

	if (file.is_open()) return;

	cout << "Sorry, I couldn't open that file." << endl;
	file.clear();

	//}
}

void testQ1a(void){

	Test *testa = new Test("Q1-a");
	testa->setWorth(50);
	suite1.addTest(testa);

	ifstream inputFile;
	ofstream outputFile;

	string inputFileName = "2012.csv";
	string outputFileName = "Test2012.rpt";
	string gradingFileName = "RPT/2012_name_NoSort.rpt";


	inputFile.open(inputFileName);
	outputFile.open(outputFileName);
	// Always check to see if file opening succeeded
	if (!inputFile.is_open())
	{
		cerr << "Could not open the input file \n";
	}
	// Always check to see if file opening succeeded
	if (!outputFile.is_open())
	{
		cerr << "Could not open the output file \n";
	}

	string lineFromFile;
	getline(inputFile, lineFromFile);

	vector<Employee *> employeeVector;
	while (getline(inputFile, lineFromFile))
	{
		Employee *pEmployee = new Employee(lineFromFile);
		employeeVector.push_back(pEmployee);
	}

	outputFile << getEmployeeFileHeader();
	for (size_t i = 0; i < employeeVector.size(); i++)
	{
		outputFile << *(employeeVector[i]);
	}
	outputFile.close();


	bool fileMatched = compareFiles(outputFileName, gradingFileName);
	if (fileMatched)
	{
	
		cerr << "Files Matched \n";
		testa->passedTest();
	}
	else
	{
		cerr << "Files Mismatched \n";
		testa->failedTest("Bad Formatting. Output mismatched");
	}
}


void testQ1b(void){

	Test *testb = new Test("Q1-b");
	testb->setWorth(50);
	suite1.addTest(testb);

	ifstream inputFile;
	ofstream outputFile;

	string inputFileName = "2014.csv";
	string outputFileName = "Test2014.rpt";
	string gradingFileName = "RPT/2014_name_NoSort.rpt";


	inputFile.open(inputFileName);
	outputFile.open(outputFileName);
	// Always check to see if file opening succeeded
	if (!inputFile.is_open())
	{
		cerr << "Could not open the input file \n";
	}
	// Always check to see if file opening succeeded
	if (!outputFile.is_open())
	{
		cerr << "Could not open the output file \n";
	}

	string lineFromFile;
	getline(inputFile, lineFromFile);

	vector<Employee *> employeeVector;
	while (getline(inputFile, lineFromFile))
	{
		Employee *pEmployee = new Employee(lineFromFile);
		employeeVector.push_back(pEmployee);
	}

	outputFile << getEmployeeFileHeader();
	for (size_t i = 0; i < employeeVector.size(); i++)
	{
		outputFile << *(employeeVector[i]);
	}
	outputFile.close();


	bool fileMatched = compareFiles(outputFileName, gradingFileName);
	if (fileMatched)
	{

		// cerr << "Files Matched \n";
		testb->passedTest();
	}
	else
	{
		// cerr << "Files Mismatched \n";
		testb->failedTest("Bad Formatting. Output mismatched");
	}
}

void testQ2a(void){

	Test *testa = new Test("Q2-a");
	testa->setWorth(10);
	suite2.addTest(testa);

	ifstream inputFile;
	ofstream outputFile;

	string inputFileName = "2013.csv";
	string outputFileName = "Test2013.rpt";
	string gradingFileName = "RPT/2013_salary_Des.rpt";

	DATA_FIELDS sortedBy = DATA_FIELDS(2);
	bool isAscending = false;


	inputFile.open(inputFileName);
	outputFile.open(outputFileName);
	// Always check to see if file opening succeeded
	if (!inputFile.is_open())
	{
		cerr << "Could not open the input file \n";
	}
	// Always check to see if file opening succeeded
	if (!outputFile.is_open())
	{
		cerr << "Could not open the output file \n";
	}

	string lineFromFile;
	getline(inputFile, lineFromFile);

	vector<Employee *> employeeVector;
	while (getline(inputFile, lineFromFile))
	{
		Employee *pEmployee = new Employee(lineFromFile);
		employeeVector.push_back(pEmployee);
	}

	CompareClass compareClass(sortedBy, isAscending);
	std::sort(employeeVector.begin(), employeeVector.end(), compareClass);

	outputFile << getEmployeeFileHeader();
	for (size_t i = 0; i < employeeVector.size(); i++)
	{
		outputFile << *(employeeVector[i]);
	}
	outputFile.close();


	bool fileMatched = compareFiles(outputFileName, gradingFileName);
	if (fileMatched)
	{

		// cerr << "Files Matched \n";
		testa->passedTest();
	}
	else
	{
		// cerr << "Files Mismatched \n";
		testa->failedTest("Reason: Descending sorting by salary 2013 File");
	}
}


void testQ2b(void){

	Test *testb = new Test("Q2-b");
	testb->setWorth(10);
	suite2.addTest(testb);

	ifstream inputFile;
	ofstream outputFile;

	string inputFileName = "2014.csv";
	string outputFileName = "Test2014.rpt";
	string gradingFileName = "RPT/2014_name_Des.rpt";

	DATA_FIELDS sortedBy = DATA_FIELDS(0);
	bool isAscending = false;


	inputFile.open(inputFileName);
	outputFile.open(outputFileName);
	// Always check to see if file opening succeeded
	if (!inputFile.is_open())
	{
		cerr << "Could not open the input file \n";
	}
	// Always check to see if file opening succeeded
	if (!outputFile.is_open())
	{
		cerr << "Could not open the output file \n";
	}

	string lineFromFile;
	getline(inputFile, lineFromFile);

	vector<Employee *> employeeVector;
	while (getline(inputFile, lineFromFile))
	{
		Employee *pEmployee = new Employee(lineFromFile);
		employeeVector.push_back(pEmployee);
	}

	CompareClass compareClass(sortedBy, isAscending);
	std::sort(employeeVector.begin(), employeeVector.end(), compareClass);

	outputFile << getEmployeeFileHeader();
	for (size_t i = 0; i < employeeVector.size(); i++)
	{
		outputFile << *(employeeVector[i]);
	}
	outputFile.close();


	bool fileMatched = compareFiles(outputFileName, gradingFileName);
	if (fileMatched)
	{

		// cerr << "Files Matched \n";
		testb->passedTest();
	}
	else
	{
		// cerr << "Files Mismatched \n";
		testb->failedTest("Reason: Descending sorting by name 2014 File");
	}
}

void testQ2c(void){

	Test *testc = new Test("Q2-c");
	testc->setWorth(10);
	suite2.addTest(testc);

	ifstream inputFile;
	ofstream outputFile;

	string inputFileName = "2012.csv";
	string outputFileName = "Test2012.rpt";
	string gradingFileName = "RPT/2012_pos_Asc.rpt";

	DATA_FIELDS sortedBy = DATA_FIELDS(4);
	bool isAscending = true;


	inputFile.open(inputFileName);
	outputFile.open(outputFileName);
	// Always check to see if file opening succeeded
	if (!inputFile.is_open())
	{
		cerr << "Could not open the input file \n";
	}
	// Always check to see if file opening succeeded
	if (!outputFile.is_open())
	{
		cerr << "Could not open the output file \n";
	}

	string lineFromFile;
	getline(inputFile, lineFromFile);

	vector<Employee *> employeeVector;
	while (getline(inputFile, lineFromFile))
	{
		Employee *pEmployee = new Employee(lineFromFile);
		employeeVector.push_back(pEmployee);
	}

	CompareClass compareClass(sortedBy, isAscending);
	std::sort(employeeVector.begin(), employeeVector.end(), compareClass);

	outputFile << getEmployeeFileHeader();
	for (size_t i = 0; i < employeeVector.size(); i++)
	{
		outputFile << *(employeeVector[i]);
	}
	outputFile.close();


	bool fileMatched = compareFiles(outputFileName, gradingFileName);
	if (fileMatched)
	{

		// cerr << "Files Matched \n";
		testc->passedTest();
	}
	else
	{
		// cerr << "Files Mismatched \n";
		testc->failedTest("Reason: Ascending sorting by position 2012 File");
	}
}

void coreTest3(Test *testN, string inputFileName, int sortedBy, SELECTION_CRITERIA sel_criteria, string rangeStart, string rangeEnd, 
	FILTER_TYPE filterType = FILTER_TYPE::FIRST, int sortedBySecond = 0, SELECTION_CRITERIA sel_criteriaSecond=SELECTION_CRITERIA::IS_EQUAL , string rangeStartSecond="", string rangeEndSecond="")
{
	ifstream inputFile;
	inputFile.open(inputFileName);

	// Always check to see if file opening succeeded
	if (!inputFile.is_open())
	{
		cerr << "Could not open the input file \n";
	}


	string lineFromFile;
	getline(inputFile, lineFromFile);

	vector<Employee *> employeeVector;
	while (getline(inputFile, lineFromFile))
	{
		Employee *pEmployee = new Employee(lineFromFile);
		employeeVector.push_back(pEmployee);
	}

	vector <vector<Employee *> *>  sortedByDataFieldVector;

	for (int i = NAME; i <= POSITION; i++)
	{
		vector <Employee *> *pEmployeeVector = new vector <Employee *>(employeeVector.begin(), employeeVector.end());
		CompareClass compareClass(static_cast<DATA_FIELDS>(i));
		std::sort(pEmployeeVector->begin(), pEmployeeVector->end(), compareClass);
		sortedByDataFieldVector.push_back(pEmployeeVector);
	}


	Filter *pFilter = new Filter(sortedByDataFieldVector[sortedBy], static_cast<DATA_FIELDS>(sortedBy), static_cast<SELECTION_CRITERIA>(sel_criteria), make_pair(rangeStart, rangeEnd));
	FilterGrade *pFilterGrade = new FilterGrade(sortedByDataFieldVector[sortedBy], static_cast<DATA_FIELDS>(sortedBy), static_cast<SELECTION_CRITERIA>(sel_criteria), make_pair(rangeStart, rangeEnd));

	if ((filterType == FILTER_TYPE::OR) || (filterType == FILTER_TYPE::AND))
	{

		pFilter->addFilter(sortedByDataFieldVector[sortedBySecond], static_cast<DATA_FIELDS>(sortedBySecond), static_cast<SELECTION_CRITERIA>(sel_criteriaSecond), make_pair(rangeStartSecond, rangeEndSecond), filterType);
		pFilterGrade->addFilter(sortedByDataFieldVector[sortedBySecond], static_cast<DATA_FIELDS>(sortedBySecond), static_cast<SELECTION_CRITERIA>(sel_criteriaSecond), make_pair(rangeStartSecond, rangeEndSecond), filterType);
	}

	string studentOutput = pFilter->getPrintFilter();
	string expectedOutput = pFilterGrade->getPrintFilter();

	if (studentOutput == expectedOutput)
	{
	//	cerr << "Files Matched \n";
	//	cerr << studentOutput << endl;
		testN->passedTest();
	}
	else
	{
		// cerr << "Files Mismatched \n";
		testN->failedTest("Reason: Expected output did not match. Tested on " + inputFileName );
		/*+"\n" +
		"Expected output: \n" + expectedOutput + "\n" +
		"Output from program: \n" + studentOutput + "\n" );*/
	}

}

//NAME, STATUS, SALARY, PAY_BASIS, POSITION

// IS_EQUAL,
// BETWEEN,
// GREATER,
// LESS,

//FILTER_TYPE
//FIRST,
//OR,
//AND,
//END,
void testQ3a(void){

	Test *testa = new Test("Q3-a");
	testa->setWorth(5);
	suite3.addTest(testa);

	string inputFileName = "2013.csv";
	int sortedBy = DATA_FIELDS::SALARY;
	SELECTION_CRITERIA sel_criteria = SELECTION_CRITERIA::GREATER;
	string rangeStart = "100000";
	string rangeEnd = "";

	coreTest3(testa, inputFileName, sortedBy, sel_criteria, rangeStart, rangeEnd);

}

void testQ3b(void){

	Test *testb = new Test("Q3-b");
	testb->setWorth(5);
	suite3.addTest(testb);

	string inputFileName = "2014.csv";
	int sortedBy = DATA_FIELDS::NAME;
	SELECTION_CRITERIA sel_criteria = SELECTION_CRITERIA::IS_EQUAL;
	string rangeStart = "Amo,_Jr.,_Gabriel_F.";
	string rangeEnd = "";

	coreTest3(testb, inputFileName, sortedBy, sel_criteria, rangeStart, rangeEnd);

}


void testQ3c(void){

	Test *testc = new Test("Q3-c");
	testc->setWorth(5);
	suite3.addTest(testc);

	string inputFileName = "2012.csv";
	int sortedBy = DATA_FIELDS::SALARY;
	SELECTION_CRITERIA sel_criteria = SELECTION_CRITERIA::BETWEEN;
	string rangeStart = "50000";
	string rangeEnd = "80000";

	coreTest3(testc, inputFileName, sortedBy, sel_criteria, rangeStart, rangeEnd);

}



void testQ3d(void){

	Test *testd = new Test("Q3-d");
	testd->setWorth(5);
	suite3.addTest(testd);

	string inputFileName = "2011.csv";
	int sortedBy = DATA_FIELDS::POSITION;
	SELECTION_CRITERIA sel_criteria = SELECTION_CRITERIA::LESS;
	string rangeStart = "DEPUTY";
	string rangeEnd = "";

	coreTest3(testd, inputFileName, sortedBy, sel_criteria, rangeStart, rangeEnd);

}


void testQ3e(void){

	Test *teste = new Test("Q3-e");
	teste->setWorth(5);
	suite3.addTest(teste);

	string inputFileName = "2013.csv";
	int sortedBy = DATA_FIELDS::NAME;
	SELECTION_CRITERIA sel_criteria = SELECTION_CRITERIA::BETWEEN;
	string rangeStart = "A";
	string rangeEnd = "B";

	// OR filter
	int sortedBySecond = DATA_FIELDS::NAME;
	SELECTION_CRITERIA sel_criteriaSecond = SELECTION_CRITERIA::BETWEEN;
	string rangeStartSecond = "T";
	string rangeEndSecond = "U";

	coreTest3(teste, inputFileName, sortedBy, sel_criteria, rangeStart, rangeEnd, FILTER_TYPE::OR, 
		sortedBySecond, sel_criteriaSecond, rangeStartSecond, rangeEndSecond);

}

void testQ3f(void){

	Test *testf = new Test("Q3-f");
	testf->setWorth(5);
	suite3.addTest(testf);

	string inputFileName = "2010.csv";
	int sortedBy = DATA_FIELDS::STATUS;
	SELECTION_CRITERIA sel_criteria = SELECTION_CRITERIA::IS_EQUAL;
	string rangeStart = "Detailee";
	string rangeEnd = "";

	// AND filter
	int sortedBySecond = DATA_FIELDS::SALARY;
	SELECTION_CRITERIA sel_criteriaSecond = SELECTION_CRITERIA::LESS;
	string rangeStartSecond = "100000";
	string rangeEndSecond = "";

	coreTest3(testf, inputFileName, sortedBy, sel_criteria, rangeStart, rangeEnd, FILTER_TYPE::AND,
		sortedBySecond, sel_criteriaSecond, rangeStartSecond, rangeEndSecond);

}
