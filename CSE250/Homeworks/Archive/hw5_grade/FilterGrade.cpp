#include <iostream> 
#include "FilterGrade.h"


using namespace std;

FilterGrade::FilterGrade(vector<Employee *> *pEmpVector, DATA_FIELDS field, SELECTION_CRITERIA selectCrit, pair<string, string> dataLimit):
field_(field), firstField_(field), selectCrit_(selectCrit)
{
	pEmpVector_ = new vector<Employee *>(pEmpVector->size(), NULL);
	extractData(pEmpVector, dataLimit, FILTER_TYPE::FIRST);
	int x = 8;

}

void FilterGrade::extractData(vector<Employee *> *pEmpVector, pair<string, string> dataLimit, FILTER_TYPE filterType)
{
	string ItemFirst = dataLimit.first;
	string ItemLast;
	if (selectCrit_ == SELECTION_CRITERIA::BETWEEN)
	{
		ItemLast = dataLimit.second;
	}

	std::vector<Employee *> LocalEmployeeVector;
	switch (selectCrit_)
	{
	case IS_EQUAL:
	{
					 pair<vector<Employee *>::iterator, std::vector<Employee *>::iterator> bounds;
					 CompareClass compareClass(field_, true);
					 Employee *pTestEmployee = new Employee();
					 pTestEmployee->setField(field_, ItemFirst);

					 bounds = equal_range(pEmpVector->begin(), pEmpVector->end(), pTestEmployee, compareClass);
					 std::vector<Employee *> TempEmployeeVector(bounds.first, bounds.second);
					 LocalEmployeeVector.swap(TempEmployeeVector);

	}
		break;
	case BETWEEN:
	{
					vector<Employee *>::iterator lowBound, highBound;
					CompareClass compareClass(field_, true);

					Employee *pTestEmployeeLower = new Employee();
					pTestEmployeeLower->setField(field_, ItemFirst);

					Employee *pTestEmployeeHigher = new Employee();
					pTestEmployeeHigher->setField(field_, ItemLast);


					lowBound = lower_bound(pEmpVector->begin(), pEmpVector->end(), pTestEmployeeLower, compareClass);
					highBound = upper_bound(pEmpVector->begin(), pEmpVector->end(), pTestEmployeeHigher, compareClass);
					std::vector<Employee *> TempEmployeeVector(lowBound, highBound);

					LocalEmployeeVector.swap(TempEmployeeVector);
	}
		break;
	case GREATER:
	{
					vector<Employee *>::iterator highBound;
					CompareClass compareClass(field_, true);

					Employee *pTestEmployeeHigher = new Employee();
					pTestEmployeeHigher->setField(field_, ItemFirst);

					highBound = upper_bound(pEmpVector->begin(), pEmpVector->end(), pTestEmployeeHigher, compareClass);
					std::vector<Employee *> TempEmployeeVector(highBound, pEmpVector->end());
					LocalEmployeeVector.swap(TempEmployeeVector);
	}
		break;
	case LESS:
	{
				 vector<Employee *>::iterator lowBound;
				 CompareClass compareClass(field_, true);

				 Employee *pTestEmployeeLower = new Employee();
				 pTestEmployeeLower->setField(field_, ItemFirst);

				 lowBound = lower_bound(pEmpVector->begin(), pEmpVector->end(), pTestEmployeeLower, compareClass);

				 std::vector<Employee *> TempEmployeeVector(pEmpVector->begin(), lowBound);
				 LocalEmployeeVector.swap(TempEmployeeVector);

	}
		break;

	}

	if (filterType == FIRST)
	{
		pEmpVector_ = new vector<Employee *>(LocalEmployeeVector);
	}
	else
	{
		// First sort LocalEmployeeVector based on firstField_
		CompareClass compareClass(NAME, true);
		std::sort(LocalEmployeeVector.begin(), LocalEmployeeVector.end(), compareClass);
		if (firstField_ != NAME)
		{
			std::sort(pEmpVector_->begin(), pEmpVector_->end(), compareClass);
		}
		std::vector<Employee *>::iterator it;

		if (filterType == AND)
		{

			// Do set_intersection
			vector<Employee *> *pEmpVectorIntersection = new vector<Employee *>(pEmpVector_->size(), NULL);
			std::vector<Employee *>::iterator it;
			it = std::set_intersection(pEmpVector_->begin(), pEmpVector_->end(), LocalEmployeeVector.begin(), LocalEmployeeVector.end(), pEmpVectorIntersection->begin(), compareClass);
			pEmpVectorIntersection->resize(it - pEmpVectorIntersection->begin());
			pEmpVector_ = pEmpVectorIntersection;
		}
		else // OR
		{
			// Do set_union
			vector<Employee *> *pEmpVectorUnion = new vector<Employee *>(pEmpVector_->size() + LocalEmployeeVector.size(), NULL);

			it = std::set_union(pEmpVector_->begin(), pEmpVector_->end(), LocalEmployeeVector.begin(), LocalEmployeeVector.end(), pEmpVectorUnion->begin(), compareClass);
			pEmpVectorUnion->resize(it - pEmpVectorUnion->begin());
			pEmpVector_ = pEmpVectorUnion;
		}
	}
}


void FilterGrade::printFilter()
{
   cout << getEmployeeFileHeader();
	for (size_t i = 0; i < pEmpVector_->size(); i++)
	{
		cout << *(pEmpVector_->at(i));
	}
}

void FilterGrade::setValues(vector<Employee *> *pEmpVector, DATA_FIELDS field, SELECTION_CRITERIA selectCrit)
{
	field_ = field;
	selectCrit_ = selectCrit;
}


void FilterGrade::addFilter(vector<Employee *> *pEmpVector, DATA_FIELDS field, SELECTION_CRITERIA selectCrit, pair<string, string> dataLimit, FILTER_TYPE filterType)
{

	setValues(pEmpVector, field, selectCrit);
	extractData(pEmpVector, dataLimit, filterType);

}
