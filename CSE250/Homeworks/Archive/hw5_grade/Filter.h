#ifndef __FILTER_H__
#define __FILTER_H__
#include <set>
#include "Employee.h"
#include <vector>
#include <algorithm>
#include <sstream>

using namespace std;

enum SELECTION_CRITERIA
{
	IS_EQUAL,
	BETWEEN,
	GREATER,
	LESS,
};

enum FILTER_TYPE
{
	FIRST,
	OR,
	AND,
	END,

};

class Filter{
public:
   // constructor
	Filter(vector<Employee *> *pEmpVector, DATA_FIELDS field, SELECTION_CRITERIA selectCrit, pair<string, string> dataLimit);
	
   /****************
    * The most important method of the class
    * Based on the dataLimit or range, it gets a subset of entries from the pEmpVector
    * It does a set set_union or set_intersection of this subset with the existing pEmpVector_.
    * Finally, stores the resultant entries to pEmpVector
    * *********/
   void extractData(vector<Employee *> *pEmpVector, pair<string, string> dataLimit, FILTER_TYPE filterType);
	
   /******
    * Prints the entries stored in pEmpVector_
    * ****/
   void printFilter();

   string getPrintFilter()
   {
      ostringstream oss ;
      // oss  << getEmployeeFileHeader();
      for (size_t i = 0; i < pEmpVector_->size(); i++)
      {
         oss << *(pEmpVector_->at(i));
      }

      return oss.str();

   }
   /************
    * Set pEmpVector_ and selectCrit_
    * ******/
	void setValues(vector<Employee *> *pEmpVector, DATA_FIELDS field, SELECTION_CRITERIA selectCrit);

   /******
    * calls setValues and extractData methods
    * ********/
	void addFilter(vector<Employee *> *pEmpVector, DATA_FIELDS field, SELECTION_CRITERIA selectCrit, pair<string, string> dataLimit, FILTER_TYPE filterType);

   // public memebers 
	DATA_FIELDS field_;
	DATA_FIELDS firstField_; // To check how to sort! For OR or AND filters, remembers the sorting for the first filter.
	SELECTION_CRITERIA selectCrit_;
	vector<Employee *> *pEmpVector_;

};

#endif
