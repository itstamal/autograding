# Make sure filename is "Grade(AssignmentName)" with proper capitalization.
import sys
import datetime
import TestSuite
import importlib
import random
import os
import json
import sqlite3

old_stdout = sys.stdout


def floatCompare(number1, number2):
    return abs(number1 - number2) < 0.01


# helper functions to check correctness (usually renamed solutions dumped in to check against students code)
def suppressCout():
    sys.stdout = open(os.devnull, 'w')


def restoreCout():
    sys.stdout = old_stdout


def genRandom():
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    return alpha[random.randint(0, 51)]


def genString(size):
    str = ""
    for i in range(0, size):
        str += genRandom()
    return str


def testQ1(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q1")
    test1.setworth(1)  # set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    title = genString(20)
    artist = genString(20)
    link = genString(20)
    request = {"requestType": "addSong", "title": title, "artist": artist, "link": link}
    StudentImplementation.handleRequest(request)

    passed = False
    connection = sqlite3.connect('songDatabase.db')
    c = connection.cursor()
    c.execute("SELECT * FROM songs")
    for row in c:
        if row[1] == title and row[2] == artist and row[3] == link:
            passed = True
    connection.commit()
    connection.close()

    if not passed:
        test1.failedtest("Error in part 1")
    else:
        test1.passedtest()
    test1.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ2(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test2 = TestSuite.Test("Q2")
    test2.setworth(1)  # set point worth of the test
    suite.addtest(test2)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    title = genString(20)
    artist = genString(20)
    link = genString(20)
    request = {"requestType": "addSong", "title": title, "artist": artist, "link": link}
    StudentImplementation.handleRequest(request)

    passed = False
    allSongs = StudentImplementation.handleRequest({"requestType": "getAllSongs"})
    allSongs = json.loads(allSongs)

    for songID in allSongs:
        row = allSongs[songID]
        if row[0] == title and row[1] == artist and row[2] == link:
            passed = True

    if not passed:
        test2.failedtest("Error in part 2")
    else:
        test2.passedtest()
    test2.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ3(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test3 = TestSuite.Test("Q3")
    test3.setworth(1)  # set point worth of the test
    suite.addtest(test3)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    passed = False

    title = genString(20)
    artist = genString(20)
    link = genString(20)
    request = {"requestType": "addSong", "title": title, "artist": artist, "link": link}
    StudentImplementation.handleRequest(request)

    allSongsAndReviews = StudentImplementation.handleRequest({"requestType": "getAllSongsAndReviews"})
    allSongsAndReviews = json.loads(allSongsAndReviews)

    testSongID = 0
    for songID in allSongsAndReviews:
        testSongID = songID
        break

    testReview = genString(100)
    StudentImplementation.handleRequest({"requestType": "addReview", "id": testSongID, "review": testReview})

    allSongsAndReviews = StudentImplementation.handleRequest({"requestType": "getAllSongsAndReviews"})
    allSongsAndReviews = json.loads(allSongsAndReviews)

    # {songID : [title, artist, YouTubeLink, [review1, review2,...]],
    try:
        for review in allSongsAndReviews[testSongID][3]:
            if review == testReview:
                passed = True
    except:
        test3.failedtest("Part 3 dictionary not formatted properly")

    if not passed:
        test3.failedtest("Error in part 3")
    else:
        test3.passedtest()
    test3.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ4(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test4 = TestSuite.Test("Q4")
    test4.setworth(1)  # set point worth of the test
    suite.addtest(test4)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    test4.failedtest("Part 4 will be manually graded by viewing your web page")

    test4.setruntime(TestSuite.calculateruntime(startTime, endTime))


# main testing logic
if __name__ == "__main__":
    # Set up three suites at the top corresponding to each "Part"
    import sys

    suppressCout()
    sys.path.insert(0, 'sys.argv[1]')
    StudentImplementation = importlib.import_module("hw6Server")
    runtime = 0.0
    suite = TestSuite.TestSuite()
    args = sys.argv[2:]
    for i in range(len(args)):
        if args[i] == "1":
            testQ1(suite, StudentImplementation)
        elif args[i] == "2":
            testQ2(suite, StudentImplementation)
        elif args[i] == "3":
            testQ3(suite, StudentImplementation)
        elif args[i] == "4":
            testQ4(suite, StudentImplementation)
        else:
            print("No test found matching input")
    # Couts for feedback results. Should be standard for each assignment.
    restoreCout()
    print(suite.results())
    print("Success Rate: ", suite.percentage(), "/4")
    print("Total Runtime: ", suite.totalruntime(), "ms")
