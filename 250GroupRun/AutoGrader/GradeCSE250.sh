assignment="StockTracker"
timeoutMsg="\n Terminated unexpectedly due to long runtime. Please test with provided makefile. \n Success Rate: 0%"
segfault="\n Segmentation Fault! Please test with provided makefile. \n Success Rate: 0%"
echo "Copying files from submit..."
#cp -rfp /submit/hartloff/CSE250/* /projects/CSE250/Fall2015/AutoGrader/Submissions/ DEPRICATED
rsync -au cheshire.cse.buffalo.edu:/web/planetexpress/temp/CSE250/ /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/
echo "Backing up submissions to Archive..."
tar -czvf /projects/CSE250/Spring2016/AutoGrader/Backup/Archive/CSE250/"automaticSubmission`date`.tar.gz" /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/* >/dev/null
supportDir=/projects/CSE250/Spring2016/AutoGrader/SupportFiles/CSE250/HW3
workingDir=/projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/HW3
rm $workingDir/TotalResults.txt
checkFile() {
   echo "" > $dir/newStat.txt
   for file in "$dir"/*
   do
      if [[ $file == *.cpp ]] 
      then
         OLD_STAT_FILE=$dir/fileCheck.txt
 
         echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
      fi
      
      if [[ $file == *.h ]] 
      then
         OLD_STAT_FILE=$dir/fileCheck.txt
 
         echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
      fi
   done
   
   diff $dir/newStat.txt $dir/fileCheck.txt >/dev/null
   result=$?   
   if [[ $result == 0 ]] || [[ $dir == ./zzArchive ]] || [[ $dir == ./zSupportFiles ]] # Depricated
   then
      return 1
   else
      # update the OLD_STAT_FILE
      cp -f $dir/newStat.txt $dir/fileCheck.txt
      return 0
   fi   
}

#removes compiled files before going to next student.
function cleanUp {
   if [[ -f a.out ]]
      then 
      rm a.out
   fi
}

# Compiles and tests the given cpp files and dumps to
# Results.txt file
function compile {
    /util/bin/g++ -std=c++11 -I $supportDir/ -I $dir $dir/*.cpp $supportDir/*.cpp -o $dir/a.out
}

function finishResult {
   echo "Submission generated on: $(date)" >> $workingDir/Results.txt
   echo "END OF RESULT-----------------------------------------------" >> $workingDir/Results.txt
   cat $workingDir/Results.txt >> $workingDir/TotalResults.txt
}

#run all files through MOSS
function moss {
   echo "Running MOSS"
   echo "------------------------RUNNING MOSS------------------------" >> $workingDir/TotalResults.txt
   perl moss.pl -l cc -d -b $assignment.h ./*/$assignment.cpp | sed '/Uploading/d' >> $workingDir/TotalResults.txt
}

#Writes username of the sutdent (submission folder) to results.
function writeUser {
   # sed is used to truncate output to just username (':' is delimiter)
   user=${dir##*/}
   echo $user > $workingDir/Results.txt
   echo $user
}
#Main Logic
for dir in /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/HW3/*; 
do
   if [[ -d $dir ]]
   then
      for file in "$dir"/*  
      do
         #Checks if file exists and has been modified in last 60 mins
         if [[ $file == *$assignment.cpp ]] && checkFile $dir && [[ $? == 0 ]]
         then
	    writeUser
	    compile $file &>> $workingDir/Results.txt
	    cd $dir
	    timeout 45 ./a.out 1 2 3 4 >> $workingDir/Results.txt
	    
	    exitCode=$?
	    if [[ $exitCode == 124 ]] 
	    then 
	       echo -e $timeoutMsg >> $workingDir/Results.txt 
	    fi
	    if [[ $exitCode == 134 ]] 
	    then 
	       echo -e $segfault >> $workingDir/Results.txt 
	    fi	    
	    if [[ $exitCode == 139 ]] 
	    then 
	       echo -e $segfault >> $workingDir/Results.txt 
	    fi
	    cd ..
            finishResult
	    
	    rm $dir/a.out 2> /dev/null
	    checkFile $dir  	    
	    #emails student with results
            cat $workingDir/Results.txt | mail -s $assignment" Results" $user@buffalo.edu
	    cleanUp        
	 fi #End file check
      done
   fi #End of submission folders
done

#moss
cat $workingDir/TotalResults.txt > /projects/CSE250/Spring2016/AutoGrader/Backup/Results/CSE250/$(date)Results.txt
# Cleans up any core dump files
rm $workingDir/Results.txt
rm core.* 2> /dev/null
cat Cronlog.txt | mail -s "Script run successful" daviddob@buffalo.edu
echo "Finished"
