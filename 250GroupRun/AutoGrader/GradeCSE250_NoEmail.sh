DB=postel.cse.buffalo.edu
USER=farnsworth
PASS=\!InfiniteHours1337\!\$

hwID="HW6"
assignment="WebGraph"
timeoutMsg="\n Terminated unexpectedly due to long runtime. Please test on timberlake. \n"
segfault="\n Segmentation Fault! Please test on timberlake. \n"
#echo "Copying files from submit..."
#cp -rfp /submit/hartloff/CSE250/* /projects/CSE250/Fall2015/AutoGrader/Submissions/ DEPRICATED
#rsync -au cheshire.cse.buffalo.edu:/web/planetexpress/temp/CSE250/ /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/
echo "Backing up submissions to Archive..."
#tar -czvf /projects/CSE250/Spring2016/AutoGrader/Backup/Archive/CSE250/"automaticSubmission`date`.tar.gz" /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/* >/dev/null
supportDir=/projects/CSE250/Spring2016/AutoGrader/SupportFiles/CSE250/$hwID
workingDir=/projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/$hwID
#rm $workingDir/TotalResults.txt
checkFile() {
    echo "" > $dir/newStat.txt
    for file in "$dir"/*
    do
        if [[ $file == *.cpp ]]
        then
            OLD_STAT_FILE=$dir/fileCheck.txt

            echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
        fi

        if [[ $file == *.h ]]
        then
            OLD_STAT_FILE=$dir/fileCheck.txt

            echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
        fi

	if [[ $file == *.py ]]
        then
            OLD_STAT_FILE=$dir/fileCheck.txt

            echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
        fi

	if [[ $file == *.java ]]
        then
            OLD_STAT_FILE=$dir/fileCheck.txt

            echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
        fi

    done

    diff $dir/newStat.txt $dir/fileCheck.txt > /dev/null
    result=$?
    if [[ $result == 0 ]] || [[ $dir == ./zzArchive ]] || [[ $dir == ./zSupportFiles ]] # Depricated
    then
        return 1
    else
        # update the OLD_STAT_FILE
        cp -f $dir/newStat.txt $dir/fileCheck.txt
        return 0
    fi
}

#removes compiled files before going to next student.
function cleanUp {
    find $dir ! -name 'fileCheck.txt' ! -name '*.cpp' ! -name '*.h' ! -name '*.java' ! -name '*.py' -type f -exec rm -rf {} +
}

# Compiles and tests the given cpp files and dumps to
# Results.txt file
function compile {
    #/util/bin/g++ -std=c++11 -I $supportDir/ -I $dir $dir/*.cpp $supportDir/*.cpp -o $dir/a.out
    /util/bin/g++ -std=c++11 $dir/*.cpp -o $dir/$assignment.out
}

function finishResult {
    echo "Submission generated on: $(date)" >> $workingDir/Results.txt
    echo "END OF RESULT-----------------------------------------------" >> $workingDir/Results.txt
    cat $workingDir/Results.txt >> $workingDir/TotalResults.txt
}

#run all files through MOSS
function moss {
    echo "Running MOSS"
    echo "------------------------RUNNING MOSS------------------------" >> $workingDir/TotalResults.txt
    perl moss.pl -l cc -d -b $assignment.h ./*/$assignment.cpp | sed '/Uploading/d' >> $workingDir/TotalResults.txt
}

#Writes username of the sutdent (submission folder) to results.
function writeUser {
    # sed is used to truncate output to just username (':' is delimiter)
    user=${dir##*/}
    echo $user > $workingDir/Results.txt
    echo $user
}


#ToDo for Mike:
# 1) Each loop count total files in dir
#       ls -1 | wc -l
# 2) While running, count passed per inner loop
# 3) Use running count for tests inside individual test dirs
# 4) User total count if and only if running count == total in dir per testsPassed
# 5) Echo out the total count after all loops
function genResult {
    testsPassed=0
    feedback=""
    runtime=0
    faulted=0
    correct=0
    totalTests=0
    anotherCounter=0
    timeOut=180


	#echo "testing"

    for tests in $supportDir/*; do

        if [[ $faulted == 1 ]]
        then
            break
        fi

        if [[ -d $tests ]]
        then
            testNumber=-1
            tempPassed=0
            runningTests=0
            temp=${tests##*/}
            temp=$(echo $tests/$temp"Input")
            #echo "Tests:"$tests

            runningTests=0
            (( anotherCounter++ ))
            while IFS= read -r inputs
            do
                (( runningTests++ ))
                (( testNumber++ ))
                #echo "Inputs:"$inputs
                if [[ $lang == "Java" ]]; then
                    START=$(date +%s.%N)
                    output=$(timeout $timeOut java -cp $dir $assignment $inputs)
                    exitCode=$?
                    if [[ $exitCode == 124 ]]
                    then
                        feedback=$(echo -e $timeoutMsg)
                        faulted=1
                        break
                    fi
                    END=$(date +%s.%N)
                    testRuntime=$(echo "$END - $START" | bc)
				fi
                if [[ $lang == "C++" ]]; then
                    START=$(date +%s.%N)
                    output=$(timeout $timeOut $dir/$assignment.out $inputs)
                    exitCode=$?
                    if [[ $exitCode == 124 ]]
                    then
                        feedback=$(echo -e $timeoutMsg)
                        faulted=1
                        break
                    fi
                    if [[ $exitCode == 134 ]]
                    then
                        feedback=$(echo -e $segfault)
                    fi
                    if [[ $exitCode == 139 ]]
                    then
                        feedback=$(echo -e $segfault)
                    fi
                    END=$(date +%s.%N)
                    testRuntime=$(echo "$END - $START" | bc)
                fi
                if [[ $lang == "Python" ]]; then
                    START=$(date +%s.%N)
                    output=$(timeout $timeOut /util/bin/python3 $dir/$assignment.py $inputs)
                    exitCode=$?
                    if [[ $exitCode == 124 ]]
                    then
                        feedback=$(echo -e $timeoutMsg)
                        faulted=1
                        break
                    fi
                    END=$(date +%s.%N)
                    testRuntime=$(echo "$END - $START" | bc)
                fi
                if [[ $anotherCounter > 2 ]]
                then
                    runtime=$(echo "($runtime + $testRuntime)" | bc -l)
                fi
                #`echo -e "$output" > $dir/output.txt
                #echo -e "Checking results of test$testNumber with $tests/${tests##*/}-$testNumber"

				if [[ $testNumber != 2  ]]; then
				    results=$(python3 /shared/projects/CSE250/Spring2016/AutoGrader/SupportFiles/CSE250/HW6/comparePart1and3.py $tests/${tests##*/}-$testNumber $dir/output.txt)
                    rm $dir/output.txt
                    fi

				if [[ $testNumber == 2 ]]; then
				    results=$(python3 /shared/projects/CSE250/Spring2016/AutoGrader/SupportFiles/CSE250/HW6/comparePart2.py $tests/${tests##*/}-$testNumber $dir/output.txt)
                    rm $dir/output.txt
				fi

				#echo "Result:"$results

                if [[ $results == "pass" ]]; then
                    (( tempPassed++ ))
                    #echo "Running:$runningTests compared to: $tempPassed"
                else
                    feedback=$(echo -e $feedback"\n "$results)
                    break
                fi

                (( totalTests++ ))
                #echo "Total tests:" $totalTests
            done < "$temp"
            #echo "Tests Run:$runningTests"
            #echo "Tests Passed: $tempPassed"
            if [[ $runningTests == $tempPassed ]]; then
                if [[ $anotherCounter == 3 ]]
                then
                    #echo "Part 3 passed"
                    correct=1
                fi
                (( testsPassed++ ))
                #echo "_______________SUCCESS____________________"
            fi
            #echo "other counter" $anotherCounter
        fi
    done

    if [[ $runtime != 0 ]]
    then
      avgRuntime=$(echo "($runtime / 2)*1000" | bc -l)
    else
      avgRuntime=99999999
    fi

    #if [[ $testsPassed == 3 ]]
    #then
    #    correct=1
    #else
    #    correct=0
    #fi

    if [[ $correct == 1 ]] && [[ $(echo " $avgRuntime < 60000.00" | bc) == 1 ]]
    then
        (( testsPassed++ ))
    else
        feedback=$(echo -e $feedback"\n Average runtime is too high for part 4")
    fi


    #echo -e "Runtime:$avgRuntime"
    #echo -e "Passed:$testsPassed"
    #echo -e "$feedback"
    echo -e "$feedback" >> $workingDir/Results.txt
    echo -e "Success Rate:  $testsPassed.0 /4" >> $workingDir/Results.txt
    echo -e "Total Runtime:  $avgRuntime ms" >> $workingDir/Results.txt

    classID=250submissions
    testsTotal=4
    temp=$(echo -e $feedback | sed "s/'/\\\'/g")

    $(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "INSERT INTO $classID(ubit, hwID, lang, testsTotal, testsPassed, runtime, feedback, timestamp) VALUES ('$user', '$hwID', '$lang', '$testsTotal', '$testsPassed', '$avgRuntime', '$temp', '$(date +"%Y-%m-%d %H:%M:%S")')")
    #$(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "INSERT INTO $classID(ubit, hwID, lang, testsTotal, testsPassed, runtime, feedback, timestamp) VALUES ('$user', '$hwID', '$lang', '$testsTotal', '$testsPassed', '$avgRuntime', '$temp', '2016-04-30 01:59:59')")

}


#Main Logic
while [[ true ]]
do
  rsync -au cheshire.cse.buffalo.edu:/web/planetexpress/temp/CSE250/ /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/
  for dir in /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/$hwID/*;
  do

      if [[ -d $dir ]]
      then
          for file in "$dir"/*
          do
              #Checks if file exist and has been modified in last 60 mins
              if [[ $file == *$assignment.* ]] && checkFile $dir && [[ $? == 0 ]]
              then
				  writeUser
                  cd $dir
                  cp -rf $supportDir/../HW6Data/data $dir/
				  lang=$(cat $dir/lang.txt)
				  echo "Lang is:"$lang
                  if [[ $lang == "C++" ]]
                  then
                      compile $file &>> $workingDir/Results.txt
                      genResult
                  fi

                  if [[ $lang == "Java" ]]
                  then
					  echo $user
                      javac $assignment.java
                      genResult
				  fi

                  if [[ $lang == "Python" ]]
                  then
                      genResult
                  fi

                  cd ..

                  finishResult
                  checkFile $dir
                  #emails student with results
                  #cat $workingDir/Results.txt | mail -s $assignment" Results" $user@buffalo.edu
                  #cleanUp
              fi #End file check
          done
      fi #End of submission folders
  done
    sleep 900
done

#moss
#cat $workingDir/TotalResults.txt > /projects/CSE250/Spring2016/AutoGrader/Backup/Results/CSE250/$(date)Results.txt
# Cleans up any core dump files
#rm $workingDir/Results.txt
rm core.* 2> /dev/null
#mail -s "Script has unexpectedly exited. Go check timberlake." daviddob@buffalo.edu
echo "Finished"
