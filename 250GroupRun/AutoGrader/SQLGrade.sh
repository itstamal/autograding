DB=postel.cse.buffalo.edu
USER=farnsworth
PASS=\!InfiniteHours1337\!\$
#tar -czvf /projects/CSE250/Spring2016/AutoGrader/Backup/Archive/CSE250/"automaticSubmission`date`.tar.gz" /projects/CSE250/Spring2016/AutoGrader/Submissions/CSE250/* >/dev/null

function parseAndPush {
    exitCode=$?
    #echo "EXIT" $exitCode
    if [[ $lang == "C++" ]]
    then
        if [[ $exitCode == 124 ]]
        then
           echo -e $timeoutMsg >> $workingDir/Results.txt
        fi
        if [[ $exitCode == 134 ]]
        then
           echo -e $segfault >> $workingDir/Results.txt
        fi
        if [[ $exitCode == 139 ]]
        then
           echo -e $segfault >> $workingDir/Results.txt
        fi
    fi
    testsTotal=0
    testsPassed=0
    runtime=0
    workingQuestion=""
    feedback=""
    while IFS=' ' read -r -a line
    do
    	if [[ ${line[0]} == "Passed" || ${line[0]} == "Failed" ]];
        then
            workingQuestion=${line[*]}
            ((testsTotal++))
    		if [[ ${line[0]} == "Passed" ]]; then
    			((testsPassed++))
    		fi
        elif [[ ${line[1]} == "Runtime:" ]]; then
            if [[ $lang == "C++" ]]
            then
                runtime=$runtime+${line[2]%??}
            else
                runtime=$runtime+${line[2]}
            fi
        else
            feedback=$feedback$workingQuestion": "${line[*]}'\n'
        fi
    done
    runtime=`echo "$runtime" | bc`
    classID=${classID:(-3)}submissions
    echo $testsTotal
    echo $testsPassed
    echo $runtime
    echo -e $feedback
    echo $classID
    echo $hwID
    temp=$(echo $feedback | sed "s/'/\\\'/g")
    $(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "INSERT INTO $classID(ubit, hwID, lang, testsTotal, testsPassed, runtime, feedback, timestamp) VALUES ('$userID', '$hwID', '$lang', '$testsTotal', '$testsPassed', '$runtime', '$temp', '$(date +"%Y-%m-%d %H:%M:%S")')")


	#output=$(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "SELECT testsPassed, MIN(runtime) FROM (SELECT * FROM $classID WHERE ubit='$userID' and hwID = '$hwID' and testsPassed = (SELECT MAX(testsPassed) FROM $classID WHERE ubit='$userID' AND hwID='$hwID')) AS maxTable;")

	#echo $output

	#output=($output)

	#prevBestGrade=${output[0]}
	#prevBestRuntime=${output[1]}

	#echo newPassed: $testsPassed
	#echo newRuntime: $runtime

	#echo grade: $prevBestGrade
	#echo runtime: $prevBestRuntime

	#if [[ $prevBestGrade == "NULL" ]] || [[ $prevBestGrade < $testsPassed ]] || (( $prevBestGrade == $testsPassed &&  $(echo "$prevBestRuntime >= $runtime" | bc) == 1 ))
	#then
	#    echo "break in"
	#fi

}

#Main Logic
queryResult="START"
while [[ $queryResult ]]
do
#    echo "Result" $queryResult
    queryResult=$(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "SELECT * FROM gradingQueue WHERE id = 0")
    #echo $queryResult
    if [[ $queryResult ]]
    then
        read nothing classID hwID userID lang autograded <<< $queryResult
        echo "Class" $classID
        echo "User" $userID
        echo "HW" $hwID
        echo "LANG" $lang
	echo "Autograded" $autograded
        timestamp=$(date +"%Y-%m-%d %H:%M:%S")
        $(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "DELETE FROM gradingQueue WHERE id = 0")
        $(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "SET @counter = -1; UPDATE gradingQueue SET id = @counter := @counter + 1 WHERE id >= 0;")
        mkdir -p /projects/CSE250/Spring2016/AutoGrader/Submissions/$classID/$hwID/$userID
        rsync -au cheshire.cse.buffalo.edu:/web/planetexpress/temp/$classID/$hwID/$userID/* /projects/CSE250/Spring2016/AutoGrader/Submissions/$classID/$hwID/$userID
        archivePath=/projects/CSE250/Spring2016/AutoGrader/Backup/Archive/$classID/$hwID/$userID/$timestamp
        mkdir -p "$archivePath"
        cp -rf /projects/CSE250/Spring2016/AutoGrader/Submissions/$classID/$hwID/$userID/* "$archivePath"
        supportDir=/projects/CSE250/Spring2016/AutoGrader/SupportFiles/$classID/$hwID
        workingDir=/projects/CSE250/Spring2016/AutoGrader/Submissions/$classID/$hwID
        if [[ $lang == "Python" ]]
        then
            echo "Running Python!"
            cp $supportDir/* $workingDir/$userID 2> /dev/null
            cd $workingDir/$userID
            timeout 180 /util/bin/python Grade$hwID.py ${hwID,,} 1 2 3 4 2>&1 | parseAndPush
        fi

        if [[ $lang == "C++" ]]
        then
            echo "Using C++ for the language"
            /util/bin/g++ -std=c++11 -I $supportDir/ -I $workingDir/$userID $workingDir/$userID/*.cpp $supportDir/*.cpp -o $workingDir/$userID/a.out
            cd $workingDir/$userID
            timeout 180 ./a.out 1 2 3 4 2>&1 | parseAndPush
        fi

        if [[ $lang == "Java" ]]
        then
            echo "Got nothin for ya, you're SOL."
        fi

    else
        echo "Were empty!"
    fi
done

#cat Cronlog.txt | mail -s "Script run successful" daviddob@buffalo.edu
echo "Finished"
