#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <unordered_set>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <limits>

#include "FireworkFactory.h"
#include "TestSuite.h"

using namespace std;

TestSuite suite;
streambuf *old = cout.rdbuf();
stringstream ss;

void suppressCout() {
    cout.rdbuf(ss.rdbuf());
}

void restoreCout() {
    cout.rdbuf(old);
}


bool testFactory(int numberOfShipments,
                 int sizeOfFireworkShipment,
                 int sizeOfMetalShipment,
                 int purchaseSize,
                 int numberOfPurchases,
                 bool sellByColor);

bool sample1();
bool sample2();
bool sample3();
double sample4();


bool testFactory(int numberOfShipments,
                 int sizeOfFireworkShipment,
                 int sizeOfMetalShipment,
                 int purchaseSize,
                 int numberOfPurchases,
                 bool sellByColor){

    FireworkFactory* factory = new FireworkFactory();

    stack < Firework * > fireworksToShip;
    queue <Color> colorOrder;
    stack <Metal> metal;
    stack <Color> colorOrderPurchase;
    int metals[4] = {0};
    int purchaseByColor[4] = {0};

    for (int j = 0; j < numberOfShipments; j++) {

        // Firework Shipments
        stack<Color> tempStack;
        for (int i = 0; i < sizeOfFireworkShipment; i++) {
            Firework *firework = new Firework(static_cast<Color>(rand() % 4));
            tempStack.push(firework->getColor());
            fireworksToShip.push(firework);
        }
        while(!tempStack.empty()){
            colorOrder.push(tempStack.top());
            tempStack.pop();
        }
        factory->fireworkShipment(fireworksToShip);


        // Metal Shipments
        stack<Metal> tempMetal;
        for (int i = 0; i < sizeOfMetalShipment; i++) {
            metal.push(static_cast<Metal>(rand() % 4));
            tempMetal.push(metal.top());
        }
        while(!tempMetal.empty()) {
            metals[tempMetal.top()]++;
            if(metals[tempMetal.top()]%5 == 0){
                colorOrder.push(metalToColor(tempMetal.top()));
            }
            tempMetal.pop();
        }
        factory->metalShipment(metal);
    }


    stack < Firework * > *customerFireworks = new stack<Firework *>();

    for(int i=0; i<numberOfPurchases; i++) {
        factory->sellFireworks(*customerFireworks, purchaseSize);
        for(int j=0; j<purchaseSize; j++){
            if(purchaseByColor[colorOrder.front()] > 0){
                purchaseByColor[colorOrder.front()]--;
                j--;
            }else {
                colorOrderPurchase.push(colorOrder.front());
            }
            colorOrder.pop();
        }

        if(sellByColor) {
            Color purchaseColor = static_cast<Color>(rand() % 4);
            factory->sellFireworks(*customerFireworks, purchaseSize, purchaseColor);
            purchaseByColor[purchaseColor] = purchaseByColor[purchaseColor] + purchaseSize;
            for (int j = 0; j < purchaseSize; j++) {
                colorOrderPurchase.push(purchaseColor);
            }
        }

    }


    bool pass = customerFireworks->size() == colorOrderPurchase.size();

    while (!customerFireworks->empty()) {
        if(pass && customerFireworks->top()->getColor() != colorOrderPurchase.top()){
            pass = false;
        }
        delete customerFireworks->top();
        customerFireworks->pop();
        colorOrderPurchase.pop();
    }

    delete customerFireworks;
    delete factory;

    return pass;
}

bool sample1() {

    // FIFO and LIFO
    // Take firework shipments and sell them. No memory management

    int numberOfShipments = 20;
    int sizeOfFireworkShipment = 20;
    int sizeOfMetalShipment = 0;
    int purchaseSize = 50;
    int numberOfPurchases = 5;

    bool pass = testFactory(numberOfShipments, sizeOfFireworkShipment, sizeOfMetalShipment, purchaseSize, numberOfPurchases, false);

    return pass;
}

bool sample2() {

    // Make fireworks from metal
    // Delete inventory in destructor

    int numberOfShipments = 20;
    int sizeOfFireworkShipment = 20;
    int sizeOfMetalShipment = 20;
    int purchaseSize = 50;
    int numberOfPurchases = 5;

    bool pass = testFactory(numberOfShipments, sizeOfFireworkShipment, sizeOfMetalShipment, purchaseSize, numberOfPurchases, false);

    pass &= Firework::getHeapFireworks() == 0;

    return pass;
}


bool sample3() {

    // Sell Fireworks by color

    int numberOfShipments = 100;
    int sizeOfFireworkShipment = 50;
    int sizeOfMetalShipment = 20;
    int purchaseSize = 10;
    int numberOfPurchases = 20;

    bool pass = testFactory(numberOfShipments, sizeOfFireworkShipment, sizeOfMetalShipment, purchaseSize, numberOfPurchases, true);

    return pass;
}

double sample4() {

    // Sell fireworks by color in O(1)

    int numberOfShipments = 100;
    int sizeOfFireworkShipment = 50;
    int sizeOfMetalShipment = 20;
    int purchaseSize = 10;
    int numberOfPurchases = 20;

    bool pass = testFactory(numberOfShipments, sizeOfFireworkShipment, sizeOfMetalShipment, purchaseSize, numberOfPurchases, true);

    if(!pass){
        return std::numeric_limits<double>::infinity();
    }

    numberOfShipments = 100;
    sizeOfFireworkShipment = 1000;
    sizeOfMetalShipment = 500;
    purchaseSize = 50;
    numberOfPurchases = 100;

    FireworkFactory* factory = new FireworkFactory();

    stack < Firework * > fireworksToShip;
    stack <Metal> metal;

    for (int j = 0; j < numberOfShipments; j++) {
        for (int i = 0; i < sizeOfFireworkShipment; i++) {
            Color currentColor = j<10 ? Purple : static_cast<Color>(rand() % 3);
            Firework *firework = new Firework(currentColor);
            fireworksToShip.push(firework);
        }
        factory->fireworkShipment(fireworksToShip);

        for (int i = 0; i < sizeOfMetalShipment; i++) {
            metal.push(static_cast<Metal>(rand() % 3));
        }
        factory->metalShipment(metal);
    }

    stack < Firework * > *customerFireworks = new stack<Firework *>();

    clock_t startTime = clock();

    for(int i=0; i<numberOfPurchases; i++) {
        factory->sellFireworks(*customerFireworks, purchaseSize);
        Color purchaseColor =  i < 50 ? Purple : static_cast<Color>(rand() % 3);
        factory->sellFireworks(*customerFireworks, purchaseSize, purchaseColor);
    }

    double elapsedTime = (double(clock() - startTime) / CLOCKS_PER_SEC)*1000.0;

    while (!customerFireworks->empty()) {
        delete customerFireworks->top();
        customerFireworks->pop();
    }

    delete customerFireworks;
    delete factory;

    return elapsedTime;
}


char genRandom() {
    std::string alpha = "udlrab";
    return alpha.at(rand() % alpha.length());
}

string intToString(int a) {
    stringstream ss;
    ss << a;
    return ss.str();
}


void testQ1(void) {
    Test *test1 = new Test("Test1");
    test1->setWorth(1);
    suite.addTest(test1);

    clock_t startTime = clock();
    bool passedTest = sample1();
    clock_t endTime = clock();

    test1->setRuntime(calculateRuntime(startTime, endTime));
    if (passedTest) {
        test1->passedTest();
    }
    else {
        test1->failedTest("Try sample 1 locally and on Timberlake");
    }
}

void testQ2(void) {
    Test *test2 = new Test("Test2");
    test2->setWorth(1);
    suite.addTest(test2);

    clock_t startTime = clock();
    bool passedTest = sample2();
    clock_t endTime = clock();

    test2->setRuntime(calculateRuntime(startTime, endTime));
    if (passedTest) {
        test2->passedTest();
    } else {
        test2->failedTest("Try sample 2 locally and on Timberlake");
    }
}


void testQ3(void) {
    Test *test3 = new Test("Test3");
    test3->setWorth(1);
    suite.addTest(test3);

    clock_t startTime = clock();
    bool passedTest = sample3();
    clock_t endTime = clock();

    test3->setRuntime(calculateRuntime(startTime, endTime));
    if (passedTest) {
        test3->passedTest();
    } else {
        test3->failedTest("Try sample 3 locally and on Timberlake");
    }
}


void testQ4(void) {
    Test *test4 = new Test("Test4");
    test4->setWorth(1);
    suite.addTest(test4);

    double runtime = sample4();

    test4->setRuntime(runtime);
    if (runtime<20) {
        test4->passedTest();
    } else {
        test4->failedTest("Try sample 4 locally and on Timberlake.");
    }

}

int main(int argc, char *argv[]) {
    //main testing logic
    double runtime;
    for (int i = 1; i < argc; i++) {
        switch (atoi(argv[i])) {
            case 1:
                cout << "Running testQ1" << endl;
                suppressCout();
                testQ1();
                restoreCout();
                break;
            case 2:
                cout << "Running testQ2" << endl;
                suppressCout();
                testQ2();
                restoreCout();
                break;
            case 3:
                cout << "Running testQ3" << endl;
                suppressCout();
                testQ3();
                restoreCout();
                break;
            case 4:
                cout << "Running testQ4" << endl;
                suppressCout();
                testQ4();
                restoreCout();
                break;
            default:
                cout << "No test found matching input" << endl;
        }
    }
    //Couts for feedback results. Should be standard for each assignment.
    cout << suite.results();
    cout << "Success Rate: " << suite.percentage() << "/4" << endl;
    cout << "Total Runtime: " << suite.totalRuntime() << "ms" << endl;
    cout << endl;
    return 0;

}
