import sys
import re


def normalize(inputString):
    string = inputString.strip()
    return re.sub(r"\D", "", string)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("improper usage")
        exit()
    referenceFilename = sys.argv[1]
    studentFilename = sys.argv[2]

    solutionSet = set([])
    studentSet = set([])

    with open(referenceFilename, "r") as referenceFile:
        with open(studentFilename, "r") as studentFile:
            for line in referenceFile:
                if len(line.strip()) > 0:
                    solutionSet.add(normalize(line))
            for line in studentFile:
                if len(line.strip()) > 0:
                    studentSet.add(normalize(line))

    if solutionSet == studentSet:
        print("pass")
    else:
        print("Did not match " + referenceFilename + " and " + studentFilename)
