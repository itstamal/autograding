#include "TestSuite.h"
#include <sstream>
using namespace std;

double calculateRuntime(clock_t clock1,clock_t clock2){
    double ticks=clock2-clock1;
    double runtime = (ticks)/float(CLOCKS_PER_SEC)*1000;
    return runtime;
}

// Suite Implementation
TestSuite::TestSuite(){
    
}

void TestSuite::addTest(Test* instance){
    testVec_.push_back(instance);
}

double TestSuite::totalRuntime(){
    double time = 0;
    for(int i = 0; i < testVec_.size(); i++){
        time += testVec_[i]->getRuntime();
    }
    return time;
}

double TestSuite::percentage(){
    for(int i = 0; i < testVec_.size(); i++){
        if(testVec_[i]->getPassed()){
            passedValue_ += testVec_[i]->getWorth();
        }
        totalValue_ += testVec_[i]->getWorth();
    }
    return ((double)passedValue_ / (double)totalValue_)*100;
}

string TestSuite::results(){
    string results = "";
    for(int i = 0; i < testVec_.size(); i++){
        results += testVec_[i]->toString() + '\n';
    }
    return results;
}

void TestSuite::setWorth(int value){
    worth_ = value;
}

int TestSuite::getWorth(){
    return worth_;
}

//Test specific implementation
Test::Test(string name){
    testName_ = name;
    totalValue_ = 0;
    passedValue_ = 0;
    runtime_ = 0;
}

bool Test::getPassed(){
    return passed_;
}

string Test::getFailInfo(){
    return failInfo_;
}

string Test::getTestName(){
    return testName_;
}

void Test::setWorth(int value){
    testWorth_ = value;
}

void Test::setRuntime(double runtime){
    runtime_ = runtime;
}

double Test::getRuntime(){
    return runtime_;
}

int Test::getWorth(){
    return testWorth_;
}

string Test::toString(){
    stringstream ss;
    if(passed_){
        ss << "Passed test " << testName_ << endl;
    }else{
        ss << "Failed test " << testName_ << endl << failInfo_ << endl;
    }
    ss << "Runtime: " << runtime_;
    return ss.str();
}

void Test::failedTest(string info){
    passed_ = false;
    failInfo_ = info;
}

void Test::passedTest(){
    passed_ = true;
}

