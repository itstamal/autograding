#ifndef CSE250_FALL2015_Test_H
#define CSE250_FALL2015_Test_H

#include <string>
#include <iostream>
#include <time.h>
#include <vector>
using namespace std;
class Test;
class TestSuite;

double calculateRuntime(clock_t, clock_t);

class TestSuite {
protected:
    vector<Test*> testVec_;
    int totalValue_;
    int passedValue_;
    int worth_;
public:
    double totalRuntime();
    TestSuite();
    string results();
    double percentage();
    void addTest(Test*);
    void setWorth(int);
    int getWorth();
};

class Test : public TestSuite {
protected:
    bool passed_;
    string failInfo_;
    string testName_;
    int testWorth_;
    double runtime_;
public:
    Test(string);
    bool getPassed();
    void setWorth(int);
    int getWorth();
    string getFailInfo();
    string getTestName();
    string toString();
    void failedTest(string);
    void passedTest();
    void setRuntime(double runtime);
    double getRuntime();
};

#endif //CSE250_FALL2015_Test_H

