#include "TreasureHunterSolution.h"

TreasureHunterSolution::TreasureHunterSolution(int x, int y, GameBoard& gameBoard) {
    this->x = x;
    this->y = y;
    currentBoard = &gameBoard;
}

TreasureHunterSolution::~TreasureHunterSolution() {
//    delete currentBoard;
}


/*
 *Changes what is on the board by passing in a pointer
 */
void TreasureHunterSolution::changeBoard(GameBoard *newBoard) {
    this->currentBoard = newBoard;
}

/*
 *Changes what is on the board by passing a reference
 */
void TreasureHunterSolution::changeBoard(GameBoard &newBoard) {
    this->currentBoard = &newBoard;
}

int TreasureHunterSolution::computeScore(std::string inputString) {

    int score = 50;
    string codeProgress = "";
    string konamiCode = "uuddlrlrba";
    bool konamiActive = false;
    string gameState = "normal";

    int exitX = currentBoard->getGoalLocation().getX();
    int exitY = currentBoard->getGoalLocation().getY();

    for (int i = 0; i < inputString.length(); i++) {

        string input = "";
        input += inputString.at(i);

        codeProgress += input;
        if (konamiCode.find(codeProgress) != 0) {
            codeProgress = "";
        } else if (codeProgress == konamiCode) {
            konamiActive = true;
        }

        if (gameState == "normal") {

            if (input == "d") {
                score += y > exitY ? 1 : -2;
                y--;
            } else if (input == "l") {
                score += x > exitX ? 1 : -2;
                x--;
            } else if (input == "u") {
                score += y < exitY ? 1 : -2;
                y++;
            } else if (input == "r") {
                score += x < exitX ? 1 : -2;
                x++;
            } else if (input == "b") {
                //b pressed;
            } else if (input == "a") {
                //a pressed;
            } else {
                score--;
            }

            Location currentLocation(x,y);

            if(currentBoard->isTreasure(currentLocation)){
                score += 10;
                currentBoard->removeTreasure(currentLocation);
            }

            if(currentBoard->isEnemy(currentLocation)){
                gameState = "onEnemy";
            }

            if (gameState == "onEnemy" && konamiActive) {
                currentBoard->killEnemy(currentLocation);
                gameState = "normal";
            }

            if (exitX == x && exitY == y) {
                score += 100;
                cout << "score: " << score << endl;
                return score;
            }

        } else if (gameState == "onEnemy") {
            if (input == "a") {
                currentBoard->killEnemy(Location(x,y));
                gameState = "normal";
            } else if (input == "u" || input == "d" || input == "l" || input == "r" || input == "b") {
                // no movement
            } else {
                // invalid input
                score--;
            }
        }

        if (score < 0) {
            score = 0;
            cout << "score: " << score << endl;
            return score;
        }
    }

    cout << "score: " << score << endl;
    return score;
}
