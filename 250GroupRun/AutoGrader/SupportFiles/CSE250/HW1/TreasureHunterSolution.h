#ifndef UNTITLED_TREASUREHUNTERSOLUTION_H
#define UNTITLED_TREASUREHUNTERSOLUTION_H

#include "GameBoard.h"


class TreasureHunterSolution : public GameLogicSolution  {
private:
    GameBoard* currentBoard = new GameBoard(Location(0,0));
    int x;
    int y;

public:

    TreasureHunterSolution(int, int, GameBoard&);
    ~TreasureHunterSolution();

    void changeBoard(GameBoard&);
    void changeBoard(GameBoard*);

    int computeScore(std::string inputString);
};

#endif //UNTITLED_TREASUREHUNTERSOLUTION_H
