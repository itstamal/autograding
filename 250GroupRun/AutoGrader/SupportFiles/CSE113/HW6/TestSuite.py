import datetime

def calculateruntime(begin, end):
    return end - begin


class TestSuite:
    def __init__(self):
        self.testVec = []


    def addtest(self, instance):
        self.testVec.append(instance)


    def totalruntime(self):
        time = datetime.timedelta()
        for i in range(len(self.testVec)):
            time += self.testVec[i].getruntime()
        return time.microseconds / 1000. + time.seconds * 1000.


    def percentage(self):
        self.passedValue = 0.0
        self.totalValue = 0.0
        for i in range(len(self.testVec)):
            if self.testVec[i].getpassed():
                self.passedValue += self.testVec[i].getworth()
            self.totalValue += self.testVec[i].getworth()
        return self.passedValue # commented to use the 4 point system / self.totalValue) * 100


    def results(self):
        results = ""
        for i in range(len(self.testVec)):
            results += str(self.testVec[i]) + '\n'
        return results


    def setworth(self, value):
        self.worth = value


    def getworth(self):
        return self.worth



class Test:
    def __init__(self, name):
        self.testName = name
        self.totalValue = 0
        self.passedValue = 0
        self.runtime = datetime.timedelta()


    def getpassed(self):
        return self.passed


    def getfailinfo(self):
        return self.failInfo


    def gettestname(self):
        return testName


    def setworth(self, value):
        self.testWorth = value


    def setruntime(self, runtime):
        self.runtime = runtime


    def getruntime(self):
        return self.runtime


    def getworth(self):
        return self.testWorth


    def __str__(self):
        s = ""
        if self.passed:
            s += "Passed test " + self.testName  + '\n'
        else:
            s += "Error in test " + self.testName + '\n' + self.failInfo
        #s += "Runtime: " + str(self.runtime); Commented out to remove "intimidating" results
        return s


    def failedtest(self, info):
        self.passed = False
        self.failInfo = info


    def passedtest(self):
        self.passed = True
