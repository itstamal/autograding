#Make sure filename is "Grade(AssignmentName)" with proper capitalization.
import sys
import datetime
import TestSuite
import importlib
import random
import os

old_stdout = sys.stdout
#helper functions to check correctness (usually renamed solutions dumped in to check against students code)
def suppressCout():
    sys.stdout = open(os.devnull,'w')

def restoreCout():
    sys.stdout = old_stdout

def myImplementationQ1(num):
    return num*75

def myImplementationQ2(numberOfSoldiers, numberOfFireballs):
    armyHealth = numberOfSoldiers * 100
    totalDamage = 25 * numberOfSoldiers * numberOfFireballs
    armyHealth = armyHealth - totalDamage
    return armyHealth

def myImplementationQ3(first, last):
    return first + " " + last

def myImplementationQ4(first, last):
    return len(first + last)

def genRandom():
    alpha ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    return alpha[random.randint(0,51)]

def genString(size):
    str = ""
    for i in range(0,size):
        str += genRandom()
    return str

def testQ1(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q1")
    test1.setworth(1) #set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now() # set up runtime
    passed = True
    for i in range(0,50):
        fireballs = random.randint(0,9)
        expectedAnswer = myImplementationQ1(fireballs) #call your implementation of function
        studentAnswer = StudentImplementation.oneFireball(fireballs) #call student's implementation of function
        #logic for pass/fail and call proper function.
        if expectedAnswer != studentAnswer:
            passed = False
            break
    if passed:
        test1.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test1.failedtest("Soldiers did not take proper damage with input: " + str(fireballs) + "\n" )
    endTime = datetime.datetime.now();
    test1.setruntime(TestSuite.calculateruntime(startTime,endTime))

def testQ2(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test2 = TestSuite.Test("Q2")
    test2.setworth(1) #set point worth of the test
    suite.addtest(test2)
    startTime = datetime.datetime.now() # set up runtime
    passed = True
    for i in range(0,50):
        fireballs = random.randint(0,5)
        soldiers = random.randint(0,1000)
        expectedAnswer = myImplementationQ2(soldiers, fireballs) #call your implementation of function
        studentAnswer = StudentImplementation.multipleFireballs(soldiers, fireballs) #call student's implementation of function
        #logic for pass/fail and call proper function.
        if expectedAnswer != studentAnswer:
            passed = False
            break
    if passed:
        test2.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test2.failedtest("Soldiers did not take proper damage with: " + str(fireballs) + " Fireballs and " + str(soldiers) + " Soldiers \n" )
    endTime = datetime.datetime.now();
    test2.setruntime(TestSuite.calculateruntime(startTime,endTime))

def testQ3(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test3 = TestSuite.Test("Q3")
    test3.setworth(1) #set point worth of the test
    suite.addtest(test3)
    startTime = datetime.datetime.now() # set up runtime
    passed = True
    for i in range(0,50):
        str = genString(random.randint(1,75))
        str2 = genString(random.randint(1,75))
        expectedAnswer = myImplementationQ3(str, str2) #call your implementation of function
        studentAnswer = StudentImplementation.fullname(str, str2) #call student's implementation of function
        #logic for pass/fail and call proper function.
        if expectedAnswer != studentAnswer:
            passed = False
            break
    if passed:
        test3.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test3.failedtest("Strings didnt match with inputs " + str + " and " + str2 + "\n")
    endTime = datetime.datetime.now();
    test3.setruntime(TestSuite.calculateruntime(startTime,endTime))

def testQ4(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test4 = TestSuite.Test("Q4")
    test4.setworth(1) #set point worth of the test
    suite.addtest(test4)
    startTime = datetime.datetime.now() # set up runtime
    passed = True
    for i in range(0,50):
        str = genString(random.randint(1,75))
        str2 = genString(random.randint(1,75))
        expectedAnswer = myImplementationQ4(str, str2) #call your implementation of function
        studentAnswer = StudentImplementation.fullnameLength(str, str2) #call student's implementation of function
        #logic for pass/fail and call proper function.
        if expectedAnswer != studentAnswer:
            passed = False
            break
    if passed:
        test4.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test4.failedtest("String length didnt match with inputs " + str + " and " + str2 + "\n")
    endTime = datetime.datetime.now();
    test4.setruntime(TestSuite.calculateruntime(startTime,endTime))


#main testing logic
if __name__ == "__main__":
    #Set up three suites at the top corresponding to each "Part"
    import sys
    suppressCout()
    sys.path.insert(0, 'sys.argv[1]')
    StudentImplementation = importlib.import_module("hw1")
    runtime = 0.0
    suite = TestSuite.TestSuite()
    args = sys.argv[2:]
    for i in range(len(args)):
        if args[i] == "1":
            testQ1(suite, StudentImplementation)
        elif args[i] == "2":
            testQ2(suite, StudentImplementation)
        elif args[i] == "3":
            testQ3(suite, StudentImplementation)
        elif args[i] == "4":
            testQ4(suite, StudentImplementation)
        else:
            print ("No test found matching input")
    #Couts for feedback results. Should be standard for each assignment.
    restoreCout()
    print (suite.results() )
    print ("Success Rate: ", suite.percentage(), "/4")
    print ("Total Runtime: ", suite.totalruntime(), "ms")
