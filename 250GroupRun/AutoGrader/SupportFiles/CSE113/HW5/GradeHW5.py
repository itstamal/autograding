# Make sure filename is "Grade(AssignmentName)" with proper capitalization.
import sys
import datetime
import TestSuite
import importlib
import random
import os
import json
import urllib.request

old_stdout = sys.stdout


def floatCompare(number1, number2):
    return abs(number1 - number2) < 0.01


# helper functions to check correctness (usually renamed solutions dumped in to check against students code)
def suppressCout():
    sys.stdout = open(os.devnull, 'w')


def restoreCout():
    sys.stdout = old_stdout


def genRandom():
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    return alpha[random.randint(0, 51)]


def genString(size):
    str = ""
    for i in range(0, size):
        str += genRandom()
    return str


def floatCompare(float1, float2):
    epsilon = 0.001
    if abs(float1 - float2) < epsilon:
        return True
    return False


def testQ1(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q1")
    test1.setworth(1)  # set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    passed = True

    request = {"request": "getReviews"}
    response = StudentImplementation.handleRequest(request)
    reviews = json.loads(response)
    count = 0
    for movie in reviews.keys():
        count += 1
        int(reviews[movie][0])
        if not len(str(reviews[movie][1])) > 0:
            passed = False
    if count >= 3:
        pass
    else:
        passed = False

    if not passed:
        test1.failedtest("Failed part 1. Returned: " + response + "\n")
    else:
        test1.passedtest()
    test1.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ2(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test2 = TestSuite.Test("Q2")
    test2.setworth(1)  # set point worth of the test
    suite.addtest(test2)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    passed = True

    theRequest = "http://www-student.cse.buffalo.edu/~hartloff/cgi-bin/courseRoster.cgi"
    result = urllib.request.urlopen(theRequest)
    jsonResult = result.read().decode('utf-8')
    listResult = json.loads(jsonResult)

    for ubit in listResult:
        passed = True
        theRequest = "http://www-student.cse.buffalo.edu/~hartloff/cgi-bin/interface.cgi?request=reviewsByID&ID=" + ubit
        result = urllib.request.urlopen(theRequest)
        topMovies = json.loads(result.read().decode('utf-8'))

        request = {"request": "reviewsByID", "ID": ubit}
        response = json.loads(StudentImplementation.handleRequest(request))

        if len(topMovies) != len(response):
            passed = False
        else:
            for movie in topMovies:
                if movie not in response:
                    passed = False
                    break


    if not passed:
        test2.failedtest("Should Return: " + str(topMovies) + "\nReturned: " + str(response) + "\nFor Request: " + str(
            request) + "\n")
    else:
        test2.passedtest()
    test2.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ3(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test3 = TestSuite.Test("Q3")
    test3.setworth(1)  # set point worth of the test
    suite.addtest(test3)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    passed = True
    theRequest = "http://www-student.cse.buffalo.edu/~hartloff/cgi-bin/interface.cgi?request=getAggregateRatings"
    result = urllib.request.urlopen(theRequest)
    solution = json.loads(result.read().decode('utf-8'))

    aggregate = json.loads(StudentImplementation.getAggregateRatings())
    if len(aggregate) != len(solution):
        passed = False
    else:
        for movie in aggregate:
            if movie not in solution.keys():
                passed = False
                break
            else:
                sol = solution[movie]
                agg = aggregate[movie]
                if not floatCompare(float(sol[0]), float(agg[0])):
                    passed = False
                    break
                if int(sol[1]) != int(agg[1]):
                    passed = False
                    break
                solReviews = sol[2]
                aggReviews = agg[2]
                if len(aggReviews) != len(solReviews):
                    passed = False
                    break
                for review in solReviews:
                    if review not in aggReviews:
                        passed = False
                        break

    if not passed:
        test3.failedtest("\nReturned: " + str(aggregate) + "\n")
    else:
        test3.passedtest()
    test3.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ4(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test4 = TestSuite.Test("Q4")
    test4.setworth(1)  # set point worth of the test
    suite.addtest(test4)
    startTime = datetime.datetime.now()  # set up runtime
    endTime = datetime.datetime.now()

    test4.failedtest("Part 4 will be manually graded by viewing your web page\n")

    test4.setruntime(TestSuite.calculateruntime(startTime, endTime))


# main testing logic
if __name__ == "__main__":
    # Set up three suites at the top corresponding to each "Part"
    import sys

    suppressCout()
    sys.path.insert(0, 'sys.argv[1]')
    StudentImplementation = importlib.import_module("hw5Server")
    runtime = 0.0
    suite = TestSuite.TestSuite()
    args = sys.argv[2:]
    for i in range(len(args)):
        if args[i] == "1":
            testQ1(suite, StudentImplementation)
        elif args[i] == "2":
            testQ2(suite, StudentImplementation)
        elif args[i] == "3":
            testQ3(suite, StudentImplementation)
        elif args[i] == "4":
            testQ4(suite, StudentImplementation)
        else:
            print("No test found matching input")
    # Couts for feedback results. Should be standard for each assignment.
    restoreCout()
    print(suite.results())
    print("Success Rate: ", suite.percentage(), "/4")
    print("Total Runtime: ", suite.totalruntime(), "ms")
