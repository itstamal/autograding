# Make sure filename is "Grade(AssignmentName)" with proper capitalization.
import sys
import datetime
import TestSuite
import importlib
import random
import os

old_stdout = sys.stdout


# helper functions to check correctness (usually renamed solutions dumped in to check against students code)
def suppressCout():
    sys.stdout = open(os.devnull, 'w')


def restoreCout():
    sys.stdout = old_stdout


def genRandom():
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    return alpha[random.randint(0, 51)]


def genString(size):
    str = ""
    for i in range(0, size):
        str += genRandom()
    return str


def testQ1(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q1")
    test1.setworth(1)  # set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now()  # set up runtime
    passed = True
    legacyPass = True

    # def rockPaperScissor(playerOneChoice, playerTwoChoice):
    # # inputs are {"rock", "paper", "scissor"}
    # return ""  # return the winner. Either "player1", "player2", or "draw" in the case of a tie

    failP1 = ""
    failP2 = ""
    failOutput = ""
    failExpectedOutput = ""

    cases = [["rock", "rock", "draw"], ["rock", "paper", "player2"], ["rock", "scissor", "player1"],
             ["paper", "rock", "player1"], ["paper", "paper", "draw"], ["paper", "scissor", "player2"],
             ["scissor", "rock", "player2"], ["scissor", "paper", "player1"], ["scissor", "scissor", "draw"]
             ]

    for case in cases:
        p1 = case[0]
        p2 = case[1]
        expected = case[2]
        studentAnswer = StudentImplementation.rockPaperScissor(p1, p2)
        if expected != studentAnswer:
            legacyPass = False
            failP1 = p1
            failP2 = p2
            failOutput = studentAnswer
            failExpectedOutput = expected
            break

    if not legacyPass:

        cases = [["rock", "rock", "draw"], ["rock", "paper", "player2"], ["rock", "scissors", "player1"],
                 ["paper", "rock", "player1"], ["paper", "paper", "draw"], ["paper", "scissors", "player2"],
                 ["scissors", "rock", "player2"], ["scissors", "paper", "player1"], ["scissors", "scissors", "draw"]
                 ]

        for case in cases:
            p1 = case[0]
            p2 = case[1]
            expected = case[2]
            studentAnswer = StudentImplementation.rockPaperScissor(p1, p2)
            if expected != studentAnswer:
                passed = False
                failP1 = p1
                failP2 = p2
                failOutput = studentAnswer
                failExpectedOutput = expected
                break


    if passed:
        test1.passedtest()
    else:
        # if failed pass in a string with explaination. (possibly example input/output)
        test1.failedtest("Player 1 played " + failP1 + ", player 2 played " + failP2 +
                         ", and rockPaperScissor returned " + failOutput + ". Should be " + failExpectedOutput + "\n")
    endTime = datetime.datetime.now();
    test1.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ2(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test2 = TestSuite.Test("Q2")
    test2.setworth(1)  # set point worth of the test
    suite.addtest(test2)
    startTime = datetime.datetime.now()  # set up runtime
    passed = True

    # def computeCommission(saleTotal, margin):
    # # margin is {"low", "mid", "high"}
    # # saleTotal is an integer
    # return 0

    failSale = 0
    failMargin = ""
    failOutput = 0
    failExpectedOutput = 0

    cases = [[710595, 'low', 35529], [737878, 'low', 36893], [586593, 'low', 29329], [710258, 'low', 35512],
             [596175, 'low', 29808], [944220, 'low', 47211], [803779, 'low', 40188], [192946, 'low', 9647],
             [696249, 'low', 34812], [998711, 'low', 49935], [418448, 'low', 20922], [182584, 'low', 9129],
             [981398, 'low', 49069], [887645, 'low', 44382], [233692, 'low', 11684], [723926, 'low', 36196],
             [523641, 'low', 26182], [464586, 'low', 23229], [181227, 'low', 9061], [680363, 'low', 34018],
             [546944, 'low', 27347], [807368, 'low', 40368], [621393, 'low', 31069], [182373, 'low', 9118],
             [184062, 'low', 9203], [420124, 'low', 21006], [607720, 'low', 30386], [754611, 'low', 37730],
             [88515, 'low', 4425], [348619, 'low', 17430], [180943, 'low', 9047], [926923, 'low', 46346],
             [495066, 'low', 24753], [892549, 'low', 44627], [653015, 'low', 32650], [683538, 'low', 34176],
             [302211, 'low', 15110], [638584, 'low', 31929], [990743, 'low', 49537], [517466, 'low', 25873],
             [541668, 'low', 27083], [135216, 'low', 6760], [163075, 'low', 8153], [341123, 'low', 17056],
             [422253, 'low', 21112], [960013, 'low', 48000], [434351, 'low', 21717], [153084, 'low', 7654],
             [669555, 'low', 33477], [132100, 'low', 6605], [240243, 'mid', 24024], [425781, 'mid', 42578],
             [875209, 'mid', 87520], [591966, 'mid', 59196], [202604, 'mid', 20260], [189529, 'mid', 18952],
             [213092, 'mid', 21309], [485974, 'mid', 48597], [500419, 'mid', 50041], [238988, 'mid', 23898],
             [324663, 'mid', 32466], [748292, 'mid', 74829], [684812, 'mid', 68481], [197135, 'mid', 19713],
             [659043, 'mid', 65904], [424600, 'mid', 42460], [84172, 'mid', 8417], [587956, 'mid', 58795],
             [650573, 'mid', 65057], [444407, 'mid', 44440], [229319, 'mid', 22931], [370799, 'mid', 37079],
             [857470, 'mid', 85747], [961991, 'mid', 96199], [866630, 'mid', 86663], [500344, 'mid', 50034],
             [29430, 'mid', 2943], [456167, 'mid', 45616], [926690, 'mid', 92669], [419751, 'mid', 41975],
             [847337, 'mid', 84733], [516841, 'mid', 51684], [622198, 'mid', 62219], [828371, 'mid', 82837],
             [551586, 'mid', 55158], [140764, 'mid', 14076], [190101, 'mid', 19010], [70324, 'mid', 7032],
             [72461, 'mid', 7246], [136635, 'mid', 13663], [630218, 'mid', 63021], [5526, 'mid', 552],
             [661519, 'mid', 66151], [108723, 'mid', 10872], [695596, 'mid', 69559], [257448, 'mid', 25744],
             [534846, 'mid', 53484], [538790, 'mid', 53879], [176784, 'mid', 17678], [470165, 'mid', 47016],
             [961592, 'high', 192318], [546833, 'high', 109366], [96500, 'high', 19300], [900436, 'high', 180087],
             [981191, 'high', 196238], [724384, 'high', 144876], [255566, 'high', 51113], [682328, 'high', 136465],
             [957652, 'high', 191530], [413832, 'high', 82766], [679862, 'high', 135972], [852449, 'high', 170489],
             [181097, 'high', 36219], [826148, 'high', 165229], [499561, 'high', 99912], [449564, 'high', 89912],
             [254643, 'high', 50928], [932917, 'high', 186583], [929962, 'high', 185992], [501832, 'high', 100366],
             [320429, 'high', 64085], [706770, 'high', 141354], [59948, 'high', 11989], [806546, 'high', 161309],
             [444052, 'high', 88810], [56288, 'high', 11257], [823115, 'high', 164623], [348872, 'high', 69774],
             [54134, 'high', 10826], [611609, 'high', 122321], [695919, 'high', 139183], [990427, 'high', 198085],
             [733624, 'high', 146724], [721612, 'high', 144322], [721701, 'high', 144340], [53277, 'high', 10655],
             [939638, 'high', 187927], [643017, 'high', 128603], [389412, 'high', 77882], [349022, 'high', 69804],
             [84446, 'high', 16889], [98179, 'high', 19635], [630989, 'high', 126197], [178543, 'high', 35708],
             [23322, 'high', 4664], [253312, 'high', 50662], [12465, 'high', 2493], [816577, 'high', 163315],
             [526511, 'high', 105302], [406384, 'high', 81276]]

    for case in cases:
        sale = case[0]
        margin = case[1]
        expected = case[2]
        studentAnswer = StudentImplementation.computeCommission(sale, margin)
        if expected != studentAnswer:
            passed = False
            failSale = sale
            failMargin = margin
            failOutput = studentAnswer
            failExpectedOutput = expected
            break

    if passed:
        test2.passedtest()
    else:
        # if failed pass in a string with explanation. (possibly example input/output)
        test2.failedtest("Sale of $" + str(failSale) + " and margin of " + failMargin +
                         " returned " + str(failOutput) + ". Should be " + str(failExpectedOutput) + "\n")
    endTime = datetime.datetime.now()
    test2.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ3(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test3 = TestSuite.Test("Q3")
    test3.setworth(1)  # set point worth of the test
    suite.addtest(test3)
    startTime = datetime.datetime.now()  # set up runtime
    passed = True

    failSalary = 0
    failKids = 0
    failOutput = ""
    failExpectedOutput = ""

    cases = [[21507, 0, 'compact car'], [19785, 0, 'compact car'], [31073, 0, 'compact car'], [22354, 0, 'compact car'],
             [30015, 0, 'compact car'], [21447, 0, 'compact car'], [3919, 0, 'compact car'], [30492, 0, 'compact car'],
             [23688, 0, 'compact car'], [31312, 0, 'compact car'], [30270, 0, 'compact car'], [18063, 0, 'compact car'],
             [49318, 0, 'compact car'], [29916, 0, 'compact car'], [22232, 0, 'compact car'], [36291, 0, 'compact car'],
             [16407, 0, 'compact car'], [17300, 0, 'compact car'], [41904, 0, 'compact car'], [4673, 0, 'compact car'],
             [70511, 0, 'sports car'], [90699, 0, 'sports car'], [96169, 0, 'sports car'], [53869, 0, 'sports car'],
             [84874, 0, 'sports car'], [54800, 0, 'sports car'], [50880, 0, 'sports car'], [79073, 0, 'sports car'],
             [89465, 0, 'sports car'], [59672, 0, 'sports car'], [73440, 0, 'sports car'], [95911, 0, 'sports car'],
             [57091, 0, 'sports car'], [89155, 0, 'sports car'], [87782, 0, 'sports car'], [70789, 0, 'sports car'],
             [80900, 0, 'sports car'], [58332, 0, 'sports car'], [58406, 0, 'sports car'], [83450, 0, 'sports car'],
             [122453, 0, 'super car'], [134959, 0, 'super car'], [139329, 0, 'super car'], [139518, 0, 'super car'],
             [555261, 0, 'super car'], [116548, 0, 'super car'], [140465, 0, 'super car'], [129462, 0, 'super car'],
             [122353, 0, 'super car'], [143044, 0, 'super car'], [110252, 0, 'super car'], [112407, 0, 'super car'],
             [114563, 0, 'super car'], [125109, 0, 'super car'], [138984, 0, 'super car'], [100207, 0, 'super car'],
             [119870, 0, 'super car'], [830961, 0, 'super car'], [114831, 0, 'super car'], [123171, 0, 'super car'],
             [31394, 1, 'station wagon'], [29215, 2, 'station wagon'], [17649, 1, 'station wagon'],
             [48696, 2, 'station wagon'], [45117, 1, 'station wagon'], [17536, 2, 'station wagon'],
             [974, 2, 'station wagon'], [45711, 2, 'station wagon'], [6922, 1, 'station wagon'],
             [21692, 2, 'station wagon'], [13324, 2, 'station wagon'], [21976, 2, 'station wagon'],
             [7135, 2, 'station wagon'], [22335, 2, 'station wagon'], [26167, 2, 'station wagon'],
             [1259, 1, 'station wagon'], [10680, 1, 'station wagon'], [20613, 2, 'station wagon'],
             [40694, 2, 'station wagon'], [30633, 1, 'station wagon'], [91103, 2, 'full-size sedan'],
             [50312, 1, 'full-size sedan'], [90993, 2, 'full-size sedan'], [54232, 1, 'full-size sedan'],
             [86369, 2, 'full-size sedan'], [67310, 1, 'full-size sedan'], [82790, 1, 'full-size sedan'],
             [58896, 2, 'full-size sedan'], [78137, 2, 'full-size sedan'], [53567, 2, 'full-size sedan'],
             [62134, 2, 'full-size sedan'], [64716, 2, 'full-size sedan'], [75361, 2, 'full-size sedan'],
             [85314, 1, 'full-size sedan'], [86849, 2, 'full-size sedan'], [58249, 1, 'full-size sedan'],
             [89961, 1, 'full-size sedan'], [89710, 2, 'full-size sedan'], [51441, 1, 'full-size sedan'],
             [52777, 1, 'full-size sedan'], [108769, 1, 'luxury sedan'], [964000, 2, 'luxury sedan'],
             [123200, 1, 'luxury sedan'], [684530, 1, 'luxury sedan'], [126957, 1, 'luxury sedan'],
             [109345, 1, 'luxury sedan'], [136577, 2, 'luxury sedan'], [109152, 1, 'luxury sedan'],
             [705706, 2, 'luxury sedan'], [113325, 1, 'luxury sedan'], [101791, 1, 'luxury sedan'],
             [108384, 2, 'luxury sedan'], [644334, 2, 'luxury sedan'], [203770, 1, 'luxury sedan'],
             [115425, 2, 'luxury sedan'], [140687, 2, 'luxury sedan'], [121456, 1, 'luxury sedan'],
             [104321, 2, 'luxury sedan'], [307276, 1, 'luxury sedan'], [126591, 1, 'luxury sedan'],
             [12072, 6, 'station wagon'], [16633, 6, 'station wagon'], [37507, 4, 'station wagon'],
             [9161, 6, 'station wagon'], [3665, 5, 'station wagon'], [43090, 3, 'station wagon'],
             [1705, 4, 'station wagon'], [40859, 6, 'station wagon'], [29094, 4, 'station wagon'],
             [3234, 4, 'station wagon'], [44951, 5, 'station wagon'], [27871, 5, 'station wagon'],
             [128, 5, 'station wagon'], [43924, 6, 'station wagon'], [48001, 5, 'station wagon'],
             [31408, 4, 'station wagon'], [5215, 4, 'station wagon'], [10922, 3, 'station wagon'],
             [45989, 5, 'station wagon'], [20935, 6, 'station wagon'], [86179, 6, 'minivan'], [94733, 5, 'minivan'],
             [84071, 5, 'minivan'], [76867, 3, 'minivan'], [64857, 3, 'minivan'], [57922, 5, 'minivan'],
             [51140, 4, 'minivan'], [62057, 5, 'minivan'], [69722, 4, 'minivan'], [61435, 3, 'minivan'],
             [79304, 4, 'minivan'], [79966, 5, 'minivan'], [62087, 3, 'minivan'], [96230, 4, 'minivan'],
             [50456, 4, 'minivan'], [89527, 4, 'minivan'], [90862, 3, 'minivan'], [83220, 6, 'minivan'],
             [93764, 3, 'minivan'], [50099, 3, 'minivan'], [123633, 6, 'full-size SUV'], [105318, 5, 'full-size SUV'],
             [128232, 3, 'full-size SUV'], [115880, 5, 'full-size SUV'], [134729, 6, 'full-size SUV'],
             [128707, 3, 'full-size SUV'], [113514, 6, 'full-size SUV'], [112657, 6, 'full-size SUV'],
             [127411, 5, 'full-size SUV'], [123241, 5, 'full-size SUV'], [126744, 3, 'full-size SUV'],
             [103240, 5, 'full-size SUV'], [128048, 6, 'full-size SUV'], [119744, 5, 'full-size SUV'],
             [124948, 6, 'full-size SUV'], [144333, 6, 'full-size SUV'], [146875, 4, 'full-size SUV'],
             [687761, 4, 'full-size SUV'], [124566, 5, 'full-size SUV'], [616377, 5, 'full-size SUV']]

    for case in cases:
        income = case[0]
        kids = case[1]
        expected = case[2]
        studentAnswer = StudentImplementation.getFamilyCar(income, kids)
        if expected != studentAnswer:
            passed = False
            failSalary = income
            failKids = kids
            failOutput = studentAnswer
            failExpectedOutput = expected
            break

    if passed:
        test3.passedtest()
    else:
        # if failed pass in a string with explanation. (possibly example input/output)
        test3.failedtest("Salary of $" + str(failSalary) + " and " + str(failKids) +
                         " children returned " + str(failOutput) + ". Should be " + str(failExpectedOutput) + "\n")
    endTime = datetime.datetime.now()
    test3.setruntime(TestSuite.calculateruntime(startTime, endTime))


def testQ4(suite, StudentImplementation):
    # Create a new test for each test function and add to proper suite.
    test4 = TestSuite.Test("Q4")
    test4.setworth(1)  # set point worth of the test
    suite.addtest(test4)
    startTime = datetime.datetime.now()  # set up runtime
    passed = True
    failString = ""
    shouldBe = ""

    # def isCommonWebAddressFormat(inputURL):
    # # return True if inputURL starts with "http" and ends with ".com" ".net" or ".org" and returns False otherwise
    # return False

    givenTrue = ["https://www.google.com", "https://en.wikipedia.org", "https://www.python.org",
                 "http://stackoverflow.com", "http://www.w3schools.com", "http://www.speedtest.net"]

    givenFalse = ["http://buffalo.edu", "http://weather.gov", "www.google.com", "website"]

    testTrue = ["http://facebook.com", "http://twitter.com", "http://google.com", "http://youtube.com",
                "http://wordpress.org", "http://linkedin.com", "http://sedo.com", "http://wikipedia.org",
                "http://blogspot.com", "http://adobe.com", "http://pinterest.com", "http://wordpress.com",
                "http://hugedomains.com", "http://instagram.com", "http://tumblr.com", "http://amazon.com",
                "http://yahoo.com", "http://microsoft.com", "http://networkadvertising.org", "http://apple.com",
                "http://flickr.com", "http://vimeo.com", "http://reddit.com", "http://w3.org", "http://weebly.com",
                "http://qq.com", "http://nytimes.com", "http://myspace.com", "http://addthis.com",
                "http://buydomains.com", "http://statcounter.com", "http://parallels.com", "http://digg.com",
                "http://blogger.com", "http://jimdo.com", "http://godaddy.com", "http://vk.com",
                "http://feedburner.com", "http://stumbleupon.com", "http://wix.com", "http://cnn.com",
                "http://bluehost.com", "http://imdb.com", "http://huffingtonpost.com", "http://msn.com",
                "http://ovh.net", "http://issuu.com", "http://macromedia.com", "http://go.com",
                "http://creativecommons.org", "http://mozilla.org", "http://theguardian.com", "http://slideshare.net",
                "http://wsj.com", "http://sourceforge.net", "http://delicious.com", "http://forbes.com",
                "http://ebay.com", "http://livejournal.com", "http://fc2.com", "http://about.com", "http://wp.com",
                "http://yelp.com", "http://typepad.com", "http://washingtonpost.com", "http://weibo.com",
                "http://joomla.org", "http://namejet.com", "http://aol.com", "http://reuters.com", "http://webs.com",
                "http://dailymotion.com", "http://usatoday.com", "http://cpanel.net", "http://gnu.org",
                "http://homestead.com", "http://cpanel.com", "http://amazonaws.com", "http://bing.com",
                "http://time.com", "http://constantcontact.com", "http://networksolutions.com", "http://bbb.org",
                "http://soundcloud.com", "http://bloomberg.com", "http://photobucket.com", "http://addtoany.com",
                "http://latimes.com", "http://geocities.com", "http://wikimedia.org", "http://opera.com",
                "http://npr.org", "http://eepurl.com", "http://etsy.com", "http://deviantart.com", "http://wired.com",
                "http://crazydomains.com", "http://imgur.com", "http://archive.org", "http://apache.org",
                "http://cnet.com", "http://hibu.com", "http://tripod.com", "http://taobao.com", "http://adition.com",
                "http://technorati.com", "http://one.com", "http://eventbrite.com", "http://php.net",
                "http://scribd.com", "http://histats.com", "http://tripadvisor.com", "http://gravatar.com",
                "http://mapquest.com", "http://bandcamp.com", "http://live.com", "http://luminate.com",
                "http://github.com", "http://businessinsider.com", "http://mashable.com", "http://cbsnews.com",
                "http://ted.com", "http://barnesandnoble.com", "http://list-manage.com", "http://pbs.org",
                "http://altervista.org", "http://foxnews.com", "http://a8.net", "http://nationalgeographic.com",
                "http://ibm.com", "http://elegantthemes.com", "http://un.org", "http://acquirethisname.com",
                "http://techcrunch.com", "http://disqus.com", "http://bbc.com", "http://hostmonster.com",
                "http://nifty.com", "http://enom.com", "http://over-blog.com", "http://wiley.com",
                "http://buzzfeed.com", "http://163.com", "http://engadget.com", "http://clickbank.net",
                "http://ning.com", "http://themeforest.net", "http://theatlantic.com", "http://netnames.com",
                "http://webmd.com", "http://ft.com", "http://businessweek.com", "http://posterous.com", "http://hp.com",
                "http://sfgate.com", "http://wikia.com", "http://nature.com", "http://blog.com", "http://1and1.com",
                "http://examiner.com", "http://cbslocal.com", "http://bizjournals.com", "http://weather.com",
                "http://sohu.com", "http://meetup.com", "http://economist.com", "http://sciencedirect.com",
                "http://goodreads.com", "http://usnews.com", "http://nbcnews.com", "http://boston.com",
                "http://nydailynews.com", "http://slate.com", "http://angelfire.com", "http://kickstarter.com",
                "http://cargocollective.com", "http://gizmodo.com", "http://phpbb.com", "http://uk2.net",
                "http://oracle.com", "http://naver.com", "http://hostgator.com", "http://ifeng.com",
                "http://chicagotribune.com", "http://skype.com", "http://mysql.com", "http://previsite.com",
                "http://mozilla.com", "http://quantcast.com", "http://bigcartel.com", "http://newyorker.com",
                "http://dedecms.com", "http://springer.com", "http://prnewswire.com", "http://vistaprint.com",
                "http://alexa.com", "http://youku.com", "http://wunderground.com", "http://squarespace.com",
                "http://detik.com", "http://mediafire.com", "http://marriott.com", "http://justhost.com",
                "http://yolasite.com", "http://jiathis.com", "http://paypal.com", "http://zdnet.com",
                "http://xinhuanet.com", "http://dropbox.com", "http://marketwatch.com", "http://alibaba.com",
                "http://booking.com", "http://reverbnation.com", "http://studiopress.com", "http://dreamhost.com",
                "http://unesco.org", "http://xing.com", "http://shopify.com", "http://shinystat.com",
                "http://icann.org", "http://squidoo.com", "http://youronlinechoices.com", "http://samsung.com",
                "http://cnbc.com", "http://discovery.com", "http://bloglovin.com", "http://sciencedaily.com",
                "http://theglobeandmail.com", "http://seesaa.net", "http://yellowbook.com", "http://myshopify.com",
                "http://netcraft.com", "http://ehow.com", "http://mac.com", "http://indiatimes.com",
                "http://behance.net", "http://list-manage1.com", "http://cdbaby.com", "http://prweb.com",
                "http://sogou.com", "http://uk2group.com", "http://perldancer.org", "http://1688.com",
                "http://debian.org", "http://idealdomainauction.com", "http://howstuffworks.com", "http://salon.com",
                "http://houzz.com", "http://blinklist.com", "http://theverge.com", "http://surveymonkey.com",
                "http://freewebs.com", "http://fastcompany.com", "http://enable-javascript.com",
                "http://businesswire.com", "http://wikihow.com", "http://istockphoto.com", "http://netscape.com",
                "http://xrea.com", "http://xiti.com", "http://scientificamerican.com", "http://slashdot.org",
                "http://walmart.com", "http://dell.com", "http://wordpress-fr.net", "http://elpais.com",
                "http://ovh.com", "http://spotify.com", "http://answers.com", "http://reference.com",
                "http://intel.com", "http://cafepress.com", "http://doi.org", "http://example.com",
                "http://windowsphone.com", "http://globo.com", "http://drupal.org", "http://arstechnica.com",
                "http://opensource.org", "http://discuz.net", "http://prestashop.com", "http://cisco.com",
                "http://shareasale.com", "http://ructg.com", "http://comsenz.com", "http://newsvine.com",
                "http://att.com", "http://ap.org", "http://thedailybeast.com", "http://rollingstone.com",
                "http://sun.com", "http://com.com", "http://telnic.org", "http://worldbank.org", "http://topsy.com",
                "http://allaboutcookies.org", "http://webnode.com", "http://lifehacker.com", "http://nymag.com",
                "http://cyberchimps.com", "http://icq.com", "http://vice.com", "http://hubpages.com",
                "http://sagepub.com", "http://mlb.com", "http://stackoverflow.com", "http://target.com",
                "http://change.org", "http://fotolia.com", "http://inc.com", "http://fortune.com", "http://ikea.com",
                "http://lcn.com", "http://sciencemag.org", "http://redcross.org", "http://smugmug.com",
                "http://tinypic.com", "http://msnbc.com", "http://mtv.com", "http://getpocket.com",
                "http://entrepreneur.com", "http://ycombinator.com", "http://instructables.com",
                "http://aboutcookies.org", "http://hexun.com", "http://blogs.com", "http://aliyun.com",
                "http://newsweek.com", "http://oxfordjournals.org", "http://patch.com", "http://urbandictionary.com",
                "http://mayoclinic.org", "http://accuweather.com", "http://woothemes.com", "http://cocolog-nifty.com",
                "http://merriam-webster.com", "http://dribbble.com", "http://hollywoodreporter.com",
                "http://sitemeter.com", "http://canalblog.com", "http://ezinearticles.com", "http://indiegogo.com",
                "http://googlegroups.com", "http://csmonitor.com", "http://thestar.com", "http://sphinn.com",
                "http://twitpic.com"]

    testFalse = ["feXeDKUXyJWUPXYu", "LjOntGebPxpxVOtW", "NDWYDHToBKIbawhcm", "GRKepdYBWXcxANtiOJT", "PPgrQaVJWAJForNV",
                 "MShYOTBHxPuvRSU", "IOJDtjiwpOqes", "fqHkTrcDzqsSg", "KzNkeNtmz", "VORxfjadLyZEIp",
                 "pGMsXAErhKOePkenjCeD", "bouVihiVdVMxsyxGoFzV", "NoMwspfXEN", "srqnZVJLyBFN", "KOszlOsGlNZh",
                 "bgudvDYllMhycuCE", "rCBVoufO", "vkozAciHnSsZzV", "rlwEVeESAIlIkUxgztDF", "JcgRtFGAN",
                 "nrkOldGRcNtwYJuQmo", "xOBKwHYCJJVrvsphOV", "ZaXZpYONu", "RiietjvmuFGwMtiMj", "tmjlCRJjLm",
                 "KVMcAINbQ", "ZlwNAjydxgKAtnVdEO", "VlHzkMjJKhSKgjGM", "vBWbkoDiIihxue", "MgCIvPeRsdS", "idpOjVhgVd",
                 "SQSMFPjYcoSao", "vUuvCGyPSTKtA", "cQBFyQgDIKAzIkHJXbSF", "ktYUVOmOpLc", "PqyNcuLny", "bnVcjCYVBw",
                 "AHtzQGersSZIrMeeDmUN", "AezQzJsKEL", "UHcJfnquPbAjsGreG", "HTKWjjKbVxQgyaG", "fqetvtbSie",
                 "ajAAIkXmASrUfFETO", "WojTmqYSvyuDgqEXbHz", "ACGYWpUtcAKeRmvHA", "VrvVTdfgSLCS", "nIhwlPQso",
                 "FRunQqLngLI", "OapSWMvnnoHsi", "evIUJTsAKsWnjF", "kkOWndSbTD", "ZqDsrgHoq", "tzFHVFoluBW",
                 "GWiRpDzLcq", "btDBIogweFAxayILG", "xadqTppZD", "ZejuBSBxwV", "pyJmwLuZ", "cIYhmpLHiVRyiq",
                 "aVuJraFWAdHfFN", "mwlxPWQSQKDC", "NJUuWaAjmAnKconpIJ", "DoVWZHuDNtKoqVfWyPJw", "voLrdrRLtxiPndwgoEwx",
                 "MFoAPRWAOYuSksNa", "wgevtKJj", "BijcbEJH", "YWwSBpjXxpbA", "RCtbSBIzLjHrAMpdeP", "LRKondvxba",
                 "ZAFrjPUxFE", "WAeenThIbdHQiV", "OLZRLrnqHDOQOPQggrF", "qxhcHCTB", "AmOxmlbpQqRmY", "mblSNIbbcIOmfnHI",
                 "FDlgcoqzwdJvxwIj", "nhuylcostpPG", "wUwTQZPrajWuFmK", "sHsGtmplVhSR", "joYLDNHCNrIvO",
                 "EFmKqBkSMkozPgb", "JeuELiCeBB", "DdcVyGnI", "lbDwUmLAAWk", "tHMLXWLMMIQU", "JqLkdZURl",
                 "JxvTWJERWjQHzZ", "tZvzNoOAEsdFyUYT", "mWtpuHDBMXb", "FdokJFrQbYgB", "EwOgFzQQsB", "ndndupfHimE",
                 "qiXiiXtPOElGQk", "kWtrmnwTyvDItWXGBjoQ", "kOXaUMUsHqick", "lExDrUfXn", "reXzJzxGKCCRtZcH",
                 "yOrdtRiVuypUmdLCUn", "HlNHmDSABZFcu", "jhzTeLJkRAzS", "rcHggiGghTlQxmQ", "ZltOLihNQSuGsoeeN",
                 "TXxDGZcM", "abRrBSKvXAJBOLVx", "bsmHMZqTZxsCvAOscEZj", "ICynqjjKxrRTOuJ", "TulylBFdjqatdJiM",
                 "pYUApubKgynLKflX", "EXIbZrVW", "vhmNucIWwAHrrYKWffh", "DtfPcflxn", "japlnyXKVmqNYyAND",
                 "vCFMCkVdcuQkWv", "riQGzMhX", "MAQXnsyikaPQUSeqRC", "ihWSPpTm", "SZeHCNer", "qjeEBoydF",
                 "OeyewjdrBtEPh", "GFqAPBRWmyprTHItRHIR", "jTdtTwiBZxx", "QpjrjrkRfsBJqWDRJTxv", "qsCgnSHNvMBA",
                 "PNfQHrDr", "enSWvbvVHedmQEuMUChd", "ZnzIXstqqGsDspW", "pUzoaDtuValbDHalLqU", "mcFPGHdlfiEtzGUx",
                 "EVkcDXSxEmVh", "sdjOfHEbizWl", "LjHHXvBOWry", "laMKcXYvjvpWulNDPjq", "fboaQxlerzyq", "CpldZDynngYOk",
                 "bgTHfrtaSfDlHkKqq", "tETsdDDypUAWDsyXs", "XrnmeuRdZMsEMdEVRyny", "koJFiLqfTAuNQCv",
                 "SMxeNASYcxXrLFlux", "XjsmNUITpG", "DTalCjzmJ", "DintJsfftKDTySlR", "FphneqEekUNxooQ",
                 "mxfVdCYTfSdxyIuryByV", "qbJzCUYxCUguU", "PvEWsRwRwhdMOvkcR", "OHIUBAQKDCD", "cgWhrvLfB",
                 "QtWWawuaQBlwKOKWJ", "nlCndBnvJETKPBQpMCmw", "GQCbzwvUUH", "ALlcyzsOMwwYRrp", "lJDxyxKwvKbWbpisRAC",
                 "hdQqPccch", "ngPnrZsPpguXGuWvm", "LkuazdSYgOJwpFvV", "tqJHotEYEJQUAqGjS", "DRMOJpCfoNmRTcY",
                 "nHvHopyIbuEGmiVdxKz", "XGLzeQGFNhljpBSYs", "iZDklGjvkK", "lBPtBDXxuIssWBupnI", "NlxGjCRUmFR",
                 "BkmQOckPG", "FlXKMzktDDxMfD", "AprboOrVbPDFENOHEKx", "tTyRBrQtXAOwmHJ", "MsvPvybvfYUhmVVwLh",
                 "uEoWQkvb", "BIMJCCyZhyATFePcbRTJ", "TinSHeDHzyL", "IwNMZfGmIxpnw", "vFiXgeYxsbh", "LyAkDuOYGq",
                 "EZVKXuDwHqIAeL", "AqLykMQdKqEVLWaP", "FNpVexIDtqCbUAeBV", "VkPTXezYNyrkloJjy", "hkAJezwGZObvxei",
                 "MOTXKKThJRnI", "wgCxtSCCmwOgoxC", "voEYThlVVi", "IctXBmrr", "VARaGYbaF", "UbfqzDjcd", "NxwRCIdwKDwhg",
                 "YzGupyxsPEHFguicXgrU", "qUMuKqcauzDHsdDA", "JKzHzXCvJyMDVyM", "RKBwOpmYkEzb", "AwQQfFnUUZbXVsRV",
                 "zHrfKlDeCOzemZKhqP", "lWvZVWgxNClHSKgpWnm", "mwfJEvKAvhmNeKbTb", "AsHENWmBGiZRZ",
                 "VFiubUohiaNRsTwxOYl", "vQVCpGOIrMJhtRJiZiKM", "krczWRrGj", "nbTlgpPU"]

    for commonSite in givenTrue:
        if not StudentImplementation.isCommonWebAddressFormat(commonSite):
            passed = False
            failString = commonSite
            shouldBe = "True"
            break

    if passed:
        for notCommonSite in givenFalse:
            if StudentImplementation.isCommonWebAddressFormat(notCommonSite):
                passed = False
                failString = notCommonSite
                shouldBe = "False"
                break

    if passed:
        for commonSite in testTrue:
            if not StudentImplementation.isCommonWebAddressFormat(commonSite):
                passed = False
                failString = commonSite
                shouldBe = "True"
                break

    if passed:
        for notCommonSite in testFalse:
            if StudentImplementation.isCommonWebAddressFormat(notCommonSite):
                passed = False
                failString = notCommonSite
                shouldBe = "False"
                break

    if passed:
        test4.passedtest()
    else:
        # if failed pass in a string with explaination. (possibly example input/output)
        test4.failedtest("isCommonWebAddressFormat did not return " + shouldBe + " on input " + failString + "\n")
    endTime = datetime.datetime.now()
    test4.setruntime(TestSuite.calculateruntime(startTime, endTime))


# main testing logic
if __name__ == "__main__":
    # Set up three suites at the top corresponding to each "Part"
    import sys

    suppressCout()
    sys.path.insert(0, 'sys.argv[1]')
    StudentImplementation = importlib.import_module("hw2")
    runtime = 0.0
    suite = TestSuite.TestSuite()
    args = sys.argv[2:]
    for i in range(len(args)):
        if args[i] == "1":
            testQ1(suite, StudentImplementation)
        elif args[i] == "2":
            testQ2(suite, StudentImplementation)
        elif args[i] == "3":
            testQ3(suite, StudentImplementation)
        elif args[i] == "4":
            testQ4(suite, StudentImplementation)
        else:
            print("No test found matching input")
    # Couts for feedback results. Should be standard for each assignment.
    restoreCout()
    print(suite.results())
    print("Success Rate: ", suite.percentage(), "/4")
    print("Total Runtime: ", suite.totalruntime(), "ms")

