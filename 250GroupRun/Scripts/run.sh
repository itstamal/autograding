#!/bin/bash
SupportPath="/projects/CSE250/Spring2016/AutoGrader/SupportFiles/"
GRADER=$SupportPath$1"/*.cpp"
STUDENT=$2"*.cpp"

CC="g++"
COMPILER_FLAGS="-w"
LINKER_FLAGS="-std=c++11"

STU_OBJ_NAME="a.out"

if [[ $1 == "java" ]]; then
    sh "javac main.java $2"
fi

if [[ $1 == "c++" ]]; then
    sh "g++ $LINKER_FLAGS -I $SupportPath$1/ -I $2 $GRADER $STUDENT -o $2$STU_OBJ_NAME"
fi

if [[ $1 == "python" ]]; then
    sh "python main.py $2"
fi

#javac main.java *.java
#main.class *.class

#java main
