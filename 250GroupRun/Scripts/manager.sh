course=$1
hwid=$2
dir=$3
shift
shift
shift

if [ $course == "CSE250" ]
then
    ssh daviddob@timberlake.cse.buffalo.edu "sh ~/Spring2016/Scripts/runCpp.sh $hwid $dir $@"
fi

if [ $course == "EAS230" ]
then
    ssh daviddob@timberlake.cse.buffalo.edu "sh ~/Spring2016/Scripts/runPy.sh $hwid $dir $@"
fi

if [ $course == "CSE113" ]
then
    ssh daviddob@timberlake.cse.buffalo.edu "sh ~/Spring2016/Scripts/runPy.sh $hwid $dir $@"
fi
