#!/bin/bash

## Parameters ##
# 1) lang
# 2) students path without /
# 3) tests path without /
# 4) solution files path
# 5) Name of assignment
#
##  Return  ##
# 1) Tests Passed
# 2) runtime
# 3) feedback >>> which right now is just whatever is printing to stdout
#
##   Notes   ##
# Must clean up when donex

testsPassed=0
feedback=""
runtime=0

#This loop runs all of the .txt files in the test dir as param 1 for anyone of the executables found in the student dir
#It is currently calculating the total runtime for all of the testsPassed
#It will use jesseScript to see whether or not they passed
#   if passed:
#       increment testsPassed
#   else:
#       save feedback for later

for inputs in $3/*.txt; do
    if [[ $1 == "Java" ]]; then
        START=$(date +%s.%N)
            output=$(java $2/$5.out $inputs)
        END=$(date +%s.%N)
        testRuntime=$(echo "$END - $START" | bc)
    fi
    if [[ $1 == "C++" ]]; then
        START=$(date +%s.%N)
            output=$($2/$5.out $inputs)
        END=$(date +%s.%N)
        testRuntime=$(echo "$END - $START" | bc)
    fi
    if [[ $1 == "Python" ]]; then
        START=$(date +%s.%N)
            output=$(python $2/$5.py $inputs)
        END=$(date +%s.%N)
        testRuntime=$(echo "$END - $START" | bc)
    fi

    runtime=$(echo "$runtime + $testRuntime" | bc)
    #echo $output

    #results=$(jesseScript solution $output)
    results=$output

    if [[ $results == "Passed" ]]; then
        (( testsPassed++ ))
    else
        feedback=echo -e $feedback"\n "$results
    fi

done

echo $runtime
echo $testsPassed
echo -e $feedback
