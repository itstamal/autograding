#!/bin/bash
# $1=filepath
# $2=hwid
DB=postel.cse.buffalo.edu
USER=farnsworth
PASS=\!InfiniteHours1337\!\$

testsTotal=0
testsPassed=0

runtime=0

workingQuestion=""
feedback=""

ubit=""
findUBIT=false
function parse {
    while IFS=' ' read -r -a line
    do
        if $findUBIT; then
            ubit=${line[0]}
            findUBIT=false
        fi

        if [[ ${line[0]} == "START" ]]; then
            findUBIT=true
        fi

        if [[ ${line[0]} == "END" ]]; then
            findUBIT=true

            runtime=`echo "$runtime * 1000" | sed s/://g | bc` 
	    echo "UBIT: "$ubit
            echo "TotalTests: "$testsTotal
            echo "TestsPassed: "$testsPassed
            echo "Runtime: "$runtime
            echo -e $feedback

            #CONNECT TO THE SQL HERE
            timestamp=$(date +"%Y-%m-%d %H:%M:%S")

            $(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "INSERT INTO 250submissions(ubit, hwID, lang, testsTotal, testsPassed, runtime, feedback, timestamp) VALUES ('$ubit', 'HW4', 'Python', '$testsTotal', '$testsPassed', '$runtime', '', '2016-04-09 01:59:59')")

            testsTotal=0
            testsPassed=0
            runtime=0

            workingQuestion=""
            feedback=""

            ubit=""
        fi

    	if [[ ${line[0]} == "Passed" || ${line[0]} == "Failed" ]]; then

            workingQuestion=${line[*]}

            ((testsTotal++))

    		if [[ ${line[0]} == "Passed" ]]; then
    			((testsPassed++))
    		fi

        elif [[ ${line[1]} == "Runtime:" ]]; then

            #runtime=$runtime+${line[2]%??} #FOR CSE 250

            runtime=$runtime+${line[2]} #FOR CSE 113
        fi
        #else
        #    feedback=$feedback$workingQuestion": "${line[*]}'\n'
        #fi


        #runtime=`echo "$runtime" | bc`

        #echo $ubit
        #echo $testsTotal
        #echo $testsPassed
        #echo $runtime
        #echo -e $feedback
    done
}


cat "$1" | parse
