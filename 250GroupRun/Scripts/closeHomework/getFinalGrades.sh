#############################################################################
#                                                                           #
#   This script is used to grab data from SQL after an assignment finishes  #
#   It will then output the results to a .csv                               #
#	PARAMS::                                                                #
#		1) class roster as a .csv                                           #
#		2) the assignemnt number as an int (ie: 4)                          #
#                                                                           #
#   Keep in mind that this will delete a .csv that this file creates        #
#   So if you care about previouse results then don't run this script in    #
#   	in the same directory.                                              #
#                                                                           #
#############################################################################

DB=postel.cse.buffalo.edu
USER=farnsworth
PASS=\!InfiniteHours1337\!\$

class=${1:0:3}

submissions=$class"submissions"
gradebook=$class"gradebook"
hwID="HW"$2

counter=0
classCount=0

echo $class
echo $submissions
echo $gradebook
echo $hwID

read -r firstline<$1

IFS=',' read -r -a ubitArray <<< "$firstline"

rm $class$hwID"Results.csv"
echo "UBIT, testsPassed, runtime, date-time, language," >> $class$hwID"Results.csv"

for i in ${ubitArray[@]}
do

	userID=${i%@*}
	userID=${userID// /}
	userID=${userID,,}

	output=$(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "SELECT testsPassed, MIN(runtime), timestamp, lang FROM (SELECT * FROM $submissions WHERE ubit = '$userID' AND hwID = '$hwID' and testsPassed = (SELECT MAX(testsPassed) FROM $submissions WHERE ubit='$userID' AND hwID='$hwID' AND timestamp <= '2016-05-14 01:59:59')) AS maxTable;")

	#echo $output

	output=($output)

	testsPassed=${output[0]}
	runtime=${output[1]}
	timestamp="${output[2]} ${output[3]}"
	lang=${output[4]}

	if [[ $runtime == "NULL" ]]; then
		((counter++))
	fi

	((classCounter++))

    echo "##########"

	echo UBIT: $userID

	echo grade: $testsPassed
	echo runtime: $runtime
	echo timestamp: $timestamp
	echo lang: $lang

    echo "##########"

	echo "$userID, $testsPassed, $runtime, $timestamp, $lang," >> $class$hwID"Results.csv"

	#This script will write to the table updateing all of the final grades
	#saveToSQL=$(mysql -h $DB -u $USER -p$PASS -D farnsworth_db -se "UPDATE $gradebook SET '$hwID' = '$testsPassed' WHERE  ubit = '$userID';")

	#finalPath=/shared/projects/CSE250/Spring2016/AutoGrader/Backup/FinalSubmissions/$class/$hwID/$userID/$lang/$timestamp/

	#mkdir -p $finalPath

	#cp /shared/projects/CSE250/Spring2016/AutoGrader/Backup/Archive/*$class/$hwID/$userID/$timestamp/* $finalPath
done

echo "null: " $counter
echo "class size: " $classCounter
