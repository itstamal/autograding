#!/bin/bash
SupportPath="/projects/CSE250/Spring2016/AutoGrader/SupportFiles/"
GRADER=$SupportPath$1"/*.cpp" 
STUDENT=$2"*.cpp"
 
CC="g++"
COMPILER_FLAGS="-w"
LINKER_FLAGS="-std=c++11"

STU_OBJ_NAME="a.out"

g++ $LINKER_FLAGS -I $SupportPath$1/ -I $2 $GRADER $STUDENT -o $2$STU_OBJ_NAME

