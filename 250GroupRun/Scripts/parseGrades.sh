#!/bin/bash

#END OF RESULT-----------------------------------------------
#aalterio
#Passed test Q1

#Passed test Q2

#Passed test Q3

#Passed test Q4


#Success Rate:  4.0 /4
#Total Runtime:  33.243 ms
#Submission generated on: Wed Feb 17 15:34:58 EST 2016
#END OF RESULT-----------------------------------------------

testsTotal=0
testsPassed=0

runtime=0

workingQuestion=""
feedback=""
function parse {
    while IFS=' ' read -r -a line
    do
        #echo ${line[*]}
    	if [[ ${line[0]} == "Passed" || ${line[0]} == "Failed" ]]; then

            workingQuestion=${line[*]}

            ((testsTotal++))

    		if [[ ${line[0]} == "Passed" ]]; then
    			((testsPassed++))
    		fi

        elif [[ ${line[1]} == "Runtime:" ]]; then
            runtime=$runtime+${line[2]}

        else
            feedback=$feedback$workingQuestion": "${line[*]}'\n'
        fi



    done

    runtime=`echo "$runtime" | bc`

    echo $testsTotal
    echo $testsPassed
    echo $runtime
    echo -e $feedback
}


sh "./testOut" $1 $2 | parse
