rm Results.txt
assignment="Interpreter"
timeoutMsg="\n Terminated unexpectedly due to long runtime. Please test with provided makefile. \n Success Rate: 0%"
segfault="\n Segmentation Fault! Please test with provided makefile. \n Success Rate: 0%"

# Compiles and tests the given cpp files and dumps to
# Results.txt file
function compile {
   cp -rf ./zSupportFiles/Manditory/Makefile $dir
   cp -rf ./zSupportFiles/Manditory/Parser.h $dir
   cp -rf ./zSupportFiles/Manditory/Parser.cpp $dir
   cp -rf ./zSupportFiles/Manditory/sampleScripts/ $dir
   cp -rf ./zSupportFiles/Manditory/InterpreterGrade.cpp $dir
   cp -n  ./zSupportFiles/Optional/* $dir
   make -C $dir &> /dev/null 
}
#Main Logic
for dir in ./*; 
do
   if [[ -d $dir ]]
   then
      for file in "$dir"/*  
      do
         user=$(echo $dir | sed 's:./::')
         #Checks if file exists and user is on leaderboard list
         if [[ $file == *$assignment.cpp ]] && grep -Fxq $user /projects/CSE250/Fall2015/AutoGrader/Submissions/Leaderboard.txt
         then
	    compile
	    cd $dir
	    echo $user
	    echo -n $user','>> ../Results.txt #keeps user and runtime on the same line csv
	    timeout 45 ./a.out >> ../Results.txt
	    
	    exitCode=$?
	    if [[ $exitCode == 124 ]] 
	    then 
	       echo -e $timeoutMsg >> ../Results.txt 
	    fi
	    if [[ $exitCode == 134 ]] 
	    then 
	       echo -e $segfault >> ../Results.txt 
	    fi	    
	    if [[ $exitCode == 139 ]] 
	    then 
	       echo -e $segfault >> ../Results.txt 
	    fi
	    cd ..  
	    rm $dir/a.out 2> /dev/null      
	 fi #End file check
      done
   fi #End of submission folders
done

# Cleans up any core dump files
rm core.* 2> /dev/null
echo "Finished"
