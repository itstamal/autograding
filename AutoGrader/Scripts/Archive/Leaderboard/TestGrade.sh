rm TotalResults.txt
assignment="Interpreter"
timeoutMsg="\n Terminated unexpectedly due to long runtime. Please test with provided makefile. \n Success Rate: 0%"
segfault="\n Segmentation Fault! Please test with provided makefile. \n Success Rate: 0%"
#echo "Copying files from submit..."
#cp -rfp /submit/hartloff/CSE250/* /projects/CSE250/Fall2015/AutoGrader/Submissions/
#echo "Backing up submissions to Archive..."
#tar -czvf /projects/CSE250/Fall2015/Backup/Archive/"automaticSubmission`date`.tar.gz" /submit/hartloff/CSE250/* >/dev/null

checkFile() {
   echo "" > $dir/newStat.txt
   for file in "$dir"/*
   do
      if [[ $file == *.cpp ]] 
      then
         OLD_STAT_FILE=$dir/fileCheck.txt
 
         echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
      fi
      
      if [[ $file == *.h ]] 
      then
         OLD_STAT_FILE=$dir/fileCheck.txt
 
         echo $(stat --printf "%y\n" $file) >> $dir/newStat.txt
      fi
   done
   
   diff $dir/newStat.txt $dir/fileCheck.txt >/dev/null
   result=$?   
   if [[ $result == 0 ]] || [[ $dir == ./zzArchive ]] || [[ $dir == ./zSupportFiles ]]
   then
      return 1
   else
      # update the OLD_STAT_FILE
      cp -f $dir/newStat.txt $dir/fileCheck.txt
      return 0
   fi   
}

function checkLeaderboard {
   oldIFS=$IFS
   IFS=$'\n'
   for line in $(cat Results.txt)
   do
      if [ $line == "Off to the Leaderboard!" ]
      then
         echo $user > Leaderboard.txt
      fi
   done
   IFS=$oldIFS
}

#removes compiled files before going to next student.
function cleanUp {
   if [[ -f a.out ]]
      then 
      rm a.out
   fi
}

# Compiles and tests the given cpp files and dumps to
# Results.txt file
function compile {
   cp -rf ./zSupportFiles/Manditory/Makefile $dir
   cp -rf ./zSupportFiles/Manditory/Parser.h $dir
   cp -rf ./zSupportFiles/Manditory/Parser.cpp $dir
   cp -rf ./zSupportFiles/Manditory/sampleScripts/ $dir
   cp -rf ./zSupportFiles/Manditory/InterpreterGrade.cpp $dir
   cp -n  ./zSupportFiles/Optional/* $dir
   make -C $dir &>> Results.txt 
}

function finishResult {
   checkLeaderboard
   echo "Submission generated on: $(date)" >> Results.txt
   echo "END OF RESULT-----------------------------------------------" >> Results.txt
   cat Results.txt >> TotalResults.txt
}

#run all files through MOSS
function moss {
   echo "Running MOSS"
   echo "------------------------RUNNING MOSS------------------------" >> TotalResults.txt
   perl moss.pl -l cc -d -b $assignment.h ./*/$assignment.cpp | sed '/Uploading/d' >> TotalResults.txt
}

#Writes username of the sutdent (submission folder) to results.
function writeUser {
   # sed is used to truncate output to just username (':' is delimiter)
   user=$(echo $dir | sed 's:./::')
   echo $user > Results.txt
   echo $user
}
#Main Logic
for dir in ./*; 
do
   if [[ -d $dir ]]
   then
      for file in "$dir"/*  
      do
         #Checks if file exists and has been modified in last 60 mins
         if [[ $file == *$assignment.cpp ]] #&& checkFile $dir && [[ $? == 0 ]]
         then
	    writeUser
	    compile $file &>> Results.txt
	    cd $dir
	    timeout 45 ./a.out >> ../Results.txt
	    
	    exitCode=$?
	    if [[ $exitCode == 124 ]] 
	    then 
	       echo -e $timeoutMsg >> ../Results.txt 
	    fi
	    if [[ $exitCode == 134 ]] 
	    then 
	       echo -e $segfault >> ../Results.txt 
	    fi	    
	    if [[ $exitCode == 139 ]] 
	    then 
	       echo -e $segfault >> ../Results.txt 
	    fi
	    cd ..
            finishResult
	    
	    rm $dir/a.out 2> /dev/null
	    #checkFile $dir  	    
	    #emails student with results
            #cat Results.txt | mail -s $assignment" Results" $user@buffalo.edu
	    cleanUp        
	 fi #End file check
      done
   fi #End of submission folders
done

#moss
#cat TotalResults.txt > $(pwd)/Archive/$(date)Results.txt
# Cleans up any core dump files
rm Results.txt
rm core.* 2> /dev/null
#cat Cronlog.txt | mail -s "Script run successful" daviddob@buffalo.edu
echo "Finished"
