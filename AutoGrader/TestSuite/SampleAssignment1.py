def oneFireball(num):
    return num*75

def multipleFireballs(numFire, numSols):
    return numSols*(100-25*numFire)

def fullname(first, last):
    return first + " " + last

def fullnameLength(first, last):
    return len(first + last)
