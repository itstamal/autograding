// Make sure filename is "Grade(AssignmentName)" with proper capitalization.
#include "HW0.h"
#include "TestSuite.h"
#include <iostream>
#include <string>
#include <cstdlib>
//Set up three suites at the top corresponding to each "Part"
TestSuite suite;

char genRandom(){
    std::string alpha ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    return alpha.at(rand() % alpha.length());
}

//helper functions to check correctness (usually renamed solutions dumped in to check against students code)
int countCharsGrade(std::string words, char character){
    int count=0;
    for(int i=0; i<words.length(); i++){
        if(words[i] == character){
            count++;
        }
    }
    return count;
}
void testQ1(void){
    //Create a new test for each test function and add to proper suite.
    Test *test1 = new Test("Q1");
    test1->setWorth(25); //set point worth of the test
    suite.addTest(test1);
    clock_t startTime = clock(); // set up runtime
    std::string customString = getCustomString(); //test for correctness
    //logic for pass/fail and call proper function.
    if(customString.compare("NULL") != 0){
        test1->passedTest();
    }else{
        //if failed pass in a string with explaination. (possibly example input/output)
        test1->failedTest("Strings didnt match");
    }
    clock_t endTime = clock();
    test1->setRuntime(calculateRuntime(startTime,endTime));
}
void testQ2(void){
    Test *test2 = new Test("Q2");
    test2->setWorth(25);
    suite.addTest(test2);
    clock_t startTime = clock();
    std::string Str;
    char c = genRandom();
    for(unsigned int i = 0; i < 80; ++i){
        Str += genRandom();
    }
    if(countChars(Str, c) == countCharsGrade(Str, c)){
        test2->passedTest();
    }else{
        test2->failedTest("Strings didnt match"); 
    }
    clock_t endTime = clock();
    test2->setRuntime(calculateRuntime(startTime,endTime));
}
void testQ3(void){
    Test *test3 = new Test("Q3");
    test3->setWorth(25);
    suite.addTest(test3);
    clock_t startTime = clock();    
    std::string words = getCustomString();
    char character = 'f';
    int count = 0;
    for(int i=0; i<words.length(); i++){
        if(words[i] == character){
            count++;
        }
    }
    if(count == countCharsCustom(character)){
        test3->passedTest();
    }else{
        test3->failedTest("Strings didnt match");         
        
    }
    clock_t endTime = clock();
    test3->setRuntime(calculateRuntime(startTime,endTime));    
}

void testQ4(void){
    Test *test4 = new Test("Q4");
    test4->setWorth(25);
    suite.addTest(test4);
    clock_t startTime = clock();     
    int count = 0;
    int n = 40;
    int a = 0, b = 1, c, i;
    for (i = 2; i <= n; i++){
        c = a + b;
        a = b;
        b = c;
    }
    count = b;
    if(count == computeFibonacci(n)){
        test4->passedTest();
    }else{
        test4->failedTest("Fibonacci number didnt match");         
        
    }
    clock_t endTime = clock();
    test4->setRuntime(calculateRuntime(startTime,endTime));     
}

int main(int argc, char *argv[]){
    //main testing logic
    double runtime;
    for (int i = 1; i < argc; i++) { 
        switch (atoi(argv[i])) {
        case 1:
            cout << "Running testQ1" << endl;
            testQ1();
            break;
        case 2:
            cout << "Running testQ2" << endl;  
            testQ2();
            break;
        case 3:
            cout << "Running testQ3" << endl;  
            testQ3();
            break;
        case 4:
            cout << "Running testQ4" << endl;  
            testQ4();
            break;
            /*        case 5:
            cout << "Running testQ2GetMultiplication" << endl;  
            testQ2GetMultiplication();
            break;
        case 6:
            cout << "Running testQ2EvaluateAsPolynomial" << endl;  
            testQ2EvaluateAsPolynomial();
            break;    
        case 7:   
            cout << "Running testQ3FindEqualPolyEval" << endl;  
            testQ3FindEqualPolyEval();
            break;   
*/            
        default:
            cout << "No test found matching input" << endl;
        }
    }
    //Couts for feedback results. Should be standard for each assignment.
    cout << suite.results() << endl; 
    cout << "Success Rate: " << suite.percentage() << "%" << endl;
    cout << "Total Runtime: " << suite.totalRuntime() << "ms" << endl;
    return 0;
}
