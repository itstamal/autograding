#Make sure filename is "Grade(AssignmentName)" with proper capitalization.
import sys
import datetime
import TestSuite
import importlib

#helper functions to check correctness (usually renamed solutions dumped in to check against students code)
def myImplementationQ1(num):
    return num*75

def myImplementationQ2(numFire, numSols):
    return numSols*(100-25*numFire)

def myImplementationQ3(first, last):
    return first + " " + last

def myImplementationQ4(first, last):
    return len(first + last)

def testQ1(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q1")
    test1.setworth(1) #set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now() # set up runtime
    expectedAnswer = myImplementationQ1(3) #call your implementation of function
    studentAnswer = StudentImplementation.oneFireball(3) #call student's implementation of function
    #logic for pass/fail and call proper function.
    if expectedAnswer == studentAnswer:
        test1.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test1.failedtest("Strings didnt match")
    endTime = datetime.datetime.now();
    test1.setruntime(TestSuite.calculateruntime(startTime,endTime))

def testQ2(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q2")
    test1.setworth(1) #set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now() # set up runtime
    expectedAnswer = myImplementationQ2(5, 10) #call your implementation of function
    studentAnswer = StudentImplementation.multipleFireballs(5, 10) #call student's implementation of function
    #logic for pass/fail and call proper function.
    if expectedAnswer == studentAnswer:
        test1.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test1.failedtest("Strings didnt match")
    endTime = datetime.datetime.now();
    test1.setruntime(TestSuite.calculateruntime(startTime,endTime))

def testQ3(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q3")
    test1.setworth(1) #set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now() # set up runtime
    expectedAnswer = myImplementationQ3("Samuel", "Siegart") #call your implementation of function
    studentAnswer = StudentImplementation.fullname("Samuel", "Siegart") #call student's implementation of function
    #logic for pass/fail and call proper function.
    if expectedAnswer == studentAnswer:
        test1.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test1.failedtest("Strings didnt match")
    endTime = datetime.datetime.now();
    test1.setruntime(TestSuite.calculateruntime(startTime,endTime))

def testQ4(suite, StudentImplementation):
    #Create a new test for each test function and add to proper suite.
    test1 = TestSuite.Test("Q4")
    test1.setworth(1) #set point worth of the test
    suite.addtest(test1)
    startTime = datetime.datetime.now() # set up runtime
    expectedAnswer = myImplementationQ4("Samuel", "Siegart") #call your implementation of function
    studentAnswer = StudentImplementation.fullnameLength("Samuel", "Siegart") #call student's implementation of function
    #logic for pass/fail and call proper function.
    if expectedAnswer == studentAnswer:
        test1.passedtest()
    else:
        #if failed pass in a string with explaination. (possibly example input/output)
        test1.failedtest("Strings didnt match")
    endTime = datetime.datetime.now();
    test1.setruntime(TestSuite.calculateruntime(startTime,endTime))


#main testing logic
if __name__ == "__main__":
    #Set up three suites at the top corresponding to each "Part"
    StudentImplementation = importlib.import_module(sys.argv[1])
    runtime = 0.0
    suite = TestSuite.TestSuite()
    args = sys.argv[2:]
    for i in xrange(len(args)):
        if args[i] == "1":
            testQ1(suite, StudentImplementation)
        elif args[i] == "2":
            testQ2(suite, StudentImplementation)
        elif args[i] == "3":
            testQ3(suite, StudentImplementation)
        elif args[i] == "4":
            testQ4(suite, StudentImplementation)
        else:
            print "No test found matching input"
    #Couts for feedback results. Should be standard for each assignment.
    print suite.results()
    print "Success Rate: ", suite.percentage(), "%"
    print "Total Runtime: ", suite.totalruntime(), "ms"
